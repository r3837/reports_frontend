import { types } from "../types/types";
import { configApi } from "../helpers/configApi";
import { titleCase } from "../helpers/titleCase";

export const settingApi = () => {
  return async (dispatch) => {
    //VERIFY CONNECTION
    // const isOnLine = await connectNetwork();
    // if (!isOnLine) {
    //   return;
    // }
    dispatch(loadingSettingsApi());

    try {
      let config = configApi(null, { method: "GET" });

      let resp = await fetch(
        `${process.env.REACT_APP_API_URL}/settings/newSend`,
        config
      );
      let response = await resp.json();

      if (resp.status === 200) {
        const docsRT = response.docsRquestType.map((item) => ({
          value: item.tipo_servicio_id,
          label: item.tipo_servicio,
        }));

        const docsR = response.docsResponsable.map((item) => ({
          value: item.usuario_id,
          label: `${item.nombres} ${item.apellidos} / (${item.cargo})`,
        }));

        const docsST = response.docsRequestStatus.map((item) => ({
          value: item.estado_solicitud,
          label: item.estado_solicitud,
        }));

        const docsEQ = response.docsRequestTeams.map((item) => ({
          value: item.equipo_id,
          label: item.nombre_equipo,
        }));

        const data = {
          docsRquestType: docsRT,
          docsResponsable: docsR,
          docsRequestStatus: docsST,
          settingUserTeams: docsEQ
        };

        dispatch(settings(data));
      }
    } catch (error) {
      console.log(error);
    }
  };
};

export const settingRequestStatusApi = () => {
  return async (dispatch) => {
    //VERIFY CONNECTION
    // const isOnLine = await connectNetwork();
    // if (!isOnLine) {
    //   return;
    // }
    dispatch(loadingSettingsApi());

    try {
      let config = configApi(null, { method: "GET" });

      let resp = await fetch(
        `${process.env.REACT_APP_API_URL}/settings/requestStatus`,
        config
      );
      let response = await resp.json();

      if (resp.status === 200) {
        const docsST = response.docsRequestStatus.map((item) => ({
          value: item.estado_solicitud,
          label: item.estado_solicitud,
        }));

        const data = {
          docsRequestStatus: docsST,
        };

        dispatch(settings(data));
      }
    } catch (error) {
      console.log(error);
    }
  };
};

export const settingForUserApi = () => {
  return async (dispatch) => {
    //VERIFY CONNECTION
    // const isOnLine = await connectNetwork();
    // if (!isOnLine) {
    //   return;
    // }
    dispatch(loadingSettingsUserApi(true));

    try {
      let config = configApi(null, { method: "GET" });

      let resp = await fetch(
        `${process.env.REACT_APP_API_URL}/settings/dataForUser`,
        config
      );
      let response = await resp.json();

      if (resp.status === 200) {
        const docsUserRol = response.docsUserRol.map((item) => ({
          value: item.rol,
          label: titleCase(item.rol),
        }));

        const docsUserDependency = response.docsUserDependency.map((item) => ({
          value: item.dependencia_id,
          label: item.nombre_dependencia,
        }));

        const docsUserCargo = response.docsUserCargo.map((item) => ({
          value: item.cargo_id,
          label: titleCase(item.cargo),
        }));

        const data = {
          settingUserRol: docsUserRol,
          settingUserDependency: docsUserDependency,
          settingUserCargo: docsUserCargo,
        };

        dispatch(settingsUser(data));
      }

      if (resp.status !== 200) {
        dispatch(loadingSettingsUserApi(false));
      }
    } catch (error) {
      console.log(error);
      dispatch(loadingSettingsUserApi(false));
    }
  };
};

export const settingForTeamApi = () => {
  return async (dispatch) => {
    //VERIFY CONNECTION
    // const isOnLine = await connectNetwork();
    // if (!isOnLine) {
    //   return;
    // }
    dispatch(loadingSettingsTeamApi(true));

    try {
      let config = configApi(null, { method: "GET" });

      let resp = await fetch(
        `${process.env.REACT_APP_API_URL}/settings/dataForTeam`,
        config
      );
      let response = await resp.json();

      if (resp.status === 200) {
        const docsUserDependency = response.docsTeamDependency.map((item) => ({
          value: item.dependencia_id,
          label: item.nombre_dependencia,
        }));

        const docsTypeTeam = response.docsTypeTeam.map((item) => ({
          value: item.tipo_equipo_id,
          label: titleCase(item.name),
        }));

        const data = {
          settingTeamTypeTeam: docsTypeTeam,
          settingTeamDependency: docsUserDependency,
        };

        dispatch(settingsTypeTeam(data));
      }

      if (resp.status !== 200) {
        dispatch(loadingSettingsTeamApi(false));
      }
    } catch (error) {
      console.log(error);
      dispatch(loadingSettingsTeamApi(false));
    }
  };
};

export const loadingSettingsApi = () => ({
  type: types.loadingSettings,
});

export const settings = (data) => ({
  type: types.settings,
  payload: data,
});

export const loadingSettingsUserApi = (data) => ({
  type: types.loadingSettingsUser,
  payload: data,
});

export const settingsUser = (data) => ({
  type: types.settingsUser,
  payload: data,
});

export const loadingSettingsTeamApi = (data) => ({
  type: types.loadingSettingsUser,
  payload: data,
});

export const settingsTypeTeam = (data) => ({
  type: types.settingsTypeTeam,
  payload: data,
});
