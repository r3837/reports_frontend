import { types } from "../types/types";
import { configApi } from "../helpers/configApi";
import { toastAlert } from "../helpers/alertToast";

export const getCargosApi = ({ page, limit }) => {
  return async (dispatch) => {
    //VERIFY CONNECTION
    // const isOnLine = await connectNetwork();
    // if (!isOnLine) {
    //   return;
    // }
    dispatch(loadingCargosApi(true));

    try {
      let config = configApi(null, { method: "GET" });

      let resp = await fetch(
        `${process.env.REACT_APP_API_URL}/register-settings/getCargos/${page}/${limit}`,
        config
      );
      let response = await resp.json();

      if (resp.status === 200) {
        dispatch(cargoSetting(response));
      }

      if (resp.status !== 200) {
        dispatch(loadingCargosApi(false));
      }
    } catch (error) {
      dispatch(loadingCargosApi(false));
    }
  };
};

export const createCargosApi = (data, close) => {
  return async (dispatch) => {
    //VERIFY CONNECTION
    // const isOnLine = await connectNetwork();
    // if (!isOnLine) {
    //   return;
    // }
    dispatch(loadingCreateCargosApi(true));

    try {
      const token = localStorage.getItem("token");
      let config = configApi(data, { method: "POST" }, token);

      let resp = await fetch(
        `${process.env.REACT_APP_API_URL}/register-settings/createCargo`,
        config
      );
      let response = await resp.json();

      if (resp.status === 200) {
        dispatch(createCargoSetting(response.doc));
        toastAlert("success", "El cargo se creo correctamente!");
        close();
      }

      if (resp.status !== 200) {
        dispatch(loadingCreateCargosApi(false));
        toastAlert(
          "error",
          "Ocurrrio un error al crear el cargo, es probable que ya existe!"
        );
      }
    } catch (error) {
      dispatch(loadingCreateCargosApi(false));
      toastAlert("error", "Ocurrrio un error al crear el cargo");
    }
  };
};

export const updateCargosApi = (data, close) => {
  return async (dispatch) => {
    //VERIFY CONNECTION
    // const isOnLine = await connectNetwork();
    // if (!isOnLine) {
    //   return;
    // }
    dispatch(loadingUpdateCargosApi(true));

    try {
      const token = localStorage.getItem("token");
      let config = configApi(data, { method: "PUT" }, token);

      let resp = await fetch(
        `${process.env.REACT_APP_API_URL}/register-settings/updateCargo`,
        config
      );
      let response = await resp.json();

      if (resp.status === 200) {
        dispatch(updateCargoSetting(response.doc));
        toastAlert("success", "El cargo se actualizo correctamente!");
        close();
      }

      if (resp.status !== 200) {
        dispatch(loadingUpdateCargosApi(false));
        toastAlert(
          "error",
          "Ocurrrio un error al actualizar el cargo, ya existe!"
        );
      }
    } catch (error) {
      dispatch(loadingUpdateCargosApi(false));
      toastAlert("error", "Ocurrrio un error al actualiar el cargo!");
    }
  };
};

export const removeCargoApi = (data) => {
  return async (dispatch) => {
    //VERIFY CONNECTION
    // const isOnLine = await connectNetwork();
    // if (!isOnLine) {
    //   return;
    // }
    dispatch(loadingRemoveCargosApi(true));

    try {
      const token = localStorage.getItem("token");
      let config = configApi(data, { method: "DELETE" }, token);

      let resp = await fetch(
        `${process.env.REACT_APP_API_URL}/register-settings/removeCargo`,
        config
      );
      let response = await resp.json();

      if (resp.status === 200) {
        dispatch(removeCargoSetting(response.doc));
        toastAlert("success", "El cargo se elimino satisfactoriamente");
      }

      if (resp.status !== 200) {
        dispatch(loadingRemoveCargosApi(false));
        toastAlert("error", "Ocurrrio un error al eliminar el cargo!");
      }
    } catch (error) {
      dispatch(loadingRemoveCargosApi(false));
      toastAlert("error", "Ocurrrio un error al eliminar el cargo!");
    }
  };
};

export const getRolesApi = ({ page, limit }) => {
  return async (dispatch) => {
    //VERIFY CONNECTION
    // const isOnLine = await connectNetwork();
    // if (!isOnLine) {
    //   return;
    // }
    dispatch(loadingCargosApi(true));

    try {
      let config = configApi(null, { method: "GET" });

      let resp = await fetch(
        `${process.env.REACT_APP_API_URL}/register-settings/getRoles/${page}/${limit}`,
        config
      );
      let response = await resp.json();

      if (resp.status === 200) {
        dispatch(rolSetting(response));
      }

      if (resp.status !== 200) {
        dispatch(loadingCargosApi(false));
      }
    } catch (error) {
      dispatch(loadingCargosApi(false));
    }
  };
};

export const createRolApi = (data, close) => {
  return async (dispatch) => {
    //VERIFY CONNECTION
    // const isOnLine = await connectNetwork();
    // if (!isOnLine) {
    //   return;
    // }
    dispatch(loadingCreateRolApi(true));

    try {
      const token = localStorage.getItem("token");
      let config = configApi(data, { method: "POST" }, token);

      let resp = await fetch(
        `${process.env.REACT_APP_API_URL}/register-settings/createRol`,
        config
      );
      let response = await resp.json();

      if (resp.status === 200) {
        dispatch(createRolSetting(response.doc));
        toastAlert("success", "El rol se creo correctamente!");
        close();
      }

      if (resp.status !== 200) {
        dispatch(loadingCreateRolApi(false));
        toastAlert(
          "error",
          "Ocurrrio un error al crear el rol, es probable que ya existe!"
        );
      }
    } catch (error) {
      dispatch(loadingCreateRolApi(false));
      toastAlert("error", "Ocurrrio un error al crear el rol");
    }
  };
};

export const updateRolApi = (data, close) => {
  return async (dispatch) => {
    //VERIFY CONNECTION
    // const isOnLine = await connectNetwork();
    // if (!isOnLine) {
    //   return;
    // }
    dispatch(loadingUpdateRolApi(true));

    try {
      const token = localStorage.getItem("token");
      let config = configApi(data, { method: "PUT" }, token);

      let resp = await fetch(
        `${process.env.REACT_APP_API_URL}/register-settings/updateRol`,
        config
      );
      let response = await resp.json();

      if (resp.status === 200) {
        dispatch(updateRolSetting(response.doc));
        toastAlert("success", "El rol se actualizo correctamente!");
        close();
      }

      if (resp.status !== 200) {
        dispatch(loadingUpdateRolApi(false));
        toastAlert(
          "error",
          "Ocurrrio un error al actualizar el rol, ya existe!"
        );
      }
    } catch (error) {
      dispatch(loadingUpdateRolApi(false));
      toastAlert("error", "Ocurrrio un error al actualiar el rol!");
    }
  };
};

export const removeRoleApi = (data) => {
  return async (dispatch) => {
    //VERIFY CONNECTION
    // const isOnLine = await connectNetwork();
    // if (!isOnLine) {
    //   return;
    // }
    dispatch(loadingRemoveRolApi(true));

    try {
      const token = localStorage.getItem("token");
      let config = configApi(data, { method: "DELETE" }, token);

      let resp = await fetch(
        `${process.env.REACT_APP_API_URL}/register-settings/removeRol`,
        config
      );
      let response = await resp.json();

      if (resp.status === 200) {
        dispatch(removeRolSetting(response.doc));
        toastAlert("success", "El rol se elimino satisfactoriamente");
      }

      if (resp.status !== 200) {
        dispatch(loadingRemoveRolApi(false));
        toastAlert("error", "Ocurrrio un error al eliminar el rol!");
      }
    } catch (error) {
      dispatch(loadingRemoveRolApi(false));
      toastAlert("error", "Ocurrrio un error al eliminar el rol!");
    }
  };
};

// dependency

export const getDependencyApi = ({ page, limit }) => {
  return async (dispatch) => {
    //VERIFY CONNECTION
    // const isOnLine = await connectNetwork();
    // if (!isOnLine) {
    //   return;
    // }
    dispatch(loadingDependencyApi(true));

    try {
      let config = configApi(null, { method: "GET" });

      let resp = await fetch(
        `${process.env.REACT_APP_API_URL}/register-settings/getDependencies/${page}/${limit}`,
        config
      );
      let response = await resp.json();

      if (resp.status === 200) {
        dispatch(dependencySetting(response));
      }

      if (resp.status !== 200) {
        dispatch(loadingDependencyApi(false));
      }
    } catch (error) {
      dispatch(loadingDependencyApi(false));
    }
  };
};

export const createDependencyApi = (data, close) => {
  return async (dispatch) => {
    //VERIFY CONNECTION
    // const isOnLine = await connectNetwork();
    // if (!isOnLine) {
    //   return;
    // }
    dispatch(loadingCreateDependencyApi(true));

    try {
      const token = localStorage.getItem("token");
      let config = configApi(data, { method: "POST" }, token);

      let resp = await fetch(
        `${process.env.REACT_APP_API_URL}/register-settings/createDependency`,
        config
      );
      let response = await resp.json();

      if (resp.status === 200) {
        dispatch(createDependencySetting(response.doc));
        toastAlert("success", "El depndencia se creo correctamente!");
        close();
      }

      if (resp.status !== 200) {
        dispatch(loadingCreateDependencyApi(false));
        toastAlert(
          "error",
          "Ocurrrio un error al crear el dependencia, es probable que ya existe!"
        );
      }
    } catch (error) {
      console.log(error);
      dispatch(loadingCreateDependencyApi(false));
      toastAlert("error", "Ocurrrio un error al crear la dependencia");
    }
  };
};

export const updateDependencyApi = (data, close) => {
  return async (dispatch) => {
    //VERIFY CONNECTION
    // const isOnLine = await connectNetwork();
    // if (!isOnLine) {
    //   return;
    // }
    dispatch(loadingUpdateDependencyApi(true));

    try {
      const token = localStorage.getItem("token");
      let config = configApi(data, { method: "PUT" }, token);

      let resp = await fetch(
        `${process.env.REACT_APP_API_URL}/register-settings/updateDependency`,
        config
      );
      let response = await resp.json();

      if (resp.status === 200) {
        dispatch(updateDependencySetting(response.doc));
        toastAlert("success", "El dependencia se actualizo correctamente!");
        close();
      }

      if (resp.status !== 200) {
        dispatch(loadingUpdateDependencyApi(false));
        toastAlert(
          "error",
          "Ocurrrio un error al actualizar el dependencia, ya existe!"
        );
      }
    } catch (error) {
      dispatch(loadingUpdateDependencyApi(false));
      toastAlert("error", "Ocurrrio un error al actualiar el dependencia!");
    }
  };
};

export const removeDependencyApi = (data) => {
  return async (dispatch) => {
    //VERIFY CONNECTION
    // const isOnLine = await connectNetwork();
    // if (!isOnLine) {
    //   return;
    // }
    dispatch(loadingRemoveDependencyApi(true));

    try {
      const token = localStorage.getItem("token");
      let config = configApi(data, { method: "DELETE" }, token);

      let resp = await fetch(
        `${process.env.REACT_APP_API_URL}/register-settings/removeDependency`,
        config
      );
      let response = await resp.json();

      if (resp.status === 200) {
        dispatch(removeDependencySetting(response.doc));
        toastAlert("success", "El dependencia se elimino satisfactoriamente");
      }

      if (resp.status !== 200) {
        dispatch(loadingRemoveDependencyApi(false));
        toastAlert("error", "Ocurrrio un error al eliminar el dependencia!");
      }
    } catch (error) {
      dispatch(loadingRemoveDependencyApi(false));
      toastAlert("error", "Ocurrrio un error al eliminar el dependencia!");
    }
  };
};

// team

export const getTeamsApi = ({ page, limit }) => {
  return async (dispatch) => {
    //VERIFY CONNECTION
    // const isOnLine = await connectNetwork();
    // if (!isOnLine) {
    //   return;
    // }
    dispatch(loadingTeamApi(true));

    try {
      let config = configApi(null, { method: "GET" });

      let resp = await fetch(
        `${process.env.REACT_APP_API_URL}/register-settings/getTeams/${page}/${limit}`,
        config
      );
      let response = await resp.json();

      if (resp.status === 200) {
        console.log(response);
        dispatch(teamSetting(response));
      }

      if (resp.status !== 200) {
        dispatch(loadingTeamApi(false));
      }
    } catch (error) {
      dispatch(loadingTeamApi(false));
    }
  };
};

export const createTeamApi = (data, close) => {
  return async (dispatch) => {
    //VERIFY CONNECTION
    // const isOnLine = await connectNetwork();
    // if (!isOnLine) {
    //   return;
    // }
    dispatch(loadingCreateTeamApi(true));

    try {
      const token = localStorage.getItem("token");
      let config = configApi(data, { method: "POST" }, token);

      let resp = await fetch(
        `${process.env.REACT_APP_API_URL}/register-settings/createTeam`,
        config
      );
      let response = await resp.json();

      if (resp.status === 200) {
        dispatch(createTeamSetting(response.doc));
        toastAlert("success", "El equipo se creo correctamente!");
        close();
      }

      if (resp.status !== 200) {
        dispatch(loadingCreateTeamApi(false));
        toastAlert(
          "error",
          "Ocurrrio un error al crear el equipo, es probable que ya existe!"
        );
      }
    } catch (error) {
      console.log(error);
      dispatch(loadingCreateTeamApi(false));
      toastAlert("error", "Ocurrrio un error al crear el equipo");
    }
  };
};

export const updateTeamApi = (data, close) => {
  return async (dispatch) => {
    //VERIFY CONNECTION
    // const isOnLine = await connectNetwork();
    // if (!isOnLine) {
    //   return;
    // }
    dispatch(loadingUpdateTeamApi(true));

    try {
      const token = localStorage.getItem("token");
      let config = configApi(data, { method: "PUT" }, token);

      let resp = await fetch(
        `${process.env.REACT_APP_API_URL}/register-settings/updateTeam`,
        config
      );
      let response = await resp.json();

      if (resp.status === 200) {
        dispatch(updateTeamSetting(response.doc));
        toastAlert("success", "El equipo se actualizo correctamente!");
        close();
      }

      if (resp.status !== 200) {
        dispatch(loadingUpdateTeamApi(false));
        toastAlert(
          "error",
          "Ocurrrio un error al actualizar el equipo, ya existe!"
        );
      }
    } catch (error) {
      dispatch(loadingUpdateTeamApi(false));
      toastAlert("error", "Ocurrrio un error al actualiar el equipo!");
    }
  };
};

export const removeTeamApi = (data) => {
  return async (dispatch) => {
    //VERIFY CONNECTION
    // const isOnLine = await connectNetwork();
    // if (!isOnLine) {
    //   return;
    // }
    dispatch(loadingRemoveTeamApi(true));

    try {
      const token = localStorage.getItem("token");
      let config = configApi(data, { method: "DELETE" }, token);

      let resp = await fetch(
        `${process.env.REACT_APP_API_URL}/register-settings/removeTeam`,
        config
      );
      let response = await resp.json();

      if (resp.status === 200) {
        dispatch(removeTeamSetting(response.doc));
        toastAlert("success", "El equipo se elimino satisfactoriamente");
      }

      if (resp.status !== 200) {
        dispatch(loadingRemoveTeamApi(false));
        toastAlert("error", "Ocurrrio un error al eliminar el equipo!");
      }
    } catch (error) {
      dispatch(loadingRemoveTeamApi(false));
      toastAlert("error", "Ocurrrio un error al eliminar el equipo!");
    }
  };
};

// type team

export const getTypeTeamsApi = ({ page, limit }) => {
  return async (dispatch) => {
    //VERIFY CONNECTION
    // const isOnLine = await connectNetwork();
    // if (!isOnLine) {
    //   return;
    // }
    dispatch(loadingTypeTeamApi(true));

    try {
      let config = configApi(null, { method: "GET" });

      let resp = await fetch(
        `${process.env.REACT_APP_API_URL}/register-settings/getTypeTeams/${page}/${limit}`,
        config
      );
      let response = await resp.json();

      if (resp.status === 200) {
        dispatch(typeTeamSetting(response));
      }

      if (resp.status !== 200) {
        dispatch(loadingTypeTeamApi(false));
      }
    } catch (error) {
      dispatch(loadingTypeTeamApi(false));
    }
  };
};

export const createTypeTeamApi = (data, close) => {
  return async (dispatch) => {
    //VERIFY CONNECTION
    // const isOnLine = await connectNetwork();
    // if (!isOnLine) {
    //   return;
    // }
    dispatch(loadingCreateTypeTeamApi(true));

    try {
      const token = localStorage.getItem("token");
      let config = configApi(data, { method: "POST" }, token);

      let resp = await fetch(
        `${process.env.REACT_APP_API_URL}/register-settings/createTypeTeam`,
        config
      );
      let response = await resp.json();

      if (resp.status === 200) {
        dispatch(createTypeTeamSetting(response.doc));
        toastAlert("success", "El tipo equipo se creo correctamente!");
        close();
      }

      if (resp.status !== 200) {
        dispatch(loadingCreateTypeTeamApi(false));
        toastAlert(
          "error",
          "Ocurrrio un error al crear el tipo equipo, es probable que ya existe!"
        );
      }
    } catch (error) {
      dispatch(loadingCreateTypeTeamApi(false));
      toastAlert("error", "Ocurrrio un error al crear el equipo");
    }
  };
};

export const updateTypeTeamApi = (data, close) => {
  return async (dispatch) => {
    //VERIFY CONNECTION
    // const isOnLine = await connectNetwork();
    // if (!isOnLine) {
    //   return;
    // }
    dispatch(loadingUpdateTypeTeamApi(true));

    try {
      const token = localStorage.getItem("token");
      let config = configApi(data, { method: "PUT" }, token);

      let resp = await fetch(
        `${process.env.REACT_APP_API_URL}/register-settings/updateTypeTeam`,
        config
      );
      let response = await resp.json();

      if (resp.status === 200) {
        dispatch(updateTypeTeamSetting(response.doc));
        toastAlert("success", "El tipo equipo se actualizo correctamente!");
        close();
      }

      if (resp.status !== 200) {
        dispatch(loadingUpdateTypeTeamApi(false));
        toastAlert(
          "error",
          "Ocurrrio un error al actualizar el tipo equipo, ya existe!"
        );
      }
    } catch (error) {
      dispatch(loadingUpdateTypeTeamApi(false));
      toastAlert("error", "Ocurrrio un error al actualiar el tipo equipo!");
    }
  };
};

export const removeTypeTeamApi = (data) => {
  return async (dispatch) => {
    //VERIFY CONNECTION
    // const isOnLine = await connectNetwork();
    // if (!isOnLine) {
    //   return;
    // }
    dispatch(loadingRemoveTypeTeamApi(true));

    try {
      const token = localStorage.getItem("token");
      let config = configApi(data, { method: "DELETE" }, token);

      let resp = await fetch(
        `${process.env.REACT_APP_API_URL}/register-settings/removeTypeTeam`,
        config
      );
      let response = await resp.json();

      if (resp.status === 200) {
        dispatch(removeTypeTeamSetting(response.doc));
        toastAlert("success", "El tipo equipo se elimino satisfactoriamente");
      }

      if (resp.status !== 200) {
        dispatch(loadingRemoveTypeTeamApi(false));
        toastAlert("error", "Ocurrrio un error al eliminar el tipo equipo!");
      }
    } catch (error) {
      dispatch(loadingRemoveTypeTeamApi(false));
      toastAlert("error", "Ocurrrio un error al eliminar el tipo equipo!");
    }
  };
};

// type service

export const getTypeServicesApi = ({ page, limit }) => {
  return async (dispatch) => {
    //VERIFY CONNECTION
    // const isOnLine = await connectNetwork();
    // if (!isOnLine) {
    //   return;
    // }
    dispatch(loadingTypeServiceApi(true));

    try {
      let config = configApi(null, { method: "GET" });

      let resp = await fetch(
        `${process.env.REACT_APP_API_URL}/register-settings/getTypeServices/${page}/${limit}`,
        config
      );
      let response = await resp.json();

      if (resp.status === 200) {
        dispatch(typeServiceSetting(response));
      }

      if (resp.status !== 200) {
        dispatch(loadingTypeServiceApi(false));
      }
    } catch (error) {
      dispatch(loadingTypeServiceApi(false));
    }
  };
};

export const createTypeServiceApi = (data, close) => {
  return async (dispatch) => {
    //VERIFY CONNECTION
    // const isOnLine = await connectNetwork();
    // if (!isOnLine) {
    //   return;
    // }
    dispatch(loadingCreateTypeServiceApi(true));

    try {
      const token = localStorage.getItem("token");
      let config = configApi(data, { method: "POST" }, token);

      let resp = await fetch(
        `${process.env.REACT_APP_API_URL}/register-settings/createTypeService`,
        config
      );
      let response = await resp.json();

      if (resp.status === 200) {
        console.log(response.doc);
        dispatch(createTypeServiceSetting(response.doc));
        toastAlert("success", "El tipo servicio se creo correctamente!");
        close();
      }

      if (resp.status !== 200) {
        dispatch(loadingCreateTypeServiceApi(false));
        toastAlert(
          "error",
          "Ocurrrio un error al crear el tipo servicio, es probable que ya existe!"
        );
      }
    } catch (error) {
      dispatch(loadingCreateTypeServiceApi(false));
      toastAlert("error", "Ocurrrio un error al crear el equipo");
    }
  };
};

export const updateTypeServiceApi = (data, close) => {
  return async (dispatch) => {
    //VERIFY CONNECTION
    // const isOnLine = await connectNetwork();
    // if (!isOnLine) {
    //   return;
    // }
    dispatch(loadingUpdateTypeServiceApi(true));

    try {
      const token = localStorage.getItem("token");
      let config = configApi(data, { method: "PUT" }, token);

      let resp = await fetch(
        `${process.env.REACT_APP_API_URL}/register-settings/updateTypeService`,
        config
      );
      let response = await resp.json();

      if (resp.status === 200) {
        dispatch(updateTypeServiceSetting(response.doc));
        toastAlert("success", "El tipo servicio se actualizo correctamente!");
        close();
      }

      if (resp.status !== 200) {
        dispatch(loadingUpdateTypeServiceApi(false));
        toastAlert(
          "error",
          "Ocurrrio un error al actualizar el tipo servicio, ya existe!"
        );
      }
    } catch (error) {
      dispatch(loadingUpdateTypeServiceApi(false));
      toastAlert("error", "Ocurrrio un error al actualiar el tipo servicio!");
    }
  };
};

export const removeTypeServiceApi = (data) => {
  return async (dispatch) => {
    //VERIFY CONNECTION
    // const isOnLine = await connectNetwork();
    // if (!isOnLine) {
    //   return;
    // }
    dispatch(loadingRemoveTypeServiceApi(true));

    try {
      const token = localStorage.getItem("token");
      let config = configApi(data, { method: "DELETE" }, token);

      let resp = await fetch(
        `${process.env.REACT_APP_API_URL}/register-settings/removeTypeService`,
        config
      );
      let response = await resp.json();

      if (resp.status === 200) {
        dispatch(removeTypeServiceSetting(response.doc));
        toastAlert("success", "El tipo servicio se elimino satisfactoriamente");
      }

      if (resp.status !== 200) {
        dispatch(loadingRemoveTypeServiceApi(false));
        toastAlert("error", "Ocurrrio un error al eliminar el tipo servicio!");
      }
    } catch (error) {
      dispatch(loadingRemoveTypeServiceApi(false));
      toastAlert("error", "Ocurrrio un error al eliminar el tipo servicio!");
    }
  };
};

export const searchSettingRolApi = ({ query, page, limit }) => {
  return async (dispatch) => {
    dispatch(loadingRolApi(true));
    dispatch(activeSearchSettingRolApi(true));
    try {
      const token = window.localStorage.getItem("token");
      let config = configApi(null, { method: "GET" }, token);

      let resp = await fetch(
        `${process.env.REACT_APP_API_URL}/register-settings/searchSettingRol/${page}/${limit}/${query}`,
        config
      );
      let response = await resp.json();

      if (resp.status === 200) {
        console.log(response);
        dispatch(searchRolSettingApi(response));
        dispatch(loadingRolApi(false));
      }

      if (resp.status !== 200) {
        toastAlert("error", response.error.msg ?? "Ocurrrio un error!");
        dispatch(loadingRolApi(false));
        dispatch(activeSearchSettingRolApi(false));
      }
    } catch (error) {
      dispatch(loadingRolApi(false));
      dispatch(activeSearchSettingRolApi(false));
      toastAlert("error", "Ocurrrio un error!");
    }
  };
};

export const searchSettingCargosApi = ({ query, page, limit }) => {
  return async (dispatch) => {
    dispatch(loadingCargosApi(true));
    dispatch(activeSearchSettingCargoApi(true));
    try {
      const token = window.localStorage.getItem("token");
      let config = configApi(null, { method: "GET" }, token);

      let resp = await fetch(
        `${process.env.REACT_APP_API_URL}/register-settings/searchSettingCargos/${page}/${limit}/${query}`,
        config
      );
      let response = await resp.json();

      if (resp.status === 200) {
        console.log(response);
        dispatch(searchCargoSettingApi(response));
        dispatch(loadingCargosApi(false));
      }

      if (resp.status !== 200) {
        toastAlert("error", response.error.msg ?? "Ocurrrio un error!");
        dispatch(loadingCargosApi(false));
        dispatch(activeSearchSettingCargoApi(false));
      }
    } catch (error) {
      dispatch(loadingCargosApi(false));
      dispatch(activeSearchSettingCargoApi(false));
      toastAlert("error", "Ocurrrio un error!");
    }
  };
};

export const searchSettingTeamApi = ({ query, page, limit }) => {
  return async (dispatch) => {
    dispatch(loadingTeamApi(true));
    dispatch(activeSearchSettingTeamApi(true));
    try {
      const token = window.localStorage.getItem("token");
      let config = configApi(null, { method: "GET" }, token);

      let resp = await fetch(
        `${process.env.REACT_APP_API_URL}/register-settings/searchSettingTeam/${page}/${limit}/${query}`,
        config
      );
      let response = await resp.json();

      if (resp.status === 200) {
        console.log(response);
        dispatch(searchTeamSettingApi(response));
        dispatch(loadingTeamApi(false));
      }

      if (resp.status !== 200) {
        toastAlert("error", response.error.msg ?? "Ocurrrio un error!");
        dispatch(loadingTeamApi(false));
        dispatch(activeSearchSettingTeamApi(false));
      }
    } catch (error) {
      dispatch(loadingTeamApi(false));
      dispatch(activeSearchSettingTeamApi(false));
      toastAlert("error", "Ocurrrio un error!");
    }
  };
};

export const searchSettingTypeTeamApi = ({ query, page, limit }) => {
  return async (dispatch) => {
    dispatch(loadingTypeTeamApi(true));
    dispatch(activeSearchSettingTypeTeamApi(true));
    try {
      const token = window.localStorage.getItem("token");
      let config = configApi(null, { method: "GET" }, token);

      let resp = await fetch(
        `${process.env.REACT_APP_API_URL}/register-settings/searchSettingTypeTeam/${page}/${limit}/${query}`,
        config
      );
      let response = await resp.json();

      if (resp.status === 200) {
        console.log(response);
        dispatch(searchTypeTeamSettingApi(response));
        dispatch(loadingTypeTeamApi(false));
      }

      if (resp.status !== 200) {
        toastAlert("error", response.error.msg ?? "Ocurrrio un error!");
        dispatch(loadingTypeTeamApi(false));
        dispatch(activeSearchSettingTypeTeamApi(false));
      }
    } catch (error) {
      dispatch(loadingTypeTeamApi(false));
      dispatch(activeSearchSettingTypeTeamApi(false));
      toastAlert("error", "Ocurrrio un error!");
    }
  };
};

export const searchSettingDependencyApi = ({ query, page, limit }) => {
  return async (dispatch) => {
    dispatch(loadingDependencyApi(true));
    dispatch(activeSearchSettingDependencyApi(true));
    try {
      const token = window.localStorage.getItem("token");
      let config = configApi(null, { method: "GET" }, token);

      let resp = await fetch(
        `${process.env.REACT_APP_API_URL}/register-settings/searchSettingDependency/${page}/${limit}/${query}`,
        config
      );
      let response = await resp.json();

      if (resp.status === 200) {
        console.log(response);
        dispatch(searchDependencySettingApi(response));
        dispatch(loadingDependencyApi(false));
      }

      if (resp.status !== 200) {
        toastAlert("error", response.error.msg ?? "Ocurrrio un error!");
        dispatch(loadingDependencyApi(false));
        dispatch(activeSearchSettingDependencyApi(false));
      }
    } catch (error) {
      dispatch(loadingDependencyApi(false));
      dispatch(activeSearchSettingDependencyApi(false));
      toastAlert("error", "Ocurrrio un error!");
    }
  };
};

export const searchSettingTypeServiceApi = ({ query, page, limit }) => {
  return async (dispatch) => {
    dispatch(loadingTypeServiceApi(true));
    dispatch(activeSearchSettingTypeServiceApi(true));
    try {
      const token = window.localStorage.getItem("token");
      let config = configApi(null, { method: "GET" }, token);

      let resp = await fetch(
        `${process.env.REACT_APP_API_URL}/register-settings/searchSettingTypeService/${page}/${limit}/${query}`,
        config
      );
      let response = await resp.json();

      if (resp.status === 200) {
        console.log(response);
        dispatch(searchTypeServiceSettingApi(response));
        dispatch(loadingTypeServiceApi(false));
      }

      if (resp.status !== 200) {
        toastAlert("error", response.error.msg ?? "Ocurrrio un error!");
        dispatch(loadingTypeServiceApi(false));
        dispatch(activeSearchSettingTypeServiceApi(false));
      }
    } catch (error) {
      dispatch(loadingTypeServiceApi(false));
      dispatch(activeSearchSettingTypeServiceApi(false));
      toastAlert("error", "Ocurrrio un error!");
    }
  };
};

export const loadingCargosApi = (data) => ({
  type: types.loadingSettingsUserCargos,
  payload: data,
});

export const cargoSetting = (data) => ({
  type: types.settingsUserCargos,
  payload: data,
});

export const loadingCreateCargosApi = (data) => ({
  type: types.loadingCreateSettingCargo,
  payload: data,
});

export const createCargoSetting = (data) => ({
  type: types.createSettingCargo,
  payload: data,
});

export const loadingUpdateCargosApi = (data) => ({
  type: types.loadingUpdateSettingCargo,
  payload: data,
});

export const updateCargoSetting = (data) => ({
  type: types.updateSettingCargo,
  payload: data,
});

export const loadingRemoveCargosApi = (data) => ({
  type: types.loadingRemoveSettingCargo,
  payload: data,
});

export const removeCargoSetting = (data) => ({
  type: types.removeSettingCargo,
  payload: data,
});

// rol

export const loadingRolApi = (data) => ({
  type: types.loadingSettingsUserRol,
  payload: data,
});

export const rolSetting = (data) => ({
  type: types.settingsUserRol,
  payload: data,
});

export const loadingCreateRolApi = (data) => ({
  type: types.loadingCreateSettingRol,
  payload: data,
});

export const createRolSetting = (data) => ({
  type: types.createSettingRol,
  payload: data,
});

export const loadingUpdateRolApi = (data) => ({
  type: types.loadingUpdateSettingRol,
  payload: data,
});

export const updateRolSetting = (data) => ({
  type: types.updateSettingRol,
  payload: data,
});

export const loadingRemoveRolApi = (data) => ({
  type: types.loadingRemoveSettingRol,
  payload: data,
});

export const removeRolSetting = (data) => ({
  type: types.removeSettingRol,
  payload: data,
});

// dependency

export const loadingDependencyApi = (data) => ({
  type: types.loadingSettingsUserDependency,
  payload: data,
});

export const dependencySetting = (data) => ({
  type: types.settingsUserDependency,
  payload: data,
});

export const loadingCreateDependencyApi = (data) => ({
  type: types.loadingCreateSettingDependency,
  payload: data,
});

export const createDependencySetting = (data) => ({
  type: types.createSettingDependency,
  payload: data,
});

export const loadingUpdateDependencyApi = (data) => ({
  type: types.loadingUpdateSettingDependency,
  payload: data,
});

export const updateDependencySetting = (data) => ({
  type: types.updateSettingDependency,
  payload: data,
});

export const loadingRemoveDependencyApi = (data) => ({
  type: types.loadingRemoveSettingDependency,
  payload: data,
});

export const removeDependencySetting = (data) => ({
  type: types.removeSettingDependency,
  payload: data,
});

// team

export const loadingTeamApi = (data) => ({
  type: types.loadingSettingsUserTeam,
  payload: data,
});

export const teamSetting = (data) => ({
  type: types.settingsUserTeam,
  payload: data,
});

export const loadingCreateTeamApi = (data) => ({
  type: types.loadingCreateSettingTeam,
  payload: data,
});

export const createTeamSetting = (data) => ({
  type: types.createSettingTeam,
  payload: data,
});

export const loadingUpdateTeamApi = (data) => ({
  type: types.loadingUpdateSettingTeam,
  payload: data,
});

export const updateTeamSetting = (data) => ({
  type: types.updateSettingTeam,
  payload: data,
});

export const loadingRemoveTeamApi = (data) => ({
  type: types.loadingRemoveSettingTeam,
  payload: data,
});

export const removeTeamSetting = (data) => ({
  type: types.removeSettingTeam,
  payload: data,
});

// type team

export const loadingTypeTeamApi = (data) => ({
  type: types.loadingSettingsUserTypeTeam,
  payload: data,
});

export const typeTeamSetting = (data) => ({
  type: types.settingsUserTypeTeam,
  payload: data,
});

export const loadingCreateTypeTeamApi = (data) => ({
  type: types.loadingCreateSettingTypeTeam,
  payload: data,
});

export const createTypeTeamSetting = (data) => ({
  type: types.createSettingTypeTeam,
  payload: data,
});

export const loadingUpdateTypeTeamApi = (data) => ({
  type: types.loadingUpdateSettingTypeTeam,
  payload: data,
});

export const updateTypeTeamSetting = (data) => ({
  type: types.updateSettingTypeTeam,
  payload: data,
});

export const loadingRemoveTypeTeamApi = (data) => ({
  type: types.loadingRemoveSettingTypeTeam,
  payload: data,
});

export const removeTypeTeamSetting = (data) => ({
  type: types.removeSettingTypeTeam,
  payload: data,
});

export const searchRolSettingApi = (data) => ({
  type: types.searchSettingUserRol,
  payload: data,
});

export const activeSearchSettingRolApi = (data) => ({
  type: types.activeSearchRolSetting,
  payload: data,
});

export const searchCargoSettingApi = (data) => ({
  type: types.searchSettingUserCargo,
  payload: data,
});

export const activeSearchSettingCargoApi = (data) => ({
  type: types.activeSearchCargoSetting,
  payload: data,
});

export const searchDependencySettingApi = (data) => ({
  type: types.searchSettingUserDependency,
  payload: data,
});

export const activeSearchSettingDependencyApi = (data) => ({
  type: types.activeSearchDependencySetting,
  payload: data,
});

export const searchTeamSettingApi = (data) => ({
  type: types.searchSettingUserTeam,
  payload: data,
});

export const activeSearchSettingTeamApi = (data) => ({
  type: types.activeSearchTeamSetting,
  payload: data,
});

export const searchTypeTeamSettingApi = (data) => ({
  type: types.searchSettingUserTypeTeam,
  payload: data,
});

export const activeSearchSettingTypeTeamApi = (data) => ({
  type: types.activeSearchTypeTeamSetting,
  payload: data,
});

export const textSearchSettingApi = (data) => ({
  type: types.textSearchSetting,
  payload: data,
});

// type servics
export const searchTypeServiceSettingApi = (data) => ({
  type: types.searchSettingUserTypeService,
  payload: data,
});

export const activeSearchSettingTypeServiceApi = (data) => ({
  type: types.activeSearchTypeServiceSetting,
  payload: data,
});

export const loadingTypeServiceApi = (data) => ({
  type: types.loadingSettingsUserTypeService,
  payload: data,
});

export const removeTypeServiceSetting = (data) => ({
  type: types.removeSettingTypeService,
  payload: data,
});

export const loadingRemoveTypeServiceApi = (data) => ({
  type: types.loadingRemoveSettingTypeService,
  payload: data,
});

export const createTypeServiceSetting = (data) => ({
  type: types.createSettingTypeService,
  payload: data,
});

export const loadingUpdateTypeServiceApi = (data) => ({
  type: types.loadingUpdateSettingTypeService,
  payload: data,
});

export const updateTypeServiceSetting = (data) => ({
  type: types.updateSettingTypeService,
  payload: data,
});

export const  typeServiceSetting = (data) => ({
  type: types.settingsUserTypeService,
  payload: data,
});

export const loadingCreateTypeServiceApi = (data) => ({
  type: types.loadingCreateSettingTypeService,
  payload: data,
});