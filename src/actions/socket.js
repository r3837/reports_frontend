import { types } from "../types/types";

export const imboxUpdateSocket = (data) => ({
  type: types.imboxUpdateSocket,
  payload: data,
});

export const requestImboxSocket = (data) => ({
  type: types.requestImboxSocket,
  payload: data,
});

export const createImboxSocket = (data) => ({
  type: types.createImbox,
  payload: data,
});

export const createObservationSocket = (data) => ({
  type: types.createObservation,
  payload: data,
});

