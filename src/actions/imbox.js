import { types } from "../types/types";
import { configApi } from "../helpers/configApi";
import { toastAlert } from "../helpers/alertToast";

// export const createImboxApi = (data) => {
//   return async (dispatch) => {
//     //VERIFY CONNECTION
//     // const isOnLine = await connectNetwork();
//     // if (!isOnLine) {
//     //   return;
//     // }
//     dispatch(loadingCreateImboxApi());

//     try {
//       const token = window.localStorage.getItem("token");
//       let config = configApi(data, { method: "POST" }, token);

//       let resp = await fetch(
//         `${process.env.REACT_APP_API_URL}/imbox/createImbox`,
//         config
//       );

//       let response = await resp.json();

//       console.log(response);

//       if (resp.status === 200) {
//         dispatch(createImbox(response.doc));
//       }
//     } catch (error) {
//       dispatch(loadingCreateImboxApi(false));
//     }
//   };
// };

// Para miembros solicitantes
export const requestObservationImboxApi = ({ page, limit }) => {
  return async (dispatch) => {
    //VERIFY CONNECTION
    // const isOnLine = await connectNetwork();
    // if (!isOnLine) {
    //   return;
    // }
    try {
      const token = window.localStorage.getItem("token");
      let config = configApi(null, { method: "GET" }, token);

      let resp = await fetch(
        `${process.env.REACT_APP_API_URL}/imbox/requestObervationImbox/${page}/${limit}`,
        config
      );
      let response = await resp.json();

      if (resp.status === 200) {
        dispatch(requestImbox(response));
      }
    } catch (error) {
      dispatch(loadingRequestImboxApi(false));
    }
  };
};

// Para miembros responsables
export const requesImboxApi = ({ page, limit }) => {
  return async (dispatch) => {
    //VERIFY CONNECTION
    // const isOnLine = await connectNetwork();
    // if (!isOnLine) {
    //   return;
    // }

    try {
      const token = window.localStorage.getItem("token");
      let config = configApi(null, { method: "GET" }, token);

      let resp = await fetch(
        `${process.env.REACT_APP_API_URL}/imbox/requestImbox/${page}/${limit}`,
        config
      );
      let response = await resp.json();

      if (resp.status === 200) {
        dispatch(requestImbox(response));
      }
    } catch (error) {
      dispatch(loadingRequestImboxApi(false));
    }
  };
};

export const sendImboxApi = ({ page, limit }) => {
  return async (dispatch) => {
    //VERIFY CONNECTION
    // const isOnLine = await connectNetwork();
    // if (!isOnLine) {
    //   return;
    // }
    dispatch(loadingSendImboxApi());

    try {
      const token = window.localStorage.getItem("token");
      let config = configApi(null, { method: "GET" }, token);

      let resp = await fetch(
        `${process.env.REACT_APP_API_URL}/imbox/sendImbox/${page}/${limit}`,
        config
      );
      let response = await resp.json();

      if (resp.status === 200) {
        dispatch(sendImbox(response));
      }
    } catch (error) {
      dispatch(loadingSendImboxApi(false));
    }
  };
};

export const viewMessageApi = ({ id }) => {
  return async (dispatch) => {
    //VERIFY CONNECTION
    // const isOnLine = await connectNetwork();
    // if (!isOnLine) {
    //   return;
    // }
    dispatch(loadingViewMessage());

    try {
      const token = window.localStorage.getItem("token");
      let config = configApi(null, { method: "GET" }, token);

      let resp = await fetch(
        `${process.env.REACT_APP_API_URL}/imbox/viewMessage/${id}`,
        config
      );
      let response = await resp.json();

      if (resp.status === 200) {
        console.log(response.doc);
        dispatch(viewMessage(response.doc));
      }
    } catch (error) {
      dispatch(loadingViewMessage(false));
    }
  };
};

export const imboxObservationsApi = ({ id, page, limit }) => {
  return async (dispatch) => {
    //VERIFY CONNECTION
    // const isOnLine = await connectNetwork();
    // if (!isOnLine) {
    //   return;
    // }
    dispatch(loadingObservation());

    try {
      const token = window.localStorage.getItem("token");
      let config = configApi(null, { method: "GET" }, token);

      let resp = await fetch(
        `${process.env.REACT_APP_API_URL}/imbox/imboxObservations/${page}/${limit}/${id}`,
        config
      );
      let response = await resp.json();

      if (resp.status === 200) {
        console.log(response);
        dispatch(imboxObservations(response));
      }
    } catch (error) {
      dispatch(loadingObservation(false));
    }
  };
};

export const sendImboxObservationsApi = (data) => {
  return async (dispatch) => {
    //VERIFY CONNECTION
    // const isOnLine = await connectNetwork();
    // if (!isOnLine) {
    //   return;
    // }
    dispatch(loadingCreateObservation(true));
    try {
      const token = window.localStorage.getItem("token");
      let config = configApi(data, { method: "POST" }, token);

      let resp = await fetch(
        `${process.env.REACT_APP_API_URL}/imbox/sendImboxObservations`,
        config
      );
      let response = await resp.json();

      if (resp.status === 200) {
        dispatch(createObservation(response.doc));
      }

      if (resp.status === 500 || resp.status === 404 || resp.status === 401) {
        toastAlert("error", response?.error?.msg ?? "Ocurrio un error!");
      }
    } catch (error) {
      dispatch(loadingCreateObservation(false));
    }
  };
};

export const requestImboxRemoveApi = ({ solicitud_id }) => {
  return async (dispatch) => {
    try {
      const token = window.localStorage.getItem("token");
      let config = configApi(null, { method: "GET" }, token);

      let resp = await fetch(
        `${process.env.REACT_APP_API_URL}/imbox/requestImboxRemove/${solicitud_id}`,
        config
      );
      let response = await resp.json();

      if (resp.status === 200) {
        dispatch(requestImboxRemove({ solicitud_id }));
        toastAlert(
          "success",
          response.doc ?? "El mensaje se elimino satisfactoriamente"
        );
      }

      if (resp.status === 500 || resp.status === 400 || resp.status === 404) {
        toastAlert("success", response.error.msg ?? "Ocurrrio un error!");
      }
    } catch (error) {
      toastAlert("success", "Ocurrrio un error!");
    }
  };
};

export const requestImboxRemoveAllApi = ({ solicitud_id }) => {
  return async (dispatch) => {
    try {
      const token = window.localStorage.getItem("token");
      let config = configApi(null, { method: "GET" }, token);

      let resp = await fetch(
        `${process.env.REACT_APP_API_URL}/imbox/requestImboxRemoveAll/${solicitud_id}`,
        config
      );
      let response = await resp.json();

      if (resp.status === 200) {
        dispatch(requestImboxRemoveAll({ solicitud_id }));
        toastAlert(
          "success",
          response.doc ?? "El mensaje se elimino satisfactoriamente"
        );
      }

      if (resp.status === 500 || resp.status === 400 || resp.status === 404) {
        toastAlert("success", response.error.msg ?? "Ocurrrio un error!");
      }
    } catch (error) {
      toastAlert("success", "Ocurrrio un error!");
    }
  };
};

export const searchImboxApi = ({ query, page, limit }) => {
  return async (dispatch) => {
    dispatch(loadingRequestImboxApi(true));
    dispatch(activeSearchApi(true));
    try {
      const token = window.localStorage.getItem("token");
      let config = configApi(null, { method: "GET" }, token);

      let resp = await fetch(
        `${process.env.REACT_APP_API_URL}/imbox/SearchImboxApi/${page}/${limit}/${query}`,
        config
      );
      let response = await resp.json();

      if (resp.status === 200) {
        console.log(response);
        dispatch(searchImbox(response));
        dispatch(loadingRequestImboxApi(false));
      }

      if (resp.status !== 200) {
        toastAlert("error", response.error.msg ?? "Ocurrrio un error!");
        dispatch(loadingRequestImboxApi(false));
        dispatch(activeSearchApi(false));
      }
    } catch (error) {
      dispatch(loadingRequestImboxApi(false));
      dispatch(activeSearchApi(false));
      toastAlert("error", "Ocurrrio un error!");
    }
  };
};

export const searchSolicitanteImboxApi = ({ query, page, limit }) => {
  return async (dispatch) => {
    dispatch(loadingRequestImboxApi(true));
    dispatch(activeSearchApi(true));
    try {
      const token = window.localStorage.getItem("token");
      let config = configApi(null, { method: "GET" }, token);

      let resp = await fetch(
        `${process.env.REACT_APP_API_URL}/imbox/SearchSolicitanteImboxApi/${page}/${limit}/${query}`,
        config
      );
      let response = await resp.json();

      if (resp.status === 200) {
        console.log(response);
        dispatch(searchImbox(response));
        dispatch(loadingRequestImboxApi(false));
      }

      if (resp.status !== 200) {
        toastAlert("error", response.error.msg ?? "Ocurrrio un error!");
        dispatch(loadingRequestImboxApi(false));
        dispatch(activeSearchApi(false));
      }
    } catch (error) {
      dispatch(loadingRequestImboxApi(false));
      dispatch(activeSearchApi(false));
      toastAlert("error", "Ocurrrio un error!");
    }
  };
};

export const searchSendImboxApi = ({ query, page, limit }) => {
  return async (dispatch) => {
    dispatch(loadingSendImboxApi(true));
    dispatch(activeSearchSendApi(true));
    try {
      const token = window.localStorage.getItem("token");
      let config = configApi(null, { method: "GET" }, token);

      let resp = await fetch(
        `${process.env.REACT_APP_API_URL}/imbox/searchSolicitanteSendImboxApi/${page}/${limit}/${query}`,
        config
      );
      let response = await resp.json();

      if (resp.status === 200) {
        console.log(response);
        dispatch(searchSendImbox(response));
        dispatch(loadingSendImboxApi(false));
      }

      if (resp.status !== 200) {
        toastAlert("error", response.error.msg ?? "Ocurrrio un error!");
        dispatch(loadingSendImboxApi(false));
        dispatch(activeSearchSendApi(false));
      }
    } catch (error) {
      dispatch(loadingSendImboxApi(false));
      dispatch(activeSearchSendApi(false));
      toastAlert("error", "Ocurrrio un error!");
    }
  };
};

export const requestImboxAllApi = () => {
  return async (dispatch) => {
    //VERIFY CONNECTION
    // const isOnLine = await connectNetwork();
    // if (!isOnLine) {
    //   return;
    // }
    dispatch(loadingRequestImboxAll(true));
    try {
      const token = window.localStorage.getItem("token");
      let config = configApi(null, { method: "GET" }, token);

      let resp = await fetch(
        `${process.env.REACT_APP_API_URL}/imbox/requestImboxAll`,
        config
      );
      let response = await resp.json();

      if (resp.status === 200) {
        dispatch(requestImboxAll(response.docs));
      }

      if (resp.status !== 200) {
        dispatch(loadingRequestImboxAll(false));
      }
    } catch (error) {
      dispatch(loadingRequestImboxAll(false));
    }
  };
};

export const loadingCreateImboxApi = (data) => ({
  type: types.loadingCreateImbox,
  payload: data,
});

export const loadingSendImboxApi = (data) => ({
  type: types.loadingSendImbox,
  payload: data,
});

export const loadingRequestImboxApi = (data) => ({
  type: types.loadingRequestImbox,
  payload: data,
});

export const requestImbox = (data) => ({
  type: types.requestImbox,
  payload: data,
});

export const sendImbox = (data) => ({
  type: types.sendImbox,
  payload: data,
});

export const loadingViewMessage = (data) => ({
  type: types.loadingViewMessage,
  payload: data,
});

export const viewMessage = (data) => ({
  type: types.viewMessage,
  payload: data,
});

export const loadingObservation = (data) => ({
  type: types.loadingObservation,
  payload: data,
});

export const imboxObservations = (data) => ({
  type: types.imboxObservations,
  payload: data,
});

export const loadingCreateObservation = (data) => ({
  type: types.loadingCreateObservation,
  payload: data,
});

export const createObservation = (data) => ({
  type: types.createObservation,
  payload: data,
});

export const requestImboxRemove = (data) => ({
  type: types.requestImboxRemove,
  payload: data,
});

export const requestImboxRemoveAll = (data) => ({
  type: types.requestImboxRemoveAll,
  payload: data,
});

export const renderMessageId = (data) => ({
  type: types.renderMessageId,
  payload: data,
});

export const imboxObservationsReset = () => ({
  type: types.imboxObservationsReset,
});

export const activeSearchApi = (data) => ({
  type: types.activeSearch,
  payload: data,
});

export const activeSearchSendApi = (data) => ({
  type: types.activeSearchSend,
  payload: data,
});

export const searchImbox = (data) => ({
  type: types.searchImbox,
  payload: data,
});

export const searchSendImbox = (data) => ({
  type: types.searchSendImbox,
  payload: data,
});

export const requestImboxAll = (data) => ({
  type: types.requestImboxAll,
  payload: data,
});

export const loadingRequestImboxAll = (data) => ({
  type: types.loadingRequestImboxAll,
  payload: data,
});
