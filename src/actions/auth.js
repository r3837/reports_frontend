import { types } from "../types/types";
import { configApi } from "../helpers/configApi";
import { toastAlert } from "../helpers/alertToast";

export const loginApi = (data) => {
  return async (dispatch) => {
    //VERIFY CONNECTION
    // const isOnLine = await connectNetwork();
    // if (!isOnLine) {
    //   return;
    // }
    dispatch(reLoadLogged());

    try {
      let config = configApi(data, { method: "POST" }, null, false);
      let resp = await fetch(
        `${process.env.REACT_APP_API_URL}/auth/signin`,
        config
      );
      let response = await resp.json();

      if (resp.status === 200) {
        response.token && window.localStorage.setItem("token", response.token);
        dispatch(login(response.doc));
      }

      if (resp.status !== 200) {
        dispatch(reLoadLogged(false));
        toastAlert("error", response?.error?.msg);
      }
    } catch (error) {
      toastAlert("error", "Ocurrio un error!");
    }
  };
};

export const forgotPasswordApi = (data) => {
  return async (dispatch) => {
    //VERIFY CONNECTION
    // const isOnLine = await connectNetwork();
    // if (!isOnLine) {
    //   return;
    // }
    try {
      let config = configApi(data, { method: "POST" }, null);
      let resp = await fetch(
        `${process.env.REACT_APP_API_URL}/auth/forgotPassword`,
        config
      );
      let response = await resp.json();
      if (resp.status === 200) {
        dispatch(resetNavegateLogin(true));
        toastAlert("success", response.msg);
      }
    } catch (error) {}
  };
};

export const changePasswordApi = (data, token) => {
  return async (dispatch) => {
    //VERIFY CONNECTION
    // const isOnLine = await connectNetwork();
    // if (!isOnLine) {
    //   return;
    // }
    try {
      let config = configApi(data, { method: "POST" }, token);
      let resp = await fetch(
        `${process.env.REACT_APP_API_URL}/auth/changePassword`,
        config
      );
      let response = await resp.json();

      if (resp.status === 200) {
        dispatch(resetNavegateLogin(true));
        toastAlert("success", response.msg);
      }
    } catch (error) {
      console.log(error);
    }
  };
};

export const isLoggedInApi = (data) => {
  return async (dispatch) => {
    //VERIFY CONNECTION
    // const isOnLine = await connectNetwork();
    // if (!isOnLine) {
    //   return;
    // }
    dispatch(reLoadLogged());

    try {
      let config = configApi(null, { method: "GET" }, data);

      let resp = await fetch(
        `${process.env.REACT_APP_API_URL}/auth/isLogged`,
        config
      );
      let response = await resp.json();

      if (resp.status === 200) {
        response.token && window.localStorage.setItem("token", response.token);
        dispatch(isLogged(response.doc));
      }

      if (resp.status !== 200) {
        window.localStorage?.removeItem("token");
        dispatch(reLoadLogged(false));
      }
    } catch (error) {
      window.localStorage?.removeItem("token");
      dispatch(reLoadLogged(false));
    }
  };
};

export const logoutApi = () => {
  return async (dispatch) => {
    try {
      window.localStorage?.removeItem("token");
      dispatch(logout());
    } catch (error) {}
  };
};

export const login = (user) => ({
  type: types.login,
  payload: user,
});

export const reLoadLogged = (data) => ({
  type: types.isLogged,
  payload: data,
});

export const isLogged = (user) => ({
  type: types.login,
  payload: user,
});

export const logout = () => ({
  type: types.logout,
});

export const resetNavegateLogin = (data) => ({
  type: types.resetNavegateLogin,
  payload: data,
});
