import { types } from "../types/types";
import { configApi } from "../helpers/configApi";
import { toastAlert } from "../helpers/alertToast";

export const getRegisteredUsersApi = ({ page, limit }) => {
  return async (dispatch) => {
    //VERIFY CONNECTION
    // const isOnLine = await connectNetwork();
    // if (!isOnLine) {
    //   return;
    // }

    dispatch(loadingUser(true));

    try {
      const token = localStorage.getItem("token");

      console.log(token);
      let config = configApi(null, { method: "POST" }, token);

      let resp = await fetch(
        `${process.env.REACT_APP_API_URL}/registered-users/getRegisteredUsers/${page}/${limit}`,
        config
      );
      let response = await resp.json();

      if (resp.status === 200) {
        dispatch(registeredUser(response));
        dispatch(loadingUser(false));
      }

      if (resp.status === 500 || resp.status === 404) {
        dispatch(loadingUser(false));
      }
    } catch (error) {
      dispatch(loadingUser(false));
      console.log(error);
    }
  };
};

export const addNewUserApi = (data, close) => {
  return async (dispatch) => {
    //VERIFY CONNECTION
    // const isOnLine = await connectNetwork();
    // if (!isOnLine) {
    //   return;
    // }

    dispatch(loadingRegister(true));

    try {
      const token = localStorage.getItem("token");
      let config = configApi(data, { method: "POST" }, token);
      let resp = await fetch(
        `${process.env.REACT_APP_API_URL}/registered-users/addNewUser`,
        config
      );
      let response = await resp.json();

      if (resp.status === 200) {
        dispatch(addNewUser(response.doc));
        toastAlert("success", "Usuario registrado...");
        close();
        dispatch(loadingRegister(false));
      }

      if (resp.status !== 200) {
        dispatch(loadingRegister(false));
        toastAlert("error", response?.error?.msg);
      }
    } catch (error) {
      dispatch(loadingRegister(false));
      toastAlert("error", "Ocurrio un error!");
    }
  };
};

export const saveUserApi = (data, close) => {
  return async (dispatch) => {
    //VERIFY CONNECTION
    // const isOnLine = await connectNetwork();
    // if (!isOnLine) {
    //   return;
    // }

    dispatch(loadingUpdateUser(true));

    try {
      const token = localStorage.getItem("token");
      let config = configApi(data, { method: "PUT" }, token);
      let resp = await fetch(
        `${process.env.REACT_APP_API_URL}/registered-users/updateUser`,
        config
      );
      let response = await resp.json();

      if (resp.status === 200) {
        dispatch(updateUser(response.doc));
        toastAlert("success", "Usuario actualizado...");
        close();
        dispatch(loadingUpdateUser(false));
      }

      if (resp.status !== 200) {
        dispatch(loadingUpdateUser(false));
        toastAlert(
          "error",
          response?.error?.msg ??
            "Ocurrio un error, verifica los campos obligatorios"
        );
      }
    } catch (error) {
      dispatch(loadingUpdateUser(false));
      toastAlert("error", "Ocurrio un error!");
    }
  };
};

export const removeUserByIdApi = (data) => {
  return async (dispatch) => {
    //VERIFY CONNECTION
    // const isOnLine = await connectNetwork();
    // if (!isOnLine) {
    //   return;
    // }

    dispatch(loadingRemoveUser(true));

    try {
      const token = localStorage.getItem("token");
      let config = configApi(data, { method: "POST" }, token);
      let resp = await fetch(
        `${process.env.REACT_APP_API_URL}/registered-users/removeUser`,
        config
      );
      let response = await resp.json();

      if (resp.status === 200) {
        dispatch(removeUser(response.doc));
        toastAlert("success", "Usuario se elimino correctamente...");
      }

      if (resp.status !== 200) {
        dispatch(loadingRemoveUser(false));
        toastAlert("error", response?.error?.msg ?? "Ocurrio un error");
      }
    } catch (error) {
      dispatch(loadingRemoveUser(false));
      toastAlert("error", "Ocurrio un error!");
    }
  };
};


export const searchUsersApi = ({ query, page, limit }) => {
  return async (dispatch) => {
    dispatch(loadingUser(true));
    dispatch(activeSearchUsersApi(true));
    try {
      const token = window.localStorage.getItem("token");
      let config = configApi(null, { method: "GET" }, token);

      let resp = await fetch(
        `${process.env.REACT_APP_API_URL}/registered-users/SearchUsersApi/${page}/${limit}/${query}`,
        config
      );
      let response = await resp.json();

      if (resp.status === 200) {
        console.log(response);
        dispatch(searchUsers(response));
        dispatch(loadingUser(false));
      }

      if (resp.status !== 200) {
        toastAlert("error", response.error.msg ?? "Ocurrrio un error!");
        dispatch(loadingUser(false));
        dispatch(activeSearchUsersApi(false));
      }
    } catch (error) {
      dispatch(loadingUser(false));
      dispatch(activeSearchUsersApi(false));
      toastAlert("error", "Ocurrrio un error!");
    }
  };
};



export const loadingUser = (data) => ({
  type: types.loadingUser,
  payload: data,
});

export const registeredUser = (data) => ({
  type: types.registeredUser,
  payload: data,
});

export const loadingRegister = (data) => ({
  type: types.loadingRegister,
  payload: data,
});

export const addNewUser = (data) => ({
  type: types.addUser,
  payload: data,
});

export const loadingUpdateUser = (data) => ({
  type: types.loadingUpdateUser,
  payload: data,
});

export const updateUser = (data) => ({
  type: types.updateUser,
  payload: data,
});

export const loadingRemoveUser = (data) => ({
  type: types.loadingRemoveUser,
  payload: data,
});

export const removeUser = (data) => ({
  type: types.removeUser,
  payload: data,
});

export const searchUsers = (data) => ({
  type: types.searchUsers,
  payload: data,
});

export const activeSearchUsersApi = (data) => ({
  type: types.activeSearchUsers,
  payload: data,
});


