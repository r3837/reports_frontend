import { AddNewUser } from "../../components/add-user-new/AddNewUser";
import { IndicatorPage } from "../../components/indicator-page/IndicatorPage";
import { ModalCustom } from "../../components/modal-custom/ModalCustom";
import { PaginationOption } from "../../components/paginations/PaginationOption";
import { SpinnerLazyCustom } from "../../components/spinner/SpinnerLazyCustom";
import { TableRegister } from "../../components/table/TableCustom";
import { UpdateUser } from "../../components/update-user/UpdateUser";
import { useRegisterPage } from "../../hooks/pages/useRegisterPage";
import { registerTitleTable } from "./utils/registerCommon";
import "./RegisterPage.scss";
import { Form } from "react-bootstrap";

export const RegisterPage = () => {
  const {
    loading,
    docs,
    page,
    handlePaginationLeft,
    handlePaginationRight,
    handleRemoveMesageRequest,
    isActiveUser,
    show,
    handleClose,
    handleShow,
    showUpdate,
    handleCloseUpdate,
    handleShowUpdate,
    selectItem,
    setSelectItem,

    handleActiveSearch,
    activeSearch,
    handleSearch,
    handleSearchState
  } = useRegisterPage();

  return (
    <div className="main__RegistratedUser">
      <IndicatorPage title="Registrar" />
      <div className="header__users shadow-lg">
        <span> Usuarios </span>
        <div className="header__users__option">
          {activeSearch && (
            <Form.Control
              className="input__search"
              type="nro_cedula"
              placeholder="Buscar"
              onChange={handleSearchState}
              onKeyPress={handleSearch}
            />
          )}
          {activeSearch ? (
            <i
              className="bx bx-x icon__search"
              onClick={handleActiveSearch}
            ></i>
          ) : (
            <i
              className="bx bx-search-alt-2 icon__search"
              onClick={handleActiveSearch}
            ></i>
          )}
         
        </div>
        <div className="btn__register">
          <ModalCustom
            show={show}
            handleClose={handleClose}
            handleShow={handleShow}
            isButton={true}
            childen={<AddNewUser handleClose={handleClose} />}
          />
        </div>
      </div>
      {loading ? (
        <SpinnerLazyCustom style="index__page" />
      ) : docs.length > 0 ? (
        <div className="card main__RequestImbox__card shadow-sm">
          <TableRegister
            isActiveUser={isActiveUser}
            titleTable={registerTitleTable}
            data={docs}
            handleUpdate={(item) => {
              setSelectItem(item);
              handleShowUpdate();
            }}
            handleRemove={(id) => handleRemoveMesageRequest(id)}
          />
          <ModalCustom
            show={showUpdate}
            handleClose={handleCloseUpdate}
            handleShow={handleShowUpdate}
            isButton={false}
            childen={
              <UpdateUser item={selectItem} handleClose={handleCloseUpdate} />
            }
          />
        </div>
      ) : (
        <>
          <div className="main__RequestImbox__Empty">
            <span>No tienes usuarios registrados</span>
          </div>
          <div className="main__RequestImbox__dividir__Empty"></div>
        </>
      )}
      <PaginationOption
        page={page}
        onClickLeft={handlePaginationLeft}
        onClickRight={handlePaginationRight}
      />
    </div>
  );
};
