import { IndicatorPage } from "../../components/indicator-page/IndicatorPage";
import { useProfilePage } from "../../hooks/pages/useProfilePage";
import "./ProfilePage.scss";

export const ProfilePage = () => {
  const { user } = useProfilePage();
  console.log(user);

  return (
    <>
      <IndicatorPage title="Perfil" />
      <div className="card text-center">
        <div className="card-header header__profile shadow-lg " style={{ background: '#308efe', color: 'white', borderRadius: "5px" }}> {user.correo_electronico}</div>
        <div className="card-body">
          <img
            src="//ssl.gstatic.com/accounts/ui/avatar_2x.png"
            alt="profile-img"
            className="profile-img-card"
          />
          <h5 className="card-title">
            {user.nombres} {user.apellidos}
            <div className="nro__idt">CC: {user.nro_cedula}</div>
          </h5>
          <p className="card-text">
            Tel: {` ${user.nro_celular ?? "Sin registrar"} `}
          </p>
          <p className="card-text">
            Estado:
            {`  ${user.estado ? "Activo" : "Inactivo"}`}
          </p>
        </div>
        <div className="card-footer shadow" style={{ background: '#308efe', color: 'white', borderRadius: "5px" }}>{user?.rol}</div>
      </div>
    </>
  );
};
