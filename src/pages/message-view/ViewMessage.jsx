import React from "react";
import { BackNavigate } from "../../components/back-navegate/BackNavegate";
import { IndicatorPage } from "../../components/indicator-page/IndicatorPage";
import { MessagesViewObservation } from "../../components/view-mesage-imbox/MessagesViewObservation";
import { useViewMessage } from "../../hooks/pages/useViewMessage";
import Select from "react-select";
import "./ViewMessage.scss";

export const ViewMessage = () => {
  const {
    doc,
    loadingViewMessage,
    settingRequestStatus,
    onChangeStatus,
    visibilityStatus,
  } = useViewMessage();
  
  const customStyles = {
    control: (base) => ({
      ...base,
      border: "0 !important",
      // This line disable the blue border
      boxShadow: "0 !important",
      "&:hover": {
        border: "0 !important",
      },
    }),
  };

  return (
    <>
      {loadingViewMessage ? (
        <IndicatorPage />
      ) : (
        <>
          <div className="viewmessage">
            <div className="card shadow-sm">
              <div className="back">
                <BackNavigate color="color__ligh" />
              </div>
              {visibilityStatus && (
                <div className="select">
                  <Select
                    placeholder={`${doc?.estado ?? "Estado solicitud"}`}
                    options={settingRequestStatus ?? []}
                    styles={customStyles}
                    onChange={onChangeStatus}
                  />
                </div>
              )}
            </div>
          </div>
          <div className="main__viewmessage">
            <div className="main__viewmessage__detail">
              <span>
                <strong>Solicitud tipo</strong>
                {"  "} {doc?.tipo_servicio}
              </span>
              <div className="main__viewmessage__detail__content">
                <span className="main__viewmessage__detail__content__date">
                  {doc?.fecha_solicitud}
                </span>
                <div>
                  <span className="main__viewmessage__detail__content__equipo">
                    <span>Equipo:</span>
                    <strong>{doc?.nombre_equipo}</strong>
                  </span>
                </div>
              </div>
            </div>
            <div className="main__viewmessage__content">
              <strong>Asunto</strong>
              <p> {doc?.asunto} </p>
            </div>
            <div className="main__viewmessage__content__observation">
              <span>Observaciones</span>
            </div>
            <MessagesViewObservation />
          </div>
        </>
      )}
    </>
  );
};
