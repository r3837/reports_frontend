import { Form } from "react-bootstrap";
import { Link, Outlet } from "react-router-dom";
import { IndicatorPage } from "../../components/indicator-page/IndicatorPage";
import { useRegisterSettingsPage } from "../../hooks/pages/useRegisterSettingsPage";
import "./RegisterSettingsPage.scss";

export const RegisterSettingsPage = () => {
  const {
    handleSearchState,
    handleSearch,
    handleActiveSearch,
    activeSearch,
    curPathActive,
  } = useRegisterSettingsPage();

  return (
    <div className="main__Registrated__settings">
      <IndicatorPage title="Opciones del sistema" />
      <div className="header__imbox shadow-lg">
        <div className="header__imbox__cod">
          <div className="title__register__system">
            <span> Sistema </span>
          </div>
          <div className="btn__register">
            <div
              className={`${
                curPathActive === undefined ? "active_selected" : ""
              } `}
            >
              <Link
                className="btn__register__option"
                to="/registrar-datos-del-sistema"
              >
                <span>Rol</span>
                <i className="bx bx-plus-circle  "></i>
              </Link>
            </div>
            <div
              className={`${
                curPathActive === "dependencia-del-sistema"
                  ? "active_selected"
                  : ""
              } `}
            >
              <Link
                className="btn__register__option"
                to="/registrar-datos-del-sistema/dependencia-del-sistema"
              >
                <span>Dependencia</span>
                <i className="bx bx-plus-circle"></i>
              </Link>
            </div>
            <div
              className={`${
                curPathActive === "cargo-del-sistema" ? "active_selected" : ""
              } `}
            >
              <Link
                className="btn__register__option"
                to="/registrar-datos-del-sistema/cargo-del-sistema"
              >
                <span>Cargo</span>
                <i className="bx bx-plus-circle"></i>
              </Link>
            </div>
            <div
              className={`${
                curPathActive === "equipo-del-sistema" ? "active_selected" : ""
              } `}
            >
              <Link
                className="btn__register__option"
                to="/registrar-datos-del-sistema/equipo-del-sistema"
              >
                <span>Equipo</span>
                <i className="bx bx-plus-circle"></i>
              </Link>
            </div>
            <div
              className={`${
                curPathActive === "tipo-equipo-del-sistema"
                  ? "active_selected"
                  : ""
              } `}
            >
              <Link
                className="btn__register__option"
                to="/registrar-datos-del-sistema/tipo-equipo-del-sistema"
              >
                <span>Tipo Equipo</span>
                <i className="bx bx-plus-circle"></i>
              </Link>
            </div>
            <div
              className={`${
                curPathActive === "tipo-servicio-del-sistema"
                  ? "active_selected"
                  : ""
              } `}
            >
              <Link
                className="btn__register__option"
                to="/registrar-datos-del-sistema/tipo-servicio-del-sistema"
              >
                <span>Tipo Servicio</span>
                <i className="bx bx-plus-circle"></i>
              </Link>
            </div>
          </div>
          <div className="header__imbox__option">
            {activeSearch ? (
              <i
                className="bx bx-x icon__search"
                onClick={handleActiveSearch}
              ></i>
            ) : (
              <i
                className="bx bx-search-alt-2 icon__search"
                onClick={handleActiveSearch}
              ></i>
            )}
          </div>
        </div>
        <div className="search__cod">
          {activeSearch && (
            <Form.Control
              className="input__search"
              type="nro_cedula"
              placeholder="Buscar"
              onChange={handleSearchState}
              onKeyPress={handleSearch}
            />
          )}
        </div>
      </div>
      <div style={{ paddingRight: "5px", paddingLeft: "5px" }}>
        <Outlet />
      </div>
    </div>
  );
};
