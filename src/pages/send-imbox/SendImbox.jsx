import { Form } from "react-bootstrap";
import { IndicatorPage } from "../../components/indicator-page/IndicatorPage";
import { PaginationOption } from "../../components/paginations/PaginationOption";
import { SpinnerLazyCustom } from "../../components/spinner/SpinnerLazyCustom";
import { TableCustom } from "../../components/table/TableCustom";
import { useSendImboxPage } from "../../hooks/pages/useSendImboxPage";
import { typeWindowImbox } from "../../types/typeWiindosImbox";
import "./SendImbox.scss";

// titulos de la tabla
const titleTable = ["Mensajes", "Solicitud", "Estado", "Fecha", "Opciones"];

export const SendImboxPage = () => {
  const {
    docs,
    page,
    handlePaginationLeft,
    handlePaginationRight,
    handleNavegate,
    handleRemoveMesageRequest,
    handleSearchState,
    handleSearch,
    handleActiveSearch,
    activeSearch,
    loading
  } = useSendImboxPage();

  return (
    <div className="main__SendImbox">
      <IndicatorPage title="Solicitudes enviadas" />
      <div className="header__imbox shadow-lg">
        <span> Mensajes Enviados </span>
        <div className="header__imbox__option">
          {activeSearch && (
            <Form.Control
              className="input__search"
              type="nro_cedula"
              placeholder="Buscar"
              onChange={handleSearchState}
              onKeyPress={handleSearch}
            />
          )}
          {activeSearch ? (
            <i
              className="bx bx-x icon__search"
              onClick={handleActiveSearch}
            ></i>
          ) : (
            <i
              className="bx bx-search-alt-2 icon__search"
              onClick={handleActiveSearch}
            ></i>
          )}
        </div>
      </div>
      {loading ? (
        <SpinnerLazyCustom style="index__page" />
      ) : docs.length > 0 ? (
        <div className="card main__SendImbox__card shadow-sm">
          <TableCustom
            type={typeWindowImbox.send}
            titleTable={titleTable}
            handleNavegate={handleNavegate}
            handleRemove={(id) => handleRemoveMesageRequest(id)}
            data={docs}
          />
        </div>
      ) : (
        <>
          <div className="main__SendImbox__Empty">
            <span>No tienes solicitudes</span>
          </div>
          <div className="main__SendImbox__dividir__Empty"></div>
        </>
      )}
      <PaginationOption
        page={page}
        onClickLeft={handlePaginationLeft}
        onClickRight={handlePaginationRight}
      />
    </div>
  );
};
