import React from "react";
import { useDispatch } from "react-redux";
import { logoutApi } from "../../actions/auth";
import { IndicatorPage } from "../../components/indicator-page/IndicatorPage";
import "./OptionsPage.scss";

export const OptionsPage = () => {
  const dispatch = useDispatch();

  return (
    <>
      <IndicatorPage title="Opciones" />
      <div className="main_email">
        <div className="main_email_header">
          <span>ALCALDIA DE PATIA</span>
        </div>
        <div className="main_email_body">
           <p>Hola, para poder seguir con el proceso de cambio de contraseña, por favor dar click al siguiente link</p>
        </div>
        <div className="main_email_link">
          <a>http://localhost:3000/cambio-de-contrasena/eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c3VhcmlvX2lkIjoiMTMiLCJpYXQiOjE2NTMxMDM0MTMsImV4cCI6MTY1MzE4OTgxM30.tUqFn3GTEO94E2HN0-aIjtgx7qwFN2Flk3Vothl0ULg</a>
        </div>
        <div className="main_email_footer">
          <span>ALCALDIA DE PATIA</span>
        </div>
      </div>
    </>
  );
};
