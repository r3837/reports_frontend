import { Form } from "react-bootstrap";
import { ExportRequestXLSX } from "../../components/exports-request-xlsx/ExportRequestXLSX";
import { IndicatorPage } from "../../components/indicator-page/IndicatorPage";
import { ModalCustom } from "../../components/modal-custom/ModalCustom";
import { PaginationOption } from "../../components/paginations/PaginationOption";
import { SpinnerLazyCustom } from "../../components/spinner/SpinnerLazyCustom";
import { TableCustomRequest } from "../../components/table/TableCustom";
import { useRequestImboxPage } from "../../hooks/pages/useRequestImboxPage";
import { typeUserRol } from "../../types/typeUserRol";
import "./RequestImbox.scss";
import { titleTable } from "./utils";

export const RequestImboxPage = () => {
  const {
    loading,
    docs,
    handleNavegate,
    page,
    handlePaginationLeft,
    handlePaginationRight,
    handleRemoveMesageRequest,
    show,
    handleClose,
    handleShow,
    handleActiveSearch,
    activeSearch,
    handleSearch,
    handleSearchState,
    user,
  } = useRequestImboxPage();

  return (
    <div className="main__RequestImbox">
      <IndicatorPage title="Bandeja de entrada" />
      <div className="header__imbox shadow-lg">
        <span> Mensajes </span>
        <div className="header__imbox__option">
          {activeSearch && (
            <Form.Control
              className="input__search"
              type="nro_cedula"
              placeholder="Buscar"
              onChange={handleSearchState}
              onKeyPress={handleSearch}
            />
          )}
          {activeSearch ? (
            <i
              className="bx bx-x icon__search"
              onClick={handleActiveSearch}
            ></i>
          ) : (
            <i
              className="bx bx-search-alt-2 icon__search"
              onClick={handleActiveSearch}
            ></i>
          )}
          {(user?.rol !== typeUserRol.solicitante.description) &&
          (
            <img
              className="img__icon"
              src="assets/excel.png"
              onClick={handleShow}
            ></img>
          )}
        </div>
      </div>
      {loading ? (
        <SpinnerLazyCustom style="index__page" />
      ) : docs?.length > 0 ? (
        <div className="card main__RequestImbox__card shadow-sm">
          <TableCustomRequest
            titleTable={titleTable}
            data={docs}
            handleNavegate={handleNavegate}
            handleRemove={(id) => handleRemoveMesageRequest(id)}
          />
        </div>
      ) : (
        <>
          <div className="main__RequestImbox__Empty">
            <span>No tienes solicitudes</span>
          </div>
          <div className="main__RequestImbox__dividir__Empty"></div>
        </>
      )}
      <ModalCustom
        show={show}
        handleClose={handleClose}
        handleShow={handleShow}
        isButton={false}
        fullscreen={false}
        title="EXPORTAR EN EXCEL"
        childen={<ExportRequestXLSX data={docs} />}
      />

      <PaginationOption
        page={page}
        onClickLeft={handlePaginationLeft}
        onClickRight={handlePaginationRight}
      />
    </div>
  );
};
