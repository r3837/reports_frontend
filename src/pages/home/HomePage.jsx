import { useSelector } from "react-redux";
import { Outlet } from "react-router-dom";
import { Sidebar } from "../../components/layout/sidebar/Sidebar";
import { ModalNewRequest } from "../../components/modal-new-request/ModalNewRequest";

import "./Home.scss";

export const HomePage = () => {
  const { user } = useSelector((state) => state.auth);
  return (
    <div
      style={{
        padding: "50px 0px 0px 370px",
      }}
    >
      <Sidebar />
      <div style={{ paddingRight: "50px" }}>
        <Outlet />
        { user.rol === "solicitante" &&
          window.location.pathname.split("/")[1] !== "mensajes" &&
          window.location.pathname.split("/")[1] !== "opciones" && (
            <ModalNewRequest />
          )}
      </div>
    </div>
  );
};
