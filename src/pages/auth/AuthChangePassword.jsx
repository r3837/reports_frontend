import { ChangePassword } from "../../components/change-password/ChangePassword";
import "./AuthChangePassword.scss";

export const AuthChangePasswordPage = () => {
  return (
    <div className="main-auth">
      {/* <div className="main-auth-logo">
        <span>logo</span>
      </div> */}
      <div className="card card-container">
        <ChangePassword />
      </div>
      ;
    </div>
  );
};
