import RecoverPassword from "../../components/recover-password/RecoverPassword";
import { SignInPage } from "../../components/signIn";
import { useAuthOptions } from "../../hooks/components/useAuthOptions";
import "./AuthPage.scss";

export const AuthPage = () => {
  const { handleMenu, selectedMenu, setSelectedMenu } = useAuthOptions();

  return (
    <div className="main-auth">
      {/* <div className="main-auth-logo">
        <img src="assets/logo.png" className="logo__main"></img>
      </div> */}
      <div className="card card-container">
        {selectedMenu === 0 ? (
          <>
            <SignInPage />
            <div className="recover_password">
              <span onClick={() => handleMenu(1)}> Olvide mi contraseña</span>
            </div>
          </>
        ) : (
          <>
            <RecoverPassword setNavegte={setSelectedMenu} />
            <div className="recover_password">
              <span onClick={() => handleMenu(0)}> Iniciar sesion</span>
            </div>
          </>
        )}
      </div>
      ;
    </div>
  );
};
