export const typeUserRol = {
  solicitante: Symbol("solicitante"),
  responsable: Symbol("responsable"),
  admin: Symbol("admin"),
};
