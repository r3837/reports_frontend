import ReactDOM from "react-dom";

import { Provider } from "react-redux";
import { store } from "./store/store";
import { ContextSocketProvider } from "./context/context-socketio";

import { BrowserRouter } from "react-router-dom";
import App from "./App";
import "bootstrap/dist/css/bootstrap.min.css";
import "boxicons/css/boxicons.min.css";

ReactDOM.render(
  <BrowserRouter>
    <Provider store={store}>
      <ContextSocketProvider>
        <App />
      </ContextSocketProvider>
    </Provider>
  </BrowserRouter>,
  document.getElementById("root")
);
