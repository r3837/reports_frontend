import React from "react";
import { Link } from "react-router-dom";
import "./BackNavegate.scss";

export const BackNavigate = ({color}) => {
  return (
    <div className="back__navegate">
      <Link to={-1}>
        <i className={`bx bx-left-arrow-alt ${color}`}></i>
      </Link>
    </div>
  );
};
