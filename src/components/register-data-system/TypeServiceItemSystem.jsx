import { useTypeSystemItemSystem } from "../../hooks/components/useTypeSystemItemSystem";
import { EmptyList } from "../empty/EmptyList";
import { ModalCustom } from "../modal-custom/ModalCustom";
import { PaginationOption } from "../paginations/PaginationOption";
import { SpinnerLazyCustom } from "../spinner/SpinnerLazyCustom";
import { AlertCustomItem } from "./components/AlertCustomItem";
import { TypeServiceItemSystemEdit } from "./components/typeService/TypeServiceItemSystemEdit";
import { TypeServiceItemSystemOptionNew } from "./components/typeService/TypeServiceItemSystemOptionNew";
import "./TypeServiceItemSystem.scss";

export const TypeServiceItemSystem = () => {
  const {
    docs,
    loadingSettingUserTypeService,
    show,
    handleClose,
    handleShow,
    handleRemoveTypeService,
    updateTypeService,
    setUpdateTypeService,
    page,
    handlePaginationLeft,
    handlePaginationRight,
  } = useTypeSystemItemSystem();

  return (
    <div className="main__type_system">
      <div className="main__type__system__content">
        {loadingSettingUserTypeService ? (
          <SpinnerLazyCustom style="index__pageV2" />
        ) : docs?.length <= 0 ? (
          <EmptyList title="tipo servicios registrados" />
        ) : (
          docs?.map((item, index) => (
            <div key={index}>
              <AlertCustomItem
                id={item.tipo_servicio_id}
                title={item?.tipo_servicio}
                content={item?.observacion}
                isTypeService={true}
                onClickEdit={(item) => {
                  setUpdateTypeService(item);
                  handleShow();
                }}
                onClickRemove={(id) => handleRemoveTypeService(id)}
              />
            </div>
          ))
        )}
      </div>
      <ModalCustom
        show={show}
        handleClose={handleClose}
        handleShow={handleShow}
        isButton={false}
        fullscreen={false}
        title="ACTUALIZAR TIPO SERVICIO"
        childen={
          <TypeServiceItemSystemEdit
            item={updateTypeService}
            handleClose={handleClose}
          />
        }
      />
      <TypeServiceItemSystemOptionNew />
      <PaginationOption
        page={page}
        onClickLeft={handlePaginationLeft}
        onClickRight={handlePaginationRight}
      />
    </div>
  );
};
