import { useTeamItemSystem } from "../../hooks/components/useTeamItemSystem";
import { EmptyList } from "../empty/EmptyList";
import { ModalCustom } from "../modal-custom/ModalCustom";
import { PaginationOption } from "../paginations/PaginationOption";
import { SpinnerLazyCustom } from "../spinner/SpinnerLazyCustom";
import { TableCustomTeam } from "../table/TableCustom";
import { TeamItemSystemEdit } from "./components/team/TeamItemSystemEdit";
import { TeamItemSystemOptionNew } from "./components/team/TeamItemSystemOptionNew";
import "./TeamItemSystem.scss";

const titleTable = [
  "Equipo ID",
  "Nombre Equipo",
  "Descripcion",
  "Tipo",
  "Dependencia",
  "Opciones",
];

export const TeamItemSystem = () => {
  const {
    docs,
    loadingSettingUserTeam,
    show,
    handleClose,
    handleShow,
    handleRemoveTeam,
    updateTeam,
    setUpdateTeam,
    page,
    handlePaginationLeft,
    handlePaginationRight,
  } = useTeamItemSystem();

  return (
    <div className="main__team__system">
      <div className="main__team__system__content">
        {loadingSettingUserTeam ? (
          <SpinnerLazyCustom style="index__pageV2" />
        ) : docs?.length <= 0 ? (
          <EmptyList title="equipos registrados" />
        ) : (
          <div className="card main__RequestImbox__card shadow-sm">
            <TableCustomTeam
              titleTable={titleTable}
              data={docs}
              handleEdit={(item) => {
                setUpdateTeam(item);
                handleShow();
              }}
              handleRemove={(id) => handleRemoveTeam(id)}
            />
          </div>
        )}
      </div>
      <ModalCustom
        show={show}
        handleClose={handleClose}
        handleShow={handleShow}
        isButton={false}
        fullscreen={false}
        title="ACTUALIZAR EQUIPO"
        childen={
          <TeamItemSystemEdit item={updateTeam} handleClose={handleClose} />
        }
      />
      <TeamItemSystemOptionNew />
      <PaginationOption
        page={page}
        onClickLeft={handlePaginationLeft}
        onClickRight={handlePaginationRight}
      />
    </div>
  );
};
