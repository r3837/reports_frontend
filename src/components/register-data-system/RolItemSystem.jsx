import { useRolItemSystem } from "../../hooks/components/useRolItemSystem";
import { EmptyList } from "../empty/EmptyList";
import { ModalCustom } from "../modal-custom/ModalCustom";
import { PaginationOption } from "../paginations/PaginationOption";
import { SpinnerLazyCustom } from "../spinner/SpinnerLazyCustom";
import { AlertCustomItem } from "./components/AlertCustomItem";
import { RolItemSystemEdit } from "./components/rol/RolItemSystemEdit";
import { RolItemSystemOptionNew } from "./components/rol/RolItemSystemOptionNew";
import "./RolItemSystem.scss";

export const RolItemSystem = () => {
  const {
    docs,
    loadingSettingUserRol,
    show,
    handleClose,
    handleShow,
    handleRemoveRol,
    updateRol,
    setUpdateRol,
    page,
    handlePaginationLeft,
    handlePaginationRight,
  } = useRolItemSystem();

  return (
    <div className="main__rol__systen">
      <div className="main__rol__systen__content">
        {loadingSettingUserRol ? (
          <SpinnerLazyCustom style="index__pageV2" />
        ) : docs?.length <= 0 ? (
          <EmptyList title="rol registrados" />
        ) : (
          docs?.map((item, index) => (
            <div key={index}>
              <AlertCustomItem
                title={item?.rol}
                onClickEdit={(item) => {
                  setUpdateRol(item);
                  handleShow();
                }}
                onClickRemove={(id) => handleRemoveRol(id)}
              />
            </div>
          ))
        )}
      </div>
      <ModalCustom
        show={show}
        handleClose={handleClose}
        handleShow={handleShow}
        isButton={false}
        fullscreen={false}
        title="ACTUALIZAR ROL"
        childen={
          <RolItemSystemEdit item={updateRol} handleClose={handleClose} />
        }
      />
      <RolItemSystemOptionNew />
      <PaginationOption
        page={page}
        onClickLeft={handlePaginationLeft}
        onClickRight={handlePaginationRight}
      />
    </div>
  );
};
