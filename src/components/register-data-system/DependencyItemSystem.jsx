import { useDependencyItemSystem } from "../../hooks/components/useDependencyItemSystem";
import { EmptyList } from "../empty/EmptyList";
import { ModalCustom } from "../modal-custom/ModalCustom";
import { PaginationOption } from "../paginations/PaginationOption";
import { SpinnerLazyCustom } from "../spinner/SpinnerLazyCustom";
import { AlertCustomItem } from "./components/AlertCustomItem";
import { DependencyItemSystemEdit } from "./components/dependency/DependencyItemSystemEdit";
import { DependencyItemSystemOptionNew } from "./components/dependency/DependencyItemSystemOptionNew";
import "./DependencyItemSystem.scss";

export const DependencyItemSystem = () => {
  const {
    docs,
    loadingSettingUserDependency,
    show,
    handleClose,
    handleShow,
    handleRemoveDependency,
    updateDependency,
    setUpdateDependency,
    page,
    handlePaginationLeft,
    handlePaginationRight,
  } = useDependencyItemSystem();

  return (
    <div className="main__dependency__systen">
      <div className="main__dependency__systen__content">
        {loadingSettingUserDependency ? (
          <SpinnerLazyCustom style="index__pageV2" />
        ) : docs?.length <= 0 ? (
          <EmptyList title="dependencias registrados" />
        ) : (
          docs?.map((item, index) => (
            <div key={index}>
              <AlertCustomItem
                id={item?.dependencia_id}
                isDependency={true}
                title={item?.nombre_dependencia}
                onClickEdit={(item) => {
                  setUpdateDependency(item);
                  handleShow();
                }}
                onClickRemove={(id) => handleRemoveDependency(id)}
              />
            </div>
          ))
        )}
      </div>
      <ModalCustom
        show={show}
        handleClose={handleClose}
        handleShow={handleShow}
        isButton={false}
        fullscreen={false}
        title="ACTUALIZAR DEPENDENCIA"
        childen={
          <DependencyItemSystemEdit
            item={updateDependency}
            handleClose={handleClose}
          />
        }
      />
      <DependencyItemSystemOptionNew />
      <PaginationOption
        page={page}
        onClickLeft={handlePaginationLeft}
        onClickRight={handlePaginationRight}
      />
    </div>
  );
};
