import { CargoItemSystemOptionNew } from "./components/cargo/CargoItemSystemOptionNew";
import { AlertCustomItem } from "./components/AlertCustomItem";
import { useCargoItemSystem } from "../../hooks/components/useCargoItemSystem";
import { SpinnerLazyCustom } from "../spinner/SpinnerLazyCustom";
import { ModalCustom } from "../modal-custom/ModalCustom";
import { CargoItemSystemEdit } from "./components/cargo/CargoItemSystemEdit";
import "./CargoItemSystem.scss";
import { PaginationOption } from "../paginations/PaginationOption";
import { EmptyList } from "../empty/EmptyList";

export const CargoItemSystem = () => {
  const {
    docs,
    loadingSettingUserCargo,
    show,
    handleClose,
    handleShow,
    handleRemoveCargo,
    updateCargo,
    setUpdateCargo,
    page,
    handlePaginationLeft,
    handlePaginationRight,
  } = useCargoItemSystem();

  return (
    <div className="main__cargo__systen">
      <div className="main__cargo__systen__content">
        {loadingSettingUserCargo ? (
          <SpinnerLazyCustom style="index__pageV2" />
        ) : docs?.length <= 0 ? (
          <EmptyList title="cargos registrados" />
        ) : (
          docs?.map((item, index) => (
            <div key={index}>
              <AlertCustomItem
                id={item?.cargo_id}
                title={item?.cargo}
                isTypeCargo={true}
                onClickEdit={(item) => {
                  setUpdateCargo(item);
                  handleShow();
                }}
                onClickRemove={(id) => handleRemoveCargo(id)}
              />
            </div>
          ))
        )}
      </div>
      <ModalCustom
        show={show}
        handleClose={handleClose}
        handleShow={handleShow}
        isButton={false}
        fullscreen={false}
        title="ACTUALIZAR CARGO"
        childen={
          <CargoItemSystemEdit item={updateCargo} handleClose={handleClose} />
        }
      />
      <CargoItemSystemOptionNew />
      <PaginationOption
        page={page}
        onClickLeft={handlePaginationLeft}
        onClickRight={handlePaginationRight}
      />
    </div>
  );
};
