import { ErrorMessage } from "@hookform/error-message";
import { Form } from "react-bootstrap";
import { Controller } from "react-hook-form";
import Select from "react-select";
import { useTeamItemSystemNew } from "../../../../hooks/components/useTeamItemSystemNew";
import "./TeamItemSystemNew.scss";

const required = (value) => {
  return (
    <div className="alert alert-danger" role="alert">
      {value}
    </div>
  );
};

export const TeamItemSystemNew = ({ handleClose }) => {
  const {
    loading,
    control,
    handleSubmit,
    errors,
    handleCreateTeam,
    settingTypeTeam,
    settingDependency,
  } = useTeamItemSystemNew(handleClose);

  return (
    <Form onSubmit={handleSubmit(handleCreateTeam)}>
      <div className="col-12 mt-2">
        <Form.Label>ID Equipo </Form.Label>
        <Controller
          name="equipo_id"
          control={control}
          rules={{
            required: "El identificador del equipo es obligatorio!",
          }}
          render={({ field }) => (
            <Form.Control {...field} type="equipo_id" placeholder="Equipo ID" />
          )}
        />
        <ErrorMessage
          errors={errors}
          name="equipo_id"
          render={({ message }) => required(message)}
        />
      </div>
      <div className="col-12 mt-2">
        <Form.Label>Dependencia del equipo</Form.Label>
        <Controller
          name="dependencia_id"
          control={control}
          rules={{
            required: "La dependencia es obligatorio!",
          }}
          render={({ field }) => (
            <Select
              {...field}
              placeholder="Dependencia"
              options={settingDependency ?? []}
            />
          )}
        />
        <ErrorMessage
          errors={errors}
          name="dependencia_id"
          render={({ message }) => required(message)}
        />
      </div>
      <div className="col-12 mt-2">
        <Form.Label>Tipo de equipo</Form.Label>
        <Controller
          name="tipo_equipo_id"
          control={control}
          rules={{
            required: "El tipo de equipo es obligatorio!                    ",
          }}
          render={({ field }) => (
            <Select
              {...field}
              placeholder="Tipo equipo"
              options={settingTypeTeam ?? []}
            />
          )}
        />
        <ErrorMessage
          errors={errors}
          name="tipo_equipo_id"
          render={({ message }) => required(message)}
        />
      </div>
      <div className="col-12 mt-2">
        <Form.Label>Equipo</Form.Label>
        <Controller
          name="nombre_equipo"
          control={control}
          rules={{
            required: "El equipo es obligatorio!",
          }}
          render={({ field }) => (
            <Form.Control
              {...field}
              type="nombre_equipo"
              placeholder="Nombre equipo"
            />
          )}
        />
        <ErrorMessage
          errors={errors}
          name="nombre_equipo"
          render={({ message }) => required(message)}
        />
      </div>
      <div className="col-12 mt-2">
        <Form.Label>Descripcion equipo</Form.Label>
        <Controller
          name="descripcion_equipo"
          control={control}
          rules={{
            required: "la descripcion del equipo es obligatorio!",
          }}
          render={({ field }) => (
            <Form.Control
              {...field}
              type="descripcion_equipo"
              placeholder="Descripcion equipo"
            />
          )}
        />
        <ErrorMessage
          errors={errors}
          name="descripcion_equipo"
          render={({ message }) => required(message)}
        />
      </div>

      <div className="form-group new__request__option">
        <button className="new__request__option__button" disabled={loading}>
          {loading && (
            <span className="spinner-border spinner-border-sm"></span>
          )}
          <span>Crear equipo</span>
        </button>
      </div>
    </Form>
  );
};
