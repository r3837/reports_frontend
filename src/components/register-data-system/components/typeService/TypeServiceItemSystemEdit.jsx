import { ErrorMessage } from "@hookform/error-message";
import { Form } from "react-bootstrap";
import { Controller } from "react-hook-form";
import { useTypeServiceItemSystemEdit } from "../../../../hooks/components/useTypeServiceItemSystemEdit";
import "./TypeServiceItemSystemEdit.scss";

const required = (value) => {
  return (
    <div className="alert alert-danger" role="alert">
      {value}
    </div>
  );
};

export const TypeServiceItemSystemEdit = ({ item, handleClose }) => {
  const { loading, control, handleSubmit, errors, handleUpdateTypeService } =
    useTypeServiceItemSystemEdit(item, handleClose);

  return (
    <Form onSubmit={handleSubmit(handleUpdateTypeService)}>
      <div className="col-12 mt-2">
        <Form.Label>Tipo de servicio</Form.Label>
        <Controller
          name="tipo_servicio"
          control={control}
          rules={{
            required: "El tipo del servicio es obligatorio!",
          }}
          render={({ field }) => (
            <Form.Control
              {...field}
              type="tipo_servicio"
              placeholder="Tipo servicio"
            />
          )}
        />
        <ErrorMessage
          errors={errors}
          name="tipo_servicio"
          render={({ message }) => required(message)}
        />
      </div>
      <div className="col-12 mt-2">
        <Form.Label>Observacion de servicio </Form.Label>
        <Controller
          name="observacion"
          control={control}
          render={({ field }) => (
            <Form.Control
              {...field}
              type="observacion"
              placeholder="Observacion servicio"
            />
          )}
        />
      </div>

      <div className="form-group new__request__option">
        <button className="new__request__option__button" disabled={loading}>
          {loading && (
            <span className="spinner-border spinner-border-sm"></span>
          )}
          <span>Actualizar tipo servicio</span>
        </button>
      </div>
    </Form>
  );
};
