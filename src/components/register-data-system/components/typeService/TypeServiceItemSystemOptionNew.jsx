import { useState } from "react";
import { ModalCustom } from "../../../modal-custom/ModalCustom";
import { TypeServiceItemSystemNew } from "./TypeServiceItemSystemNew";
import "./TypeServiceItemSystemOptionNew.scss";

export const TypeServiceItemSystemOptionNew = () => {
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  return (
    <div>
      <div className="main__rol__systen__btn" onClick={handleShow}>
        <span className="css-button-sliding-to-left--black">
          Nuevo tipo servicio
        </span>
      </div>

      <ModalCustom
        show={show}
        handleClose={handleClose}
        handleShow={handleShow}
        isButton={false}
        fullscreen={false}
        title="NUEVO TIPO SERVICIO"
        childen={<TypeServiceItemSystemNew handleClose={handleClose} />}
      />
    </div>
  );
};
