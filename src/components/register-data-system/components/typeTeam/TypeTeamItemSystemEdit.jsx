import { ErrorMessage } from "@hookform/error-message";
import { Form } from "react-bootstrap";
import { Controller } from "react-hook-form";
import { useTypeTeamItemSystemEdit } from "../../../../hooks/components/useTypeTeamItemSystemEdit";
import "./TypeTeamItemSystemEdit.scss";

const required = (value) => {
  return (
    <div className="alert alert-danger" role="alert">
      {value}
    </div>
  );
};

export const TypeTeamItemSystemEdit = ({ item, handleClose }) => {
  const { loading, control, handleSubmit, errors, handleUpdateTypeTeam } =
    useTypeTeamItemSystemEdit(item, handleClose);

  return (
    <Form onSubmit={handleSubmit(handleUpdateTypeTeam)}>
      <div className="col-12 mt-2">
        <Form.Label>Tipo de equipo</Form.Label>
        <Controller
          name="name"
          control={control}
          rules={{
            required: "El tipo del equipo es obligatorio!",
          }}
          render={({ field }) => (
            <Form.Control {...field} type="name" placeholder="Tipo equipo" />
          )}
        />
        <ErrorMessage
          errors={errors}
          name="name"
          render={({ message }) => required(message)}
        />
      </div>

      <div className="form-group new__request__option">
        <button className="new__request__option__button" disabled={loading}>
          {loading && (
            <span className="spinner-border spinner-border-sm"></span>
          )}
          <span>Actualizar tipo equipo</span>
        </button>
      </div>
    </Form>
  );
};
