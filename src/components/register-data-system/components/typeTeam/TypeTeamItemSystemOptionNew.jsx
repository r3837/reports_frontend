import { useState } from "react";
import { ModalCustom } from "../../../modal-custom/ModalCustom";
import { TypeTeamItemSystemNew } from "./TypeTeamItemSystemNew";
import "./TypeTeamItemSystemOptionNew.scss";

export const TypeTeamItemSystemOptionNew = () => {
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  return (
    <div>
      <div className="main__rol__systen__btn" onClick={handleShow}>
        <span className="css-button-sliding-to-left--black">
          Nuevo tipo equipo
        </span>
      </div>

      <ModalCustom
        show={show}
        handleClose={handleClose}
        handleShow={handleShow}
        isButton={false}
        fullscreen={false}
        title="NUEVO TIPO EQUIPO"
        childen={<TypeTeamItemSystemNew handleClose={handleClose} />}
      />
    </div>
  );
};
