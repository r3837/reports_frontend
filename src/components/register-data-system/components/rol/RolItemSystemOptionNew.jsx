import { useState } from "react";
import { ModalCustom } from "../../../modal-custom/ModalCustom";
import { RolItemSystemNew } from "./RolItemSystemNew";
import "./RolItemSystemOptionNew.scss";

export const RolItemSystemOptionNew = () => {
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  return (
    <div>
      <div className="main__rol__systen__btn" onClick={handleShow}>
        <span className="css-button-sliding-to-left--black">Nuevo rol</span>
      </div>

      <ModalCustom
        show={show}
        handleClose={handleClose}
        handleShow={handleShow}
        isButton={false}
        fullscreen={false}
        title="NUEVO ROL"
        childen={<RolItemSystemNew handleClose={handleClose} />}
      />
    </div>
  );
};
