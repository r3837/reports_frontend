import { ErrorMessage } from "@hookform/error-message";
import { Form } from "react-bootstrap";
import { Controller } from "react-hook-form";
import { useRolItemSystemNew } from "../../../../hooks/components/useRolItemSystemNew";
import "./RolItemSystemNew.scss";

const required = (value) => {
  return (
    <div className="alert alert-danger" role="alert">
      {value}
    </div>
  );
};

export const RolItemSystemNew = ({ handleClose }) => {
  const { loading, control, handleSubmit, errors, handleCreateRol } =
    useRolItemSystemNew(handleClose);

  return (
    <Form onSubmit={handleSubmit(handleCreateRol)}>
      <div className="col-12 mt-2">
        <Controller
          name="rol"
          control={control}
          rules={{
            required: "El rol es obligatorio!",
          }}
          render={({ field }) => (
            <Form.Control {...field} type="rol" placeholder="Rol" />
          )}
        />
        <ErrorMessage
          errors={errors}
          name="rol"
          render={({ message }) => required(message)}
        />
      </div>
      <div className="form-group new__request__option">
        <button className="new__request__option__button" disabled={loading}>
          {loading && (
            <span className="spinner-border spinner-border-sm"></span>
          )}
          <span>Crear rol</span>
        </button>
      </div>
    </Form>
  );
};
