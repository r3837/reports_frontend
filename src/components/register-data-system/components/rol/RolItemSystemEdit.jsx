import { ErrorMessage } from "@hookform/error-message";
import { Form } from "react-bootstrap";
import { Controller } from "react-hook-form";
import { useRolItemSystemEdit } from "../../../../hooks/components/useRolItemSystemEdit";
import "./RolItemSystemEdit.scss";

const required = (value) => {
  return (
    <div className="alert alert-danger" role="alert">
      {value}
    </div>
  );
};

export const RolItemSystemEdit = ({ item, handleClose }) => {
  const { loading, control, handleSubmit, errors, handleUpdateRol } =
    useRolItemSystemEdit(item, handleClose);

  return (
    <Form onSubmit={handleSubmit(handleUpdateRol)}>
      <div className="col-12 mt-2">
        <Controller
          name="rol"
          control={control}
          rules={{
            required: "El rol es obligatorio!",
          }}
          render={({ field }) => (
            <Form.Control {...field} type="rol" placeholder="Rol" />
          )}
        />
        <ErrorMessage
          errors={errors}
          name="rol"
          render={({ message }) => required(message)}
        />
      </div>
      <div className="form-group new__request__option">
        <button className="new__request__option__button" disabled={loading}>
          {loading && (
            <span className="spinner-border spinner-border-sm"></span>
          )}
          <span>Actualizar rol</span>
        </button>
      </div>
    </Form>
  );
};
