import { useState } from "react";
import { ModalCustom } from "../../../modal-custom/ModalCustom";
import { DependencyItemSystemNew } from "./DependencyItemSystemNew";
import "./DependencyItemSystemOptionNew.scss";

export const DependencyItemSystemOptionNew = () => {
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  return (
    <div>
      <div className="main__dependency__systen__btn" onClick={handleShow}>
        <span className="css-button-sliding-to-left--black">
          Nueva dependencia
        </span>
      </div>
      <ModalCustom
        show={show}
        handleClose={handleClose}
        handleShow={handleShow}
        isButton={false}
        fullscreen={false}
        title="NUEVA DEPENDENCIA"
        childen={<DependencyItemSystemNew handleClose={handleClose} />}
      />
    </div>
  );
};
