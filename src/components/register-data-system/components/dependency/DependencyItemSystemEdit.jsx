import { ErrorMessage } from "@hookform/error-message";
import { Form } from "react-bootstrap";
import { Controller } from "react-hook-form";
import { useDependencyItemSystemEdit } from "../../../../hooks/components/useDependencyItemSystemEdit";
import "./DependencyItemSystemEdit.scss";

const required = (value) => {
  return (
    <div className="alert alert-danger" role="alert">
      {value}
    </div>
  );
};

export const DependencyItemSystemEdit = ({ item, handleClose }) => {
  const { loading, control, handleSubmit, errors, handleUpdateDependency } =
    useDependencyItemSystemEdit(item, handleClose);

  return (
    <Form onSubmit={handleSubmit(handleUpdateDependency)}>
      <div className="col-12 mt-2">
        <Controller
          name="nombre_dependencia"
          control={control}
          rules={{
            required: "El dependencia es obligatorio!",
          }}
          render={({ field }) => (
            <Form.Control
              {...field}
              type="nombre_dependencia"
              placeholder="Dependencia"
            />
          )}
        />
        <ErrorMessage
          errors={errors}
          name="nombre_dependencia"
          render={({ message }) => required(message)}
        />
      </div>
      <div className="form-group new__request__option">
        <button className="new__request__option__button" disabled={loading}>
          {loading && (
            <span className="spinner-border spinner-border-sm"></span>
          )}
          <span>Actualizar dependencia</span>
        </button>
      </div>
    </Form>
  );
};
