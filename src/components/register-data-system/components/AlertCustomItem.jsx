import "./AlertCustomItem.scss";

export const AlertCustomItem = ({
  id,
  title,
  content,
  isDependency = false,
  isTypeTeam = false,
  isTypeCargo = false,
  isTypeService = false,
  onClickEdit,
  onClickRemove,
}) => {
  return (
    <div className="alert alert-primary" role="alert">
      <div className="title">{title} </div>
      <div className="content">{ content?.length > 0 ? " -- " + content : content }</div>
      <div className="option">
        <i
          className="bx bx-edit"
          onClick={() =>
            onClickEdit(
              isDependency
                ? { dependencia_id: id, nombre_dependencia: title }
                : isTypeTeam
                ? { name: title, tipo_equipo_id: id }
                : isTypeCargo
                ? { cargo: title, cargo_id: id }
                : isTypeService 
                ? { tipo_servicio: title, tipo_servicio_id: id, observacion: content }
                :title
            )
          }
        ></i>
        <i
          className="bx bx-x"
          onClick={() =>
            onClickRemove(isDependency || isTypeTeam || isTypeService ? { id } : title)
          }
        ></i>
      </div>
    </div>
  );
};
