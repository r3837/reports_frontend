import { ErrorMessage } from "@hookform/error-message";
import { Form } from "react-bootstrap";
import { Controller } from "react-hook-form";
import { useCargoItemSystemNew } from "../../../../hooks/components/useCargoItemSystemNew";
import "./CargoItemSystemNew.scss";

const required = (value) => {
  return (
    <div className="alert alert-danger" role="alert">
      {value}
    </div>
  );
};

export const CargoItemSystemNew = ({ handleClose }) => {
  const { loading, control, handleSubmit, errors, handleCreateCargo } =
    useCargoItemSystemNew(handleClose);
  return (
    <Form onSubmit={handleSubmit(handleCreateCargo)}>
      <div className="col-12 mt-2">
        <Controller
          name="cargo"
          control={control}
          rules={{
            required: "El cargo es obligatorio!",
          }}
          render={({ field }) => (
            <Form.Control
              {...field}
              type="cargo"
              placeholder="Cargo"
            />
          )}
        />
        <ErrorMessage
          errors={errors}
          name="cargo"
          render={({ message }) => required(message)}
        />
      </div>
      <div className="form-group new__request__option">
        <button className="new__request__option__button" disabled={loading}>
          {loading && (
            <span className="spinner-border spinner-border-sm"></span>
          )}
          <span>Crear cargo</span>
        </button>
      </div>
    </Form>
  );
};
