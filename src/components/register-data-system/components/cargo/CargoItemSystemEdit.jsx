import { ErrorMessage } from "@hookform/error-message";
import { Form } from "react-bootstrap";
import { Controller } from "react-hook-form";
import { useCargoItemSystemEdit } from "../../../../hooks/components/useCargoItemSystemEdit";
import "./CargoItemSystemEdit.scss";

const required = (value) => {
  return (
    <div className="alert alert-danger" role="alert">
      {value}
    </div>
  );
};

export const CargoItemSystemEdit = ({ item, handleClose }) => {
  const { loading, control, handleSubmit, errors, handleUpdateCargo } =
    useCargoItemSystemEdit(item, handleClose);

  return (
    <Form onSubmit={handleSubmit(handleUpdateCargo)}>
      <div className="col-12 mt-2">
        <Controller
          name="cargo"
          control={control}
          rules={{
            required: "El rol es obligatorio!",
          }}
          render={({ field }) => (
            <Form.Control {...field} type="cargo" placeholder="Cargo" />
          )}
        />
        <ErrorMessage
          errors={errors}
          name="cargo"
          render={({ message }) => required(message)}
        />
      </div>
      <div className="form-group new__request__option">
        <button className="new__request__option__button" disabled={loading}>
          {loading && (
            <span className="spinner-border spinner-border-sm"></span>
          )}
          <span>Actualizar cargo</span>
        </button>
      </div>
    </Form>
  );
};
