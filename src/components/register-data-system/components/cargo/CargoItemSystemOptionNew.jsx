import { useState } from "react";
import { ModalCustom } from "../../../modal-custom/ModalCustom";
import { CargoItemSystemNew } from "./CargoItemSystemNew";
import "./CargoItemSystemOptionNew.scss";

export const CargoItemSystemOptionNew = () => {
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  return (
    <div>
      <div className="main__cargo__systen__btn" onClick={handleShow}>
        <span className="css-button-sliding-to-left--black">Nuevo cargo</span>
      </div>
      <ModalCustom
        show={show}
        handleClose={handleClose}
        handleShow={handleShow}
        isButton={false}
        fullscreen={false}
        title="NUEVO CARGO"
        childen={<CargoItemSystemNew handleClose={handleClose} />}
      />
    </div>
  );
};
