import { useTypeTeamItemSystem } from "../../hooks/components/useTypeTeamItemSystem";
import { EmptyList } from "../empty/EmptyList";
import { ModalCustom } from "../modal-custom/ModalCustom";
import { PaginationOption } from "../paginations/PaginationOption";
import { SpinnerLazyCustom } from "../spinner/SpinnerLazyCustom";
import { AlertCustomItem } from "./components/AlertCustomItem";
import { TypeTeamItemSystemEdit } from "./components/typeTeam/TypeTeamItemSystemEdit";
import { TypeTeamItemSystemOptionNew } from "./components/typeTeam/TypeTeamItemSystemOptionNew";
import "./TypeTeamItemSystem.scss";

export const TypeTeamItemSystem = () => {
  const {
    docs,
    loadingSettingUserTypeTeam,
    show,
    handleClose,
    handleShow,
    handleRemoveTypeTeam,
    updateTypeTeam,
    setUpdateTypeTeam,
    page,
    handlePaginationLeft,
    handlePaginationRight,
  } = useTypeTeamItemSystem();

  return (
    <div className="main__team__system">
      <div className="main__team__system__content">
        {loadingSettingUserTypeTeam ? (
          <SpinnerLazyCustom style="index__pageV2" />
        ) : docs?.length <= 0 ? (
          <EmptyList title="tipo equipos registrados" />
        ) : (
          docs?.map((item, index) => (
            <div key={index}>
              <AlertCustomItem
                id={item.tipo_equipo_id}
                title={item?.name}
                isTypeTeam={true}
                onClickEdit={(item) => {
                  setUpdateTypeTeam(item);
                  handleShow();
                }}
                onClickRemove={(id) => handleRemoveTypeTeam(id)}
              />
            </div>
          ))
        )}
      </div>
      <ModalCustom
        show={show}
        handleClose={handleClose}
        handleShow={handleShow}
        isButton={false}
        fullscreen={false}
        title="ACTUALIZAR TIPO EQUIPO"
        childen={
          <TypeTeamItemSystemEdit
            item={updateTypeTeam}
            handleClose={handleClose}
          />
        }
      />
      <TypeTeamItemSystemOptionNew />
      <PaginationOption
        page={page}
        onClickLeft={handlePaginationLeft}
        onClickRight={handlePaginationRight}
      />
    </div>
  );
};
