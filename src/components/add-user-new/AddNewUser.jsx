import { Form } from "react-bootstrap";
import { ErrorMessage } from "@hookform/error-message";
import { Controller } from "react-hook-form";
import { useAddNewUser } from "../../hooks/components/useAddNewUser";
import { SpinnerLazyCustom } from "../spinner/SpinnerLazyCustom";
import Select from "react-select";
import "./AddNewUser.scss";

const required = (value) => {
  return (
    <div className="alert alert-danger" role="alert">
      {value}
    </div>
  );
};

export const AddNewUser = ({ handleClose }) => {
  const {
    control,
    handleSubmit,
    handleNewUser,
    errors,
    loading,
    loadingeSettingUser,
    settingUserRol,
    settingUserDependency,
    settingUserCargo,
    settingUserStatus
  } = useAddNewUser(handleClose);


  return (
    <>
      {loadingeSettingUser ? (
        <SpinnerLazyCustom style="index__page" />
      ) : (
        <Form onSubmit={handleSubmit(handleNewUser)}>
          <div className="row main__add__new__user">
            <div className="col-12">
              <div className="header__imbox shadow-lg">
                <span> Registro de usuario </span>
              </div>
            </div>
            <div className="col-6">
              <Form.Group className="mb-3">
                <Controller
                  name="nro_cedula"
                  control={control}
                  rules={{
                    required: "El numero de cedula es obligatorio!",
                  }}
                  render={({ field }) => (
                    <>
                      <Form.Label>Cedula</Form.Label>
                      <Form.Control
                        {...field}
                        type="nro_cedula"
                        placeholder="Numero cedula"
                      />
                    </>
                  )}
                />
                <ErrorMessage
                  errors={errors}
                  name="nro_cedula"
                  render={({ message }) => required(message)}
                />
              </Form.Group>

              <Form.Group className="mb-3">
                <Controller
                  name="nombres"
                  control={control}
                  rules={{
                    required: "El nombres es obligatorio!",
                  }}
                  render={({ field }) => (
                    <>
                      <Form.Label>Nombres</Form.Label>
                      <Form.Control
                        {...field}
                        type="nombres"
                        placeholder="Nombres"
                      />
                    </>
                  )}
                />
                <ErrorMessage
                  errors={errors}
                  name="nombres"
                  render={({ message }) => required(message)}
                />
              </Form.Group>

              <Form.Group className="mb-3">
                <Controller
                  name="apellidos"
                  control={control}
                  rules={{
                    required: "El Apellidos es obligatorio!",
                  }}
                  render={({ field }) => (
                    <>
                      <Form.Label>Apellidos</Form.Label>
                      <Form.Control
                        {...field}
                        type="apellidos"
                        placeholder="Apellidos"
                      />
                    </>
                  )}
                />
                <ErrorMessage
                  errors={errors}
                  name="apellidos"
                  render={({ message }) => required(message)}
                />
              </Form.Group>

              <Form.Group className="mb-3">
                <Controller
                  name="correo_electronico"
                  control={control}
                  rules={{
                    required: "El Correo electronico es obligatorio!",
                  }}
                  render={({ field }) => (
                    <>
                      <Form.Label>Correo electronico</Form.Label>
                      <Form.Control
                        {...field}
                        type="correo_electronico"
                        placeholder="Correo electronico"
                      />
                    </>
                  )}
                />
                <ErrorMessage
                  errors={errors}
                  name="correo_electronico"
                  render={({ message }) => required(message)}
                />
              </Form.Group>

              <Form.Group className="mb-3">
                <Controller
                  name="nro_celular"
                  control={control}
                  rules={{
                    required: "El numero celular es obligatorio!",
                  }}
                  render={({ field }) => (
                    <>
                      <Form.Label>Numero celular</Form.Label>
                      <Form.Control
                        {...field}
                        type="nro_celular"
                        placeholder="Numero celular"
                      />
                    </>
                  )}
                />
                <ErrorMessage
                  errors={errors}
                  name="nro_celular"
                  render={({ message }) => required(message)}
                />
              </Form.Group>
            </div>
            <div className="col-6">
              <Form.Group className="mb-3">
                <Controller
                  name="rol"
                  control={control}
                  rules={{
                    required: "El rol es obligatorio!",
                  }}
                  render={({ field }) => (
                    <>
                      <Form.Label>Rol</Form.Label>
                      <Select
                        {...field}
                        placeholder="Rol"
                        options={settingUserRol ?? []}
                      />
                    </>
                  )}
                />
                <ErrorMessage
                  errors={errors}
                  name="rol"
                  render={({ message }) => required(message)}
                />
              </Form.Group>

              <Form.Group className="mb-3">
                <Controller
                  name="estado"
                  control={control}
                  rules={{
                    required: "El estado es obligatorio!",
                  }}
                  render={({ field }) => (
                    <>
                      <Form.Label>Estado</Form.Label>
                      <Select
                        {...field}
                        placeholder="Estado"
                        options={settingUserStatus ?? []}
                      />
                    </>
                  )}
                />
                <ErrorMessage
                  errors={errors}
                  name="estado"
                  render={({ message }) => required(message)}
                />
              </Form.Group>

              <Form.Group className="mb-3">
                <Controller
                  name="dependencia_id"
                  control={control}
                  rules={{
                    required: "La dependencia es obligatorio!",
                  }}
                  render={({ field }) => (
                    <>
                      <Form.Label>Dependencia</Form.Label>
                      <Select
                        {...field}
                        placeholder="Dependencia"
                        options={settingUserDependency ?? []}
                      />
                    </>
                  )}
                />
                <ErrorMessage
                  errors={errors}
                  name="dependencia_id"
                  render={({ message }) => required(message)}
                />
              </Form.Group>

              <Form.Group className="mb-3">
                <Controller
                  name="cargo_id"
                  control={control}
                  rules={{
                    required: "La cargo es obligatorio!",
                  }}
                  render={({ field }) => (
                    <>
                      <Form.Label>Cargo</Form.Label>
                       <Select
                        {...field}
                        placeholder="Cargo"
                        options={settingUserCargo ?? []}
                      />
                    </>
                  )}
                />
                <ErrorMessage
                  errors={errors}
                  name="cargo_id"
                  render={({ message }) => required(message)}
                />
              </Form.Group>
            </div>
          </div>
          <div className="main__add__new__user__option">
            <div className="form-group">
              <button className="btn__new_user" disabled={loading}>
                {loading && (
                  <span className="spinner-border spinner-border-sm"></span>
                )}
                <span>Registrar usuario</span>
              </button>
            </div>
          </div>
          {/* <button className="btn__new_user">Registrar usuario</button> */}
        </Form>
      )}
    </>
  );
};
