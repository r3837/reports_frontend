import React from "react";
import "./StyleMessageObservation.scss";

export const StyleMessageObservation = ({ item }) => {
  return (
    <div className="row card card__message__item">
      <div className="col-2 content__profile">
        <div className="card__message__item__img">
          <img
            src="//ssl.gstatic.com/accounts/ui/avatar_2x.png"
            alt="profile-img"
            className="profile-img-card"
          />
        </div>
        <div className="card__message__item__name">
          <span>{item.nombres}</span>
        </div>
      </div>
      <div className="col-8">
        <div className="card__message__item__content">
          <p>{item.observacion}</p>
        </div>
      </div>
      <div className="col-2 content__date">
        <div className="card__message__item__date">{item.fecha_observacion}</div>
      </div>
    </div>
  );
};
