import { Table } from "react-bootstrap";
import { typeWindowImbox } from "../../types/typeWiindosImbox";
import "./TableCustom.scss";
import { typeStatus } from "./utils/typeStatus";

export const TableCustom = ({
  type,
  titleTable,
  data,
  handleNavegate,
  handleRemove,
}) => {
  return (
    <Table striped bordered hover size="sm">
      <thead>
        <tr>
          {titleTable.map((item, index) => (
            <th key={index}>{item}</th>
          ))}
        </tr>
      </thead>
      <tbody>
        {data.map((item, index) => (
          <tr key={index}>
            <td onClick={() => handleNavegate(item)}>
              {type == typeWindowImbox.request
                ? `${item.nombres} ${item.apellidos}`
                : item.asunto.length > 15
                ? `${item.asunto.substr(0, 15)}...`
                : item.asunto}
            </td>
            <td onClick={() => handleNavegate(item)}>{item.tipo_servicio}</td>
            <td className="status__table" onClick={() => handleNavegate(item)}>
              <div
                className={
                  item.estado === typeStatus.pending.description
                    ? "status__p__table"
                    : item.estado === typeStatus.pause.description
                    ? "status__ep__table"
                    : item.estado === typeStatus.process.description
                    ? "status__pr__table"
                    : "status__c__table"
                }
                onClick={() => handleNavegate(item)}
              >
                {item.estado}
              </div>
            </td>
            <td onClick={() => handleNavegate(item)}>{item.fecha_solicitud}</td>
            <td
              className="option"
              onClick={() => handleRemove(item.solicitud_id)}
            >
              <i className="bx bx-x"></i>
            </td>
          </tr>
        ))}
      </tbody>
    </Table>
  );
};

export const TableCustomRequest = ({
  titleTable,
  data,
  handleNavegate,
  handleRemove,
  isExport = false,
}) => {
  return (
    <Table striped bordered hover size="sm">
      <thead>
        <tr>
          {titleTable.map((item, index) => (
            <th key={index}>{item}</th>
          ))}
        </tr>
      </thead>
      <tbody>
        {data.map((item, index) => (
          <tr key={index}>
            <td>{item.solicitud_id}</td>
            <td onClick={() => handleNavegate(item)}>
              { item?.asunto?.length > 30
                  ? `${item.asunto.substr(0, 30)}...`
                  : item.asunto
               }
            </td>
            <td onClick={() => handleNavegate(item)}>
              {`${item.nombres} ${item.apellidos}`}
            </td>
            <td onClick={() => handleNavegate(item)}>{item.tipo_servicio}</td>
            <td className="status__table" onClick={() => handleNavegate(item)}>
              <div
                className={
                  item.estado === typeStatus.pending.description
                    ? "status__p__table"
                    : item.estado === typeStatus.pause.description
                    ? "status__ep__table"
                    : item.estado === typeStatus.process.description
                    ? "status__pr__table"
                    : "status__c__table"
                }
              >
                {item.estado}
              </div>
            </td>
            <td key={index + 4} onClick={() => handleNavegate(item)}>
              {item.fecha_observacion
                ? item.fecha_observacion
                : item.fecha_solicitud}
            </td>
            {!isExport && (
              <td
                className="option"
                onClick={() => handleRemove(item.solicitud_id)}
              >
                <i className="bx bx-x"></i>
              </td>
            )}
          </tr>
        ))}
      </tbody>
    </Table>
  );
};

export const TableRegister = ({
  titleTable,
  data,
  handleUpdate,
  handleRemove,
  isActiveUser,
}) => {
  return (
    <Table striped bordered hover size="sm">
      <thead>
        <tr>
          {titleTable.map((item, index) => (
            <th key={index}>{item}</th>
          ))}
        </tr>
      </thead>
      <tbody>
        {data.map((item, index) => (
          <tr key={index}>
            <td onClick={() => handleUpdate(item)}>{item.nro_cedula}</td>
            <td onClick={() => handleUpdate(item)}>
              {`${item.nombres} ${item.apellidos}`}
            </td>
            <td onClick={() => handleUpdate(item)}>
              {item.correo_electronico}
            </td>
            <td onClick={() => handleUpdate(item)}>
              <div>{item.nro_celular}</div>
            </td>
            <td onClick={() => handleUpdate(item)}>{item.rol}</td>
            <td onClick={() => handleUpdate(item)}>
              {item.estado ? "Activo" : "Inactivo"}
            </td>
            <td onClick={() => handleUpdate(item)}>{item.cargo}</td>
            <td onClick={() => handleUpdate(item)}>
              {item.nombre_dependencia}
            </td>
            <td
              className="option"
              onClick={() =>
                isActiveUser !== item.usuario_id
                  ? handleRemove(item.nro_cedula)
                  : null
              }
            >
              {isActiveUser !== item.usuario_id ? (
                <i className="bx bx-x"></i>
              ) : null}
            </td>
          </tr>
        ))}
      </tbody>
    </Table>
  );
};

export const TableCustomTeam = ({
  type,
  titleTable,
  data,
  handleEdit,
  handleRemove,
}) => {
  return (
    <Table striped bordered hover size="sm">
      <thead>
        <tr>
          {titleTable.map((item, index) => (
            <th key={index}>{item}</th>
          ))}
        </tr>
      </thead>
      <tbody>
        {data.map((item, index) => (
          <tr key={index}>
            <td>{item.equipo_id}</td>
            <td>{item.nombre_equipo}</td>
            <td>{item.descripcion_equipo}</td>
            <td>{item.name}</td>
            <td>{item.nombre_dependencia}</td>
            <td className="option">
              <i className="bx bx-edit" onClick={() => handleEdit(item)}></i>
              <i
                className="bx bx-x ml-2"
                onClick={() => handleRemove(item?.equipo_id)}
              ></i>
            </td>
          </tr>
        ))}
      </tbody>
    </Table>
  );
};
