export const typeStatus = {
  pending: Symbol("En pendiente"),
  pause: Symbol("En pausa"),
  complete: Symbol("Completada"),
  process: Symbol("En proceso"),
};
