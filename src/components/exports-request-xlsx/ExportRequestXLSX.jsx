import Workbook from "react-excel-workbook";
import { useExportRequestXLSX } from "../../hooks/components/useExportRequestXLSX";
import { SpinnerLazyCustom } from "../spinner/SpinnerLazyCustom";
import "./ExportRequestXLSX.scss";

export const ExportRequestXLSX = ({ data }) => {
  const { docs, loading } = useExportRequestXLSX();

  return (
    <>
      {loading ? (
        <SpinnerLazyCustom style="index__pageV3" />
      ) : (
        <div className="main__option__exports_data">
          <Workbook
            filename="example.xlsx"
            element={
              <div className="main__option_export__item">
                <div className="img__icon__btn">
                  <span>1vista</span>
                  <img className="img__icon" src="assets/excel.png"></img>
                </div>
              </div>
            }
          >
            <Workbook.Sheet data={data} name="Sheet A">
              <Workbook.Column label="Solicitud ID" value="solicitud_id" />
              <Workbook.Column label="Asunto" value="asunto" />
              <Workbook.Column
                label="Fecha Solicitud"
                value="fecha_solicitud"
              />
              <Workbook.Column label="Estado" value="estado" />
              <Workbook.Column label="Prioridad" value="prioridad" />
              <Workbook.Column label="Nombres" value="nombres" />
              <Workbook.Column label="Apellidos" value="apellidos" />
              <Workbook.Column label="Tipo Servicio" value="tipo_servicio" />
            </Workbook.Sheet>
          </Workbook>
          <Workbook
            filename="example.xlsx"
            element={
              <div className="main__option_export__item">
                <div className="img__icon__btn">
                  <span>todo</span>
                  <img className="img__icon" src="assets/excel.png"></img>
                </div>
              </div>
            }
          >
            <Workbook.Sheet data={docs} name="Sheet A">
              <Workbook.Column label="Solicitud ID" value="solicitud_id" />
              <Workbook.Column label="Asunto" value="asunto" />
              <Workbook.Column
                label="Fecha Solicitud"
                value="fecha_solicitud"
              />
              <Workbook.Column label="Estado" value="estado" />
              <Workbook.Column label="Prioridad" value="prioridad" />
              <Workbook.Column label="Nombres" value="nombres" />
              <Workbook.Column label="Apellidos" value="apellidos" />
              <Workbook.Column label="Tipo Servicio" value="tipo_servicio" />
            </Workbook.Sheet>
          </Workbook>
        </div>
      )}
    </>
  );
};
