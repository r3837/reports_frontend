import "./EmptyList.scss";
export const EmptyList = ({ title = "campos registrados" }) => {
  return (
    <>
      <div className="main__Empty">
        <span>No tienes {title}</span>
      </div>
      <div className="main__dividir__Empty"></div>
    </>
  );
};
