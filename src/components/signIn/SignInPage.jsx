import { Navigate } from "react-router-dom";
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";

import { useSignIn } from "../../hooks/components/useSignIn";
import isEmail from "validator/lib/isEmail";
import { useAuthOptions } from "../../hooks/components/useAuthOptions";
import "./SignInPage.scss";

const requiredE = (value) => {
  if (!value) {
    return (
      <div className="alert alert-danger" role="alert">
        El correo es obligatorio!
      </div>
    );
  }
};
const vEmail = (value) => {
  if (!isEmail(value)) {
    return (
      <div className="alert alert-danger" role="alert">
        El correo no es valido!
      </div>
    );
  }
};
const requiredP = (value) => {
  if (!value) {
    return (
      <div className="alert alert-danger" role="alert">
        El contraseña es obligatorio!
      </div>
    );
  }
};

const SignInPage = () => {
  const {
    form,
    onChangeUsername,
    handleLogin,
    onChangePassword,
    loading,
    checkBtn,
    email,
    password,
    logged,
  } = useSignIn();
  const { handleMenu, selectedMenu } = useAuthOptions();

  if (logged) {
    return <Navigate to="/" />;
  }
  return (
    <>
      <img src="assets/logo.png" className="profile-img-card-logo"></img>
      <Form onSubmit={handleLogin} ref={form}>
        <div className="form-group">
          <label htmlFor="username">Correo Electrónico</label>
          <div className="inner-addon left-addon form-group">
            <span className="glyphicon">
              <i className="bx bx-user"></i>
            </span>
            <Input
              type="text"
              className="form-control"
              name="username"
              value={email}
              onChange={onChangeUsername}
              validations={[requiredE, vEmail]}
            />
          </div>
        </div>
        <div className="form-group">
          <label htmlFor="password">Contraseña</label>
          <div className="inner-addon left-addon form-group">
            <span className="glyphicon">
              <i className="bx bx-lock"></i>
            </span>
            <Input
              type="password"
              className="form-control"
              name="password"
              value={password}
              onChange={onChangePassword}
              validations={[requiredP]}
            />
          </div>
        </div>

        <div className="form-group btn_login">
          <button className="btn__primary btn-block" disabled={loading}>
            {loading && (
              <span className="spinner-border spinner-border-sm"></span>
            )}
            <span>Iniciar sesion</span>
          </button>
        </div>
        <CheckButton style={{ display: "none" }} ref={checkBtn} />
      </Form>
    </>
  );
};
export default SignInPage;
