import { useMessagesViewObservation } from "../../hooks/components/useMessagesViewObservation";
import { SendViewMessageImbox } from "../send-view-message-imbox/SendViewMessageImbox";
import { StyleMessageObservation } from "../style-message-observation/StyleMessageObservation";
import "./MessagesViewObservation.scss";

export const MessagesViewObservation = () => {
  const { docs, observations, handlePagination, visivilityPagination } =
    useMessagesViewObservation();

  return (
    <div className="main__observation">
      <div className="main__observation__message">
        <div className="main__observation__message__render">
          {docs.length <= 0 ? (
            <div className="alert alert-primary" role="alert">
              No tienes observaciones en este momento!
            </div>
          ) : (
            docs.map((item, index) => (
              <div key={index} className="card__message">
                <div
                  className="main__observation__message__dividir"
                  style={{
                    marginTop: observations.totalPages > 1 ? "50px" : "15px",
                  }}
                >
                  {index + 1}
                </div>
                <div key={index}>
                  <StyleMessageObservation item={item} />
                </div>
              </div>
            ))
          )}

          {visivilityPagination && (
            <div
              className="main__observation__message__pagination"
              onClick={handlePagination}
            >
              Cargar mas mensajes...
            </div>
          )}
        </div>
      </div>
      <SendViewMessageImbox />
    </div>
  );
};
