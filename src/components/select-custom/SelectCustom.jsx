import Select from "react-select";

export const SelectCustom = ({ name, options, onChange, field }) => {
  return (
    <Select
      // className="basic-single"
      // // classNamePrefix={name}
      // placeholder={`${name.charAt(0).toUpperCase() + name.slice(1)}`.replaceAll(
      //   "_",
      //   " "
      // )}
      //   defaultValue={colourOptions[0]}
      // name={name}
      {...field}
      options={options}
      // onChange={({ value }) =>
      //   onChange({
      //     target: {
      //       name,
      //       value,
      //     },
      //   })
      // }
    />
  );
};
