import React from "react";
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";

import { useForgotPassword } from "../../hooks/components/useForgotPassword";
import isEmail from "validator/lib/isEmail";
import "./RecoverPassword.css";

const requiredE = (value) => {
  if (!value) {
    return (
      <div className="alert alert-danger" role="alert">
        El correo es obligatorio!
      </div>
    );
  }
};
const email = (value) => {
  if (!isEmail(value)) {
    return (
      <div className="alert alert-danger" role="alert">
        El correo no es valido!
      </div>
    );
  }
};

const RecoverPassword = ({ setNavegte }) => {
  const { form, email, onChangeEmail, handleForgot, loading, checkBtn } =
    useForgotPassword(setNavegte);
  return (
    <>
      <img src="assets/logo.png" className="profile-img-card"></img>
      <Form onSubmit={handleForgot} ref={form}>
        <div className="form-group">
          <label htmlFor="username">Correo Electrónico</label>
          <div className="inner-addon left-addon form-group">
            <span className="glyphicon">
              <i className="bx bx-lock"></i>
            </span>
            <Input
              type="text"
              className="form-control"
              name="email"
              value={email}
              onChange={onChangeEmail}
              validations={[requiredE]}
            />
          </div>
        </div>

        <div className="form-group btn_login">
          <button className="btn__primary btn-block" disabled={loading}>
            {loading && (
              <span className="spinner-border spinner-border-sm"></span>
            )}
            <span>Confirmar</span>
          </button>
        </div>
        <CheckButton style={{ display: "none" }} ref={checkBtn} />
      </Form>
    </>
  );
};
export default RecoverPassword;
