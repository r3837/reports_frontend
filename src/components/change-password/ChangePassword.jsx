import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";
import { useChangePassword } from "../../hooks/components/useChangePassword";
import "./ChangePassword.scss";

const requiredP = (value) => {
  if (!value) {
    return (
      <div className="alert alert-danger" role="alert">
        La contraseña es obligatorio!
      </div>
    );
  }
};
const requiredS = (value) => {
  if (!value) {
    return (
      <div className="alert alert-danger" role="alert">
        La contraseña es obligatorio!
      </div>
    );
  }
};

export const ChangePassword = () => {
  const {
    form,
    onChangeConfirmPassword,
    handleChangePassword,
    onChangePassword,
    loading,
    checkBtn,
    password,
    confirmPsssword,
  } = useChangePassword();

  return (
    <>
      <img src="assets/logo.png" className="profile-img-card"></img>
      <div className="title-change-password">Cambiar contraseña</div>
      <Form onSubmit={handleChangePassword} ref={form}>
        <div className="form-group">
          <label htmlFor="password">Nueva Contraseña</label>
          <div className="inner-addon left-addon form-group">
            <span className="glyphicon">
              <i className="bx bx-lock"></i>
            </span>
            <Input
              type="password"
              className="form-control"
              name="password"
              value={password}
              onChange={onChangePassword}
              validations={[requiredP]}
            />
          </div>
        </div>
        <div className="form-group">
          <label htmlFor="password">Confirmar Contraseña</label>
          <div className="inner-addon left-addon form-group">
            <span className="glyphicon">
              <i className="bx bx-lock"></i>
            </span>
            <Input
              type="password"
              className="form-control"
              name="confirm-password"
              value={confirmPsssword}
              onChange={onChangeConfirmPassword}
              validations={[requiredS]}
            />
          </div>
        </div>

        <div className="form-group btn_login">
          <button className="btn__primary btn-block" disabled={loading}>
            {loading && (
              <span className="spinner-border spinner-border-sm"></span>
            )}
            <span>Confirmar</span>
          </button>
        </div>
        <CheckButton style={{ display: "none" }} ref={checkBtn} />
      </Form>
    </>
  );
};
