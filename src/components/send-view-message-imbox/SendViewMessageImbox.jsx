import React from "react";
import { ErrorMessage } from "@hookform/error-message";
import { Controller } from "react-hook-form";
import { RequiredAlertErrors } from "../required-alert-errors/RequiredAlertErrors";
import { useSendViewMessageImbox } from "../../hooks/components/useSendViewMessageImbox";
import "./SendViewMessageImbox.scss";

export const SendViewMessageImbox = () => {
  const { control, errors, handleSubmit, handleSendRequest } =
    useSendViewMessageImbox();

  return (
    <form
      onSubmit={handleSubmit(handleSendRequest)}
      className="main__observation__send">
      <div className="form-group">
        <Controller
          name="asunto"
          control={control}
          rules={{
            required: "El mensaje al menos debe contener algo esta vacio!",
          }}
          render={({ field }) => (
            <textarea
              {...field}
              name="asunto"
              className="form-control"
              placeholder="Asunto*"
              rows="6"
            />
          )}
        />
        <ErrorMessage
          errors={errors}
          name="asunto"
          render={({ message }) => RequiredAlertErrors(message)}
        />
      </div>
      <button className="css-button-sliding-to-left--black">Enviar</button>
    </form>
  );
};
