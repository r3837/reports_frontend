import { useState } from "react";
import { Modal } from "react-bootstrap";
import { NewSend } from "../new-send/NewSend";
import "./ModalNewRequest.scss";

export const ModalNewRequest = () => {
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  return (
    <>
      <div className="new__request__write" onClick={handleShow}>
        <span className="css-button-sliding-to-left--black">Mensaje</span>
      </div>

      <Modal
        show={show}
        onHide={handleClose}
        backdrop="static"
        scrollable
        keyboard={false}
        className="new__request"
      >
        <Modal.Header className="new__request__header" closeButton>
          <div className="new__request__title">
            <span>Nueva Solicitud</span>
          </div>
        </Modal.Header>
        <Modal.Body>
          <NewSend handleClose={handleClose} />
        </Modal.Body>
      </Modal>
    </>
  );
};
