import "./IndicatorPage.scss";
export const IndicatorPage = ({ title }) => {
  return (
    <div className="indicator__page">
      <i className="bx bxs-home"></i>
      <span>/ {title}</span>
    </div>
  );
};
