import { Spinner } from "react-bootstrap";
import "./SpinnerLazyCustom.scss";

export const SpinnerLazyCustom = ({ style }) => {
  return (
    <div className={` ${style ?? "content_loading"}`}>
      <Spinner animation="grow" className="loading" />
    </div>
  );
};
