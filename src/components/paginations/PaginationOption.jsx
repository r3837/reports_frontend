import "./PaginationOption.scss";

export const PaginationOption = ({ page = 0, onClickLeft, onClickRight }) => {
  return (
    <div className="main__SendImbox__pagination">
      <div className="pagination__option" onClick={onClickLeft}>
        <i className="bx bx-left-arrow-alt"></i>
      </div>
      <div className="main__SendImbox__pagination__indicator">
        <span>pagina:</span>
        <span className="page">{page}</span>
      </div>
      <div className="pagination__option" onClick={onClickRight}>
        <i className="bx bx-right-arrow-alt"></i>
      </div>
    </div>
  );
};
