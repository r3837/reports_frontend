import { useEffect, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useLocation } from "react-router-dom";
import { logoutApi } from "../../../actions/auth";
import "./Sidebar.scss";

export const Sidebar = () => {
  const dispatch = useDispatch();
  const { user } = useSelector((state) => state.auth);
  const [activeIndex, setActiveIndex] = useState(0);
  const [stepHeight, setStepHeight] = useState(0);
  const sidebarRef = useRef();
  const indicatorRef = useRef();
  const location = useLocation();

  useEffect(() => {
    setTimeout(() => {
      const sidebarItem = sidebarRef.current.querySelector(
        ".sidebar__menu__item"
      );
      indicatorRef.current.style.height = `${sidebarItem.clientHeight}px`;
      setStepHeight(sidebarItem.clientHeight);
    }, 50);
  }, []);

  const items = (isRol) => {
    let items = [
      {
        display: "Perfil",
        icon: <i className="bx bxs-user-circle"></i>,
        to: "/",
        section: "",
      },
      {
        display: "Bandeja de entrada",
        icon: <i className="bx bx-mail-send"></i>,
        to: "/solicitudes-entrantes",
        section: "solicitudes-entrantes",
      },
    ];

    if (isRol === "solicitante") {
      items.push({
        display: "Solicitudes enviadas",
        icon: <i className="bx bx-send"></i>,
        to: "/solicitudes-enviadas",
        section: "solicitudes-enviadas",
      });
    } else {
      items.push(
        ...[
          {
            display: "Registro cuenta",
            icon: <i className="bx bx-plus-circle"></i>,
            to: "/registrar",
            section: "registrar",
          },
          {
            display: "Registro opciones",
            icon: <i className="bx bx-plus-circle"></i>,
            to: "/registrar-datos-del-sistema",
            section: "registrar-datos-del-sistema",
          },
        ]
      );
    }

    return items;
  };

  // change active index

  useEffect(() => {
    const curPath = window.location.pathname.split("/")[1];
    if (curPath === "mensajes") return;
    const activeIndex = items(user.rol).findIndex(
      (item) => item.section === curPath
    );

    setActiveIndex(curPath.length === 0 ? 0 : activeIndex);
  }, [location]);

  return (
    <div className="sidebar">
      {/* <div className="sidebar__logo">Logo</div> */}
      <div className="sidebar__logo">
        <img src="assets/logo.png" className="logo__main"></img>
      </div>
      <div ref={sidebarRef} className="sidebar__menu">
        <div
          ref={indicatorRef}
          className="sidebar__menu__indicator"
          style={{
            transform: `translateX(-50%) translateY(${
              activeIndex * stepHeight
            }px)`,
          }}
        ></div>
        {items(user.rol).map((item, index) => {
          return (
            <Link key={index} to={item.to}>
              <div
                className={` sidebar__menu__item ${
                  activeIndex === index ? "active" : ""
                } `}
              >
                <div className="sidebar__menu__item__icon">{item.icon}</div>
                <div className="sidebar__menu__item__text">{item.display}</div>
              </div>
            </Link>
          );
        })}
      </div>
      <div className="sidebar__sesion">
        <div
          className="sidebar__sesion__btn__out"
          onClick={() => dispatch(logoutApi())}
        >
          <span>Cerrar sesion</span>
          <i className="bx bx-log-out-circle icon_logout"></i>
        </div>
      </div>
    </div>
  );
};
