
export const RequiredAlertErrors = (value) => {
  return (
    <div className="alert alert-danger" role="alert">
      {value}
    </div>
  );
};
