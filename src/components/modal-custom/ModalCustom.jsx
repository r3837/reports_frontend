import { Modal } from "react-bootstrap";
import "./ModalCustom.scss";

export const ModalCustom = ({
  show,
  handleClose,
  handleShow,
  childen,
  isButton = false,
  fullscreen = true,
  title = "",
}) => {
  return (
    <>
      {isButton && <i className="bx bx-plus-circle" style={{ color: '#fff' }} onClick={handleShow}></i>}
      <Modal
        show={show}
        onHide={handleClose}
        backdrop="static"
        scrollable
        dialogClassName="modal-90w"
        keyboard={false}
        className="new__new__user"
        fullscreen={fullscreen}
      >
        <Modal.Header className="new__new__user__header" closeButton>
          {title}
        </Modal.Header>
        <Modal.Body>{childen}</Modal.Body>
      </Modal>
    </>
  );
};
