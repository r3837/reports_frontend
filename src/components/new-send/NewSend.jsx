import { useNewSend } from "../../hooks/components/useNewSend";
import { optionsPriority } from "../select-custom/utils/ParamsSelect";
import Select from "react-select";
import { ErrorMessage } from "@hookform/error-message";
import { Controller } from "react-hook-form";

import "./NewSend.scss";

const required = (value) => {
  return (
    <div className="alert alert-danger" role="alert">
      {value}
    </div>
  );
};

export const NewSend = ({ handleClose }) => {
  const {
    handleSendRequest,
    settingRequestTypes,
    settingsResponsables,
    settingRequestTeams,
    loading,
    control,
    handleSubmit,
    errors,
  } = useNewSend(handleClose);

  return (
    <section className="contactus" id="contact">
      <div className="container">
        <div className="row">
          <div className="col-sm-12" data-aos="fade-up" data-aos-offset="-500">
            <form onSubmit={handleSubmit(handleSendRequest)}>
              <div className="row">
                <div className="col-sm-6">
                  <Controller
                    name="tipo_servicio"
                    control={control}
                    rules={{
                      required:
                        "El tipo de servicio es obligatorio!",
                    }}
                    render={({ field }) => (
                      <Select
                        {...field}
                        placeholder="Tipo servicio"
                        options={settingRequestTypes ?? []}
                      />
                    )}
                  />
                  <ErrorMessage
                    errors={errors}
                    name="tipo_servicio"
                    render={({ message }) => required(message)}
                  />
                </div>
                <div className="col-sm-6">
                  <Controller
                    name="prioridad"
                    control={control}
                    rules={{ required: "La prioridad es obligatorio!" }}
                    render={({ field }) => (
                      <Select
                        {...field}
                        placeholder="Prioridad"
                        options={optionsPriority ?? []}
                      />
                    )}
                  />
                  <ErrorMessage
                    errors={errors}
                    name="prioridad"
                    render={({ message }) => required(message)}
                  />
                </div>
                <div className="col-sm-6 mt-2">
                  <Controller
                    name="equipo_id"
                    control={control}
                    rules={{ required: "El equipo es obligatorio!" }}
                    render={({ field }) => (
                      <Select
                        {...field}
                        placeholder="Equipo"
                        options={settingRequestTeams ?? []}
                      />
                    )}
                  />
                  <ErrorMessage
                    errors={errors}
                    name="equipo_id"
                    render={({ message }) => required(message)}
                  />
                </div>
                <div className="col-12 mt-2">
                  <Controller
                    name="responsable"
                    control={control}
                    rules={{
                      required: "El responsable es obligatorio!",
                    }}
                    render={({ field }) => (
                      <Select
                        {...field}
                        placeholder="Responsable"
                        options={settingsResponsables ?? []}
                      />
                    )}
                  />
                  <ErrorMessage
                    errors={errors}
                    name="responsable"
                    render={({ message }) => required(message)}
                  />
                </div>
                <div className="col-sm-12 mt-3">
                  <div className="form-group">
                    <Controller
                      name="asunto"
                      control={control}
                      rules={{
                        required:
                          "El mensaje al menos debe contener algo esta vacio!                      ",
                      }}
                      render={({ field }) => (
                        <textarea
                          {...field}
                          name="asunto"
                          className="form-control"
                          placeholder="Asunto*"
                          rows="16"
                        />
                      )}
                    />
                    <ErrorMessage
                      errors={errors}
                      name="asunto"
                      render={({ message }) => required(message)}
                    />
                  </div>
                </div>
              </div>
              <div className="form-group new__request__option">
                <button
                  className="new__request__option__button"
                  disabled={loading}
                >
                  {loading && (
                    <span className="spinner-border spinner-border-sm"></span>
                  )}
                  <span>Solicitar</span>
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
  );
};
