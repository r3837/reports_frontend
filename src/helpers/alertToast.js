import Swal from "sweetalert2";

const Toast = Swal.mixin({
  toast: true,
  position: "top-end",
  showConfirmButton: false,
  timer: 3000,
  timerProgressBar: true,
  didOpen: (toast) => {
    toast.addEventListener("mouseenter", Swal.stopTimer);
    toast.addEventListener("mouseleave", Swal.resumeTimer);
  },
});

export const toastAlert = (icon = "success", title) =>
  Toast.fire({
    icon,
    title,
  });

export const toastAlertConfirm = (
  icon = "warning",
  title,
  subtitle,
  confirmButtonText,
  event
) => {
  Swal.fire({
    title,
    text: subtitle,
    icon: icon,
    showCancelButton: true,
    confirmButtonColor: "#3085d6",
    cancelButtonColor: "#d33",
    confirmButtonText,
  }).then((result) => {
    if (result.isConfirmed) {
      event();
    }
  });
};
