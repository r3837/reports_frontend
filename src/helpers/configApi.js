export const configApi = (data, { method = "POST" }, token = "") => {
  let headers = {
    "Access-Control-Allow-Origin": "*",
    "Content-Type": "application/json",
  };

  if (token !== "" || token !== null) {
    headers = {
      ...headers,
      'x-token': token,
    };
  }
  if (data) {
    return {
      method,
      headers,
      body: JSON.stringify(data),
    };
  }
  return {
    method,
    headers,
  };
};
