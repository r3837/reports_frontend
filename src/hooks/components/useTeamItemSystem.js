import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  getTeamsApi,
  removeRoleApi,
  removeTeamApi,
  searchSettingTeamApi,
} from "../../actions/register-settings";
import { toastAlert, toastAlertConfirm } from "../../helpers/alertToast";

export const useTeamItemSystem = () => {
  const dispatch = useDispatch();

  const {
    settingUserTeam,
    loadingSettingUserTeam,
    paginationTeamPrev,
    textSearchSetting,
    searchSettingUserTeam,
    activeSearchSetting: searchActive,
  } = useSelector((state) => state.registerSettings);

  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const [updateTeam, setUpdateTeam] = useState(null);

  useEffect(() => {
    if (settingUserTeam?.docs.length <= 0) {
      dispatch(getTeamsApi({ page: 1, limit: 10 }));
    }
  }, []);

  useEffect(() => {
    if (paginationTeamPrev) {
      handlePaginationLeft();
    }
  }, [paginationTeamPrev]);

  const handlePaginationLeft = () => {
    if (searchActive) {
      if (searchSettingUserTeam?.page > 1) {
        dispatch(
          searchSettingTeamApi({
            query: textSearchSetting,
            page: searchSettingUserTeam?.page - 1,
            limit: process.env.REACT_APP_LIMIT_PAGINATION,
          })
        );
      } else {
        toastAlert("success", "Eso es todo!");
      }
    } else {
      if (settingUserTeam?.page > 1) {
        dispatch(
          getTeamsApi({
            page: settingUserTeam?.page - 1,
            limit: process.env.REACT_APP_LIMIT_PAGINATION,
          })
        );
      } else {
        toastAlert("success", "Eso es todo!");
      }
    }
  };

  const handlePaginationRight = () => {
    if (searchActive) {
      if (settingUserTeam?.page < settingUserTeam?.totalPages) {
        dispatch(
          searchSettingTeamApi({
            query: textSearchSetting,
            page: settingUserTeam?.page - 1,
            limit: process.env.REACT_APP_LIMIT_PAGINATION,
          })
        );
      } else {
        toastAlert("success", "Eso es todo!");
      }
    } else {
      if (settingUserTeam?.page < settingUserTeam?.totalPages) {
        dispatch(getTeamsApi({ page: settingUserTeam?.page + 1, limit: 10 }));
      } else {
        toastAlert("success", "Eso es todo!");
      }
    }
  };

  const handleRemoveTeam = (equipo_id) => {
    toastAlertConfirm(
      "warning",
      "Eliminar",
      "Estas seguro en eliminar el Equipo...",
      "Eliminar",
      () => {
        dispatch(removeTeamApi({ equipo_id }));
      }
    );
  };

  return {
    loadingSettingUserTeam,
    docs: searchActive
      ? searchSettingUserTeam.docs
      : settingUserTeam?.docs ?? [],
    page: searchActive
      ? searchSettingUserTeam?.page
      : settingUserTeam?.page ?? 0,
    show,
    handleClose,
    handleShow,
    handleRemoveTeam,
    updateTeam,
    setUpdateTeam,
    page: settingUserTeam?.page,
    handlePaginationLeft,
    handlePaginationRight,
  };
};
