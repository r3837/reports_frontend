import { useState } from "react";
import { useEffect } from "react";
import { useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { forgotPasswordApi, resetNavegateLogin } from "../../actions/auth";

export const useForgotPassword = (setNavegte) => {
  const form = useRef();
  const checkBtn = useRef();
  const dispatch = useDispatch();

  const [email, setEmail] = useState("");
  const [loading, setLoading] = useState(false);
  const { navegateLogin } = useSelector((state) => state.auth);

  useEffect(() => {
    if (navegateLogin) {
      setNavegte(0)
      dispatch(resetNavegateLogin(false));
    }
  }, [navegateLogin]);

  const onChangeEmail = (e) => {
    const email = e.target.value;
    setEmail(email);
  };

  const handleForgot = (e) => {
    e.preventDefault();
    setLoading(true);
    form.current.validateAll();
    if (checkBtn.current.context._errors.length === 0) {
      dispatch(forgotPasswordApi({ correo_electronico: email }));
    } else {
      setLoading(false);
    }
  };

  return {
    form,
    onChangeEmail,
    handleForgot,
    loading,
    checkBtn,
    email
  };
};
