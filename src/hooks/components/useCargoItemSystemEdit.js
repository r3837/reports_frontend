import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { updateCargosApi } from "../../actions/register-settings";

export const useCargoItemSystemEdit = (item, handleClose) => {
  const dispatch = useDispatch();
  const { loadingUpdateSettingCargo } = useSelector(
    (state) => state.registerSettings
  );

  const {
    control,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm({
    defaultValues: {
      cargo_id: item?.cargo_id,
      cargo: item?.cargo,
    },
  });


  const handleUpdateCargo = (data) => {
    dispatch(updateCargosApi(data, handleClose));
  };

  return {
    handleUpdateCargo,
    control,
    handleSubmit,
    errors,
    loading: loadingUpdateSettingCargo,
  };
};
