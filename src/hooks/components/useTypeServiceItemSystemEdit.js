import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { updateTypeServiceApi } from "../../actions/register-settings";

export const useTypeServiceItemSystemEdit = (item, handleClose) => {
  const dispatch = useDispatch();
  const { loadingUpdateSettingTypeService } = useSelector(
    (state) => state.registerSettings
  );

  const {
    control,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm({
    defaultValues: {
      tipo_servicio_id: item?.tipo_servicio_id,
      tipo_servicio: item?.tipo_servicio,
      observacion: item?.observacion?.length > 0 ? item?.observacion : ""
    },
  });

  const handleUpdateTypeService = (data) => {
    dispatch(updateTypeServiceApi(data, handleClose));
  };

  return {
    handleUpdateTypeService,
    control,
    handleSubmit,
    errors,
    loading: loadingUpdateSettingTypeService,
  };
};
