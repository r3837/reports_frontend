import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { updateCargosApi, updateRolApi } from "../../actions/register-settings";

export const useRolItemSystemEdit = (item, handleClose) => {
  const dispatch = useDispatch();
  const { loadingUpdateSettingRol } = useSelector(
    (state) => state.registerSettings
  );

  const {
    control,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm({
    defaultValues: {
      rol: item,
    },
  });

  const handleUpdateRol = (data) => {
    dispatch(updateRolApi({ oldRol: item, ...data }, handleClose));
  };

  return {
    handleUpdateRol,
    control,
    handleSubmit,
    errors,
    loading: loadingUpdateSettingRol,
  };
};
