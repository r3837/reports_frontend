import { useEffect } from "react";
import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { updateTeamApi } from "../../actions/register-settings";
import { settingForTeamApi } from "../../actions/settings";

export const useTeamItemSystemEdit = (item, handleClose) => {
  const dispatch = useDispatch();
  const { loadingUpdateSettingTeam } = useSelector(
    (state) => state.registerSettings
  );

  const { settingUserDependency, settingUserTypeTeam } = useSelector(
    (state) => state.settings
  );

  console.log(item);

  const {
    control,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm({
    defaultValues: {
      equipo_id: item?.equipo_id,
      nombre_equipo: item?.nombre_equipo,
      descripcion_equipo: item?.descripcion_equipo,
      dependencia_id: {
        value: item?.dependencia_id,
        label: item.nombre_dependencia,
      },
      tipo_equipo_id: {
        value: item?.tipo_equipo_id,
        label: item.name,
      },
    },
  });

  useEffect(() => {
    if (
      settingUserDependency?.length <= 0 ||
      settingUserTypeTeam?.length <= 0
    ) {
      dispatch(settingForTeamApi());
    }
  }, []);

  const handleUpdateTeam = (data) => {
    const {
      equipo_id,
      nombre_equipo,
      descripcion_equipo,
      dependencia_id: { value: dependenciaId },
      tipo_equipo_id: { value: tipoEquipoId },
    } = data;
    dispatch(
      updateTeamApi(
        {
          equipo_id,
          nombre_equipo,
          descripcion_equipo,
          dependencia_id: dependenciaId,
          tipo_equipo_id: tipoEquipoId,
        },
        handleClose
      )
    );
  };

  return {
    handleUpdateTeam,
    control,
    handleSubmit,
    errors,
    loading: loadingUpdateSettingTeam,
    settingTypeTeam: settingUserTypeTeam,
    settingDependency: settingUserDependency,
  };
};
