import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { updateDependencyApi } from "../../actions/register-settings";

export const useDependencyItemSystemEdit = (item, handleClose) => {
  const dispatch = useDispatch();
  const { loadingUpdateSettingDependency } = useSelector(
    (state) => state.registerSettings
  );

  const {
    control,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm({
    defaultValues: {
      nombre_dependencia: item?.nombre_dependencia,
      dependencia_id: item?.dependencia_id,
    },
  });

  const handleUpdateDependency = (data) => {
    dispatch(updateDependencyApi(data, handleClose));
  };

  return {
    handleUpdateDependency,
    control,
    handleSubmit,
    errors,
    loading: loadingUpdateSettingDependency,
  };
};
