import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { sendImboxObservationsApi } from "../../actions/imbox";
import { toastAlert } from "../../helpers/alertToast";
import ContextSocket from "../../context/context-socketio";
import { useContext } from "react";

export const useSendViewMessageImbox = () => {
  const { id } = useParams();
  const dispatch = useDispatch();
  const Socket = useContext(ContextSocket);

  const { viewMessage } = useSelector((state) => state.imbox);
  const { user } = useSelector((state) => state.auth);

  const {
    control,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm({
    defaultValues: {
      asunto: "",
      solicitud_id: id.replaceAll(":", ""),
    },
  });

  const handleSendRequest = async (data) => {
    const { usuario_responsable, usuario_solicitante } = viewMessage;
    Socket?.emit("on_message_observation", {
      ...data,
      rol: user?.rol,
      usuario_id_sender:
        user?.usuario_id === usuario_responsable
          ? usuario_responsable
          : usuario_solicitante,
      usuario_id_receiver:
        user?.usuario_id === usuario_responsable
          ? usuario_solicitante
          : usuario_responsable,
    });
    reset();
  };
  return {
    control,
    errors,
    handleSubmit,
    handleSendRequest,
  };
};
