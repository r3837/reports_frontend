import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  getDependencyApi,
  removeDependencyApi,
  searchSettingDependencyApi,
} from "../../actions/register-settings";
import { toastAlert, toastAlertConfirm } from "../../helpers/alertToast";

export const useDependencyItemSystem = () => {
  const dispatch = useDispatch();
  const {
    settingUserDependency,
    loadingSettingUserDependency,
    paginationDependencyPrev,
    textSearchSetting,
    searchSettingUserDependency,
    activeSearchSetting: searchActive,
  } = useSelector((state) => state.registerSettings);

  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const [updateDependency, setUpdateDependency] = useState(null);

  useEffect(() => {
    if (settingUserDependency?.docs.length <= 0) {
      dispatch(getDependencyApi({ page: 1, limit: 10 }));
    }
  }, []);

  useEffect(() => {
    if (paginationDependencyPrev) {
      handlePaginationLeft();
    }
  }, [paginationDependencyPrev]);

  const handlePaginationLeft = () => {
    if (searchActive) {
      if (searchSettingUserDependency?.page > 1) {
        dispatch(
          searchSettingDependencyApi({
            query: textSearchSetting,
            page: searchSettingUserDependency?.page - 1,
            limit: process.env.REACT_APP_LIMIT_PAGINATION,
          })
        );
      } else {
        toastAlert("success", "Eso es todo!");
      }
    } else {
      if (settingUserDependency?.page > 1) {
        dispatch(
          getDependencyApi({ page: settingUserDependency?.page - 1, limit: 10 })
        );
      } else {
        toastAlert("success", "Eso es todo!");
      }
    }
  };

  const handlePaginationRight = () => {
    if (searchActive) {
      if (
        searchSettingUserDependency?.page <
        searchSettingUserDependency?.totalPages
      ) {
        dispatch(
          searchSettingDependencyApi({
            query: textSearchSetting,
            page: searchSettingUserDependency?.page - 1,
            limit: process.env.REACT_APP_LIMIT_PAGINATION,
          })
        );
      } else {
        toastAlert("success", "Eso es todo!");
      }
    } else {
      if (settingUserDependency?.page < settingUserDependency?.totalPages) {
        dispatch(
          getDependencyApi({ page: settingUserDependency?.page + 1, limit: 10 })
        );
      } else {
        toastAlert("success", "Eso es todo!");
      }
    }
  };

  const handleRemoveDependency = ({ id }) => {
    toastAlertConfirm(
      "warning",
      "Eliminar",
      "Estas seguro en eliminar la Dependencia...",
      "Eliminar",
      () => {
        dispatch(removeDependencyApi({ dependencia_id: id }));
      }
    );
  };

  return {
    loadingSettingUserDependency,
    docs: searchActive
      ? searchSettingUserDependency.docs
      : settingUserDependency?.docs ?? [],
    page: searchActive
      ? searchSettingUserDependency?.page
      : settingUserDependency?.page ?? 0,
    show,
    handleClose,
    handleShow,
    handleRemoveDependency,
    updateDependency,
    setUpdateDependency,
    page: settingUserDependency?.page,
    handlePaginationLeft,
    handlePaginationRight,
  };
};
