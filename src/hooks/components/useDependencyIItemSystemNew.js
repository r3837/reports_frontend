import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { createDependencyApi } from "../../actions/register-settings";

export const useDependencyItemSystemNew = (handleClose) => {
  const dispatch = useDispatch();
  const { loadingSettingUserDependency } = useSelector(
    (state) => state.registerSettings
  );

  const {
    control,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm({
    defaultValues: {
      nombre_dependencia: "",
    },
  });

  const handleCreateDependency = (data) => {
    dispatch(createDependencyApi(data, handleClose));
  };

  return {
    handleCreateDependency,
    control,
    handleSubmit,
    errors,
    loading: loadingSettingUserDependency,
  };
};
