import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { saveUserApi } from "../../actions/registered-users";
import { titleCase } from "../../helpers/titleCase";

export const useUpdateUser = (close, item) => {
  const dispatch = useDispatch();
  const { loadingRegister } = useSelector((state) => state.users);

  const {
    loadingeSettingUser,
    settingUserRol,
    settingUserDependency,
    settingUserCargo,
    settingUserStatus,
  } = useSelector((state) => state.settings);

  const {
    control,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm({
    defaultValues: {
      nro_cedula: item?.nro_cedula,
      nombres: item?.nombres,
      apellidos: item?.apellidos,
      correo_electronico: item?.correo_electronico,
      nro_celular: item?.nro_celular,
      rol: { value: item?.rol, label: titleCase(item?.rol) },
      estado: {
        value: item?.estado,
        label: item?.estado ? "Activo" : "Inactivo",
      },
      dependencia_id: {
        value: item?.dependencia_id,
        label: item?.nombre_dependencia,
      },
      cargo_id: {
        value: item?.cargo_id,
        label: titleCase(item?.cargo),
      },
    },
  });

  const handleUpdateUser = (data) => {
    const {
      nro_cedula,
      nombres,
      apellidos,
      correo_electronico,
      nro_celular,
      rol: { value: rolValue },
      estado: { value: estadoValue },
      dependencia_id: { value: dependenciaIdValue },
      cargo_id: { value: cargoValue },
    } = data;

    dispatch(
      saveUserApi(
        {
          usuario_id: item?.usuario_id,
          nro_cedula,
          nombres,
          apellidos,
          correo_electronico,
          nro_celular,
          rol: rolValue,
          estado: estadoValue,
          dependencia_id: dependenciaIdValue,
          cargo: cargoValue,
        },
        close
      )
    );
  };

  return {
    handleUpdateUser,
    handleSubmit,
    control,
    errors,
    loading: loadingRegister,
    loadingeSettingUser,
    settingUserRol,
    settingUserDependency,
    settingUserCargo,
    settingUserStatus,
  };
};
