import { useEffect, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import {
  imboxObservations,
  imboxObservationsApi,
  imboxObservationsReset,
} from "../../actions/imbox";

export const useMessagesViewObservation = () => {
  const { id } = useParams();
  const dispatch = useDispatch();
  const sId = useRef(id);

  const { user } = useSelector((state) => state.auth);
  const { renderMessageId } = useSelector((state) => state.imbox);

  const { loadingViewMessage, viewMessage, loadingObservation } = useSelector(
    (state) => state.imbox
  );

  useEffect(() => {
    if (
      renderMessageId !== sId.current
    ) {
      dispatch(imboxObservationsReset());
      dispatch(imboxObservationsApi({ id, page: 1, limit: 4 }));
      sId.current = id;
    }
  }, [renderMessageId]);


  const handlePagination = () => {
    viewMessage.observations?.page < viewMessage.observations?.totalPages &&
      dispatch(
        imboxObservationsApi({
          id,
          page: viewMessage.observations?.page + 1,
          limit: 4,
        })
      );
  };

  return {
    loadingViewMessage,
    docs: viewMessage?.observations?.docs ?? [],
    observations: viewMessage?.observations,
    handlePagination,
    visivilityPagination:
      viewMessage.observations?.page < viewMessage.observations?.totalPages,
  };
};
