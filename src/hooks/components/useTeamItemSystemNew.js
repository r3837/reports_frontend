import { useEffect } from "react";
import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { createRolApi, createTeamApi } from "../../actions/register-settings";
import { settingForTeamApi } from "../../actions/settings";

export const useTeamItemSystemNew = (handleClose) => {
  const dispatch = useDispatch();
  const { loadingSettingUserTeam } = useSelector(
    (state) => state.registerSettings
  );

  const { settingUserDependency, settingUserTypeTeam } = useSelector(
    (state) => state.settings
  );

  const {
    control,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm({
    defaultValues: {
      equipo_id: "",
      nombre_equipo: "",
      descripcion_equipo: "",
      dependencia_id: 0,
      tipo_equipo_id: 0,
    },
  });

  useEffect(() => {
    if (
      settingUserDependency?.length <= 0 ||
      settingUserTypeTeam?.length <= 0
    ) {
      dispatch(settingForTeamApi());
    }
  }, []);

  const handleCreateTeam = (data) => {
    const {
      equipo_id,
      nombre_equipo,
      descripcion_equipo,
      dependencia_id: { value: dependenciaId },
      tipo_equipo_id: { value: tipoEquipoId },
    } = data;

    console.log({
      equipo_id,
      nombre_equipo,
      descripcion_equipo,
      dependencia_id: dependenciaId,
      tipo_equipo_id: tipoEquipoId,
    });
    dispatch(
      createTeamApi(
        {
          equipo_id,
          nombre_equipo,
          descripcion_equipo,
          dependencia_id: dependenciaId,
          tipo_equipo_id: tipoEquipoId,
        },
        handleClose
      )
    );
  };

  return {
    handleCreateTeam,
    control,
    handleSubmit,
    errors,
    loading: loadingSettingUserTeam,
    settingTypeTeam: settingUserTypeTeam,
    settingDependency: settingUserDependency,
  };
};
