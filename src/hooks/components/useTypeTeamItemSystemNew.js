import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { createTypeTeamApi } from "../../actions/register-settings";

export const useTypeTeamItemSystemNew = (handleClose) => {
  const dispatch = useDispatch();
  const { loadingSettingUserTypeTeam } = useSelector(
    (state) => state.registerSettings
  );

  const {
    control,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm({
    defaultValues: {
      name: "",
    },
  });

  const handleCreateTypeTeam = (data) => {
    dispatch(createTypeTeamApi(data, handleClose));
  };

  return {
    handleCreateTypeTeam,
    control,
    handleSubmit,
    errors,
    loading: loadingSettingUserTypeTeam,
  };
};
