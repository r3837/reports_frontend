import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  getTypeServicesApi,
  removeTypeServiceApi,
  searchSettingTypeServiceApi,
} from "../../actions/register-settings";
import { toastAlert, toastAlertConfirm } from "../../helpers/alertToast";

export const useTypeSystemItemSystem = () => {
  const dispatch = useDispatch();

  const {
    settingUserTypeService,
    loadingSettingUserTypeService,
    paginationTypeServicePrev,
    searchSettingUserTypeService,
    textSearchSetting,
    activeSearchSetting: searchActive,
  } = useSelector((state) => state.registerSettings);

  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const [updateTypeService, setUpdateTypeService] = useState(null);

  useEffect(() => {
    if (settingUserTypeService?.docs.length <= 0) {
      dispatch(getTypeServicesApi({ page: 1, limit: 10 }));
    }
  }, []);

  useEffect(() => {
    if (paginationTypeServicePrev) {
      handlePaginationLeft();
    }
  }, [paginationTypeServicePrev]);

  const handlePaginationLeft = () => {
    if (searchActive) {
      if (searchSettingUserTypeService?.page > 1) {
        dispatch(
          searchSettingTypeServiceApi({
            query: textSearchSetting,
            page: searchSettingUserTypeService?.page - 1,
            limit: process.env.REACT_APP_LIMIT_PAGINATION,
          })
        );
      } else {
        toastAlert("success", "Eso es todo!");
      }
    } else {
      if (settingUserTypeService?.page > 1) {
        dispatch(
          getTypeServicesApi({
            page: settingUserTypeService?.page - 1,
            limit: 10,
          })
        );
      } else {
        toastAlert("success", "Eso es todo!");
      }
    }
  };

  const handlePaginationRight = () => {
    if (searchActive) {
      if (
        searchSettingUserTypeService?.page <
        searchSettingUserTypeService?.totalPages
      ) {
        dispatch(
          searchSettingTypeServiceApi({
            query: textSearchSetting,
            page: searchSettingUserTypeService?.page - 1,
            limit: process.env.REACT_APP_LIMIT_PAGINATION,
          })
        );
      } else {
        toastAlert("success", "Eso es todo!");
      }
    } else {
      if (settingUserTypeService?.page < settingUserTypeService?.totalPages) {
        dispatch(
          getTypeServicesApi({
            page: settingUserTypeService?.page + 1,
            limit: 10,
          })
        );
      } else {
        toastAlert("success", "Eso es todo!");
      }
    }
  };

  const handleRemoveTypeService = ({ id }) => {
    toastAlertConfirm(
      "warning",
      "Eliminar",
      "Estas seguro en eliminar el tipo servicio...",
      "Eliminar",
      () => {
        console.log(id);
        dispatch(removeTypeServiceApi({ tipo_servicio_id: id }));
      }
    );
  };

  return {
    loadingSettingUserTypeService,
    docs: searchActive
      ? searchSettingUserTypeService?.docs
      : settingUserTypeService?.docs ?? [],
    page: searchActive
      ? searchSettingUserTypeService?.page
      : settingUserTypeService?.page ?? 0,
    show,
    handleClose,
    handleShow,
    handleRemoveTypeService,
    updateTypeService,
    setUpdateTypeService,
    handlePaginationLeft,
    handlePaginationRight,
  };
};
