import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { addNewUserApi } from "../../actions/registered-users";

export const useAddNewUser = (close) => {
  const dispatch = useDispatch();
  const { loafdingRegister } = useSelector((state) => state.users);
  const {
    loadingeSettingUser,
    settingUserRol,
    settingUserDependency,
    settingUserCargo,
    settingUserStatus,
  } = useSelector((state) => state.settings);

  const {
    control,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm({
    defaultValues: {
      nro_cedula: "",
      nombres: "",
      apellidos: "",
      correo_electronico: "",
      nro_celular: "",
      rol: "",
      estado: "",
      dependencia_id: "",
      cargo_id: 0,
    },
  });

  const handleNewUser = (data) => {
    const {
      nro_cedula,
      nombres,
      apellidos,
      correo_electronico,
      nro_celular,
      dependencia_id: { value: vDependency_id },
      rol: { value: vRol },
      estado: { value: VStatus },
      cargo_id: { value: vCargo },
    } = data;

    dispatch(
      addNewUserApi(
        {
          nro_cedula,
          nombres,
          apellidos,
          correo_electronico,
          nro_celular,
          rol: vRol,
          estado: VStatus,
          cargo: vCargo,
          dependencia_id: vDependency_id,
        },
        close
      )
    );
  };

  return {
    handleNewUser,
    handleSubmit,
    control,
    errors,
    loading: loafdingRegister,
    loadingeSettingUser,
    settingUserRol,
    settingUserDependency,
    settingUserCargo,
    settingUserStatus,
  };
};
