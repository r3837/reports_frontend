import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { createCargosApi } from "../../actions/register-settings";

export const useCargoItemSystemNew = (handleClose) => {
  const dispatch = useDispatch();
  const { loadingSettingUserCargo } = useSelector(
    (state) => state.registerSettings
  );

  const {
    control,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm({
    defaultValues: {
      cargo: "",
    },
  });

  const handleCreateCargo = (data) => {
    dispatch(createCargosApi(data, handleClose));
  };

  return {
    handleCreateCargo,
    control,
    handleSubmit,
    errors,
    loading: loadingSettingUserCargo,
  };
};
