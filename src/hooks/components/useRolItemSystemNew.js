import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { createRolApi } from "../../actions/register-settings";

export const useRolItemSystemNew = (handleClose) => {
  const dispatch = useDispatch();
  const { loadingSettingUserRol } = useSelector(
    (state) => state.registerSettings
  );

  const {
    control,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm({
    defaultValues: {
      rol: "",
    },
  });

  const handleCreateRol = (data) => {
    dispatch(createRolApi(data, handleClose));
  };

  return {
    handleCreateRol,
    control,
    handleSubmit,
    errors,
    loading: loadingSettingUserRol,
  };
};
