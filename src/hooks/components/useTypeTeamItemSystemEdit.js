import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { updateTypeTeamApi } from "../../actions/register-settings";

export const useTypeTeamItemSystemEdit = (item, handleClose) => {
  const dispatch = useDispatch();
  const { loadingUpdateSettingTypeTeam } = useSelector(
    (state) => state.registerSettings
  );

  const {
    control,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm({
    defaultValues: {
      tipo_equipo_id: item?.tipo_equipo_id,
      name: item?.name,
    },
  });

  const handleUpdateTypeTeam = (data) => {
    dispatch(updateTypeTeamApi(data, handleClose));
  };

  return {
    handleUpdateTypeTeam,
    control,
    handleSubmit,
    errors,
    loading: loadingUpdateSettingTypeTeam,
  };
};
