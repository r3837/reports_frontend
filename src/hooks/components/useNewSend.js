import { useContext, useEffect } from "react";
import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { createImboxApi } from "../../actions/imbox";
import { settingApi } from "../../actions/settings";
import ContextSocket from "../../context/context-socketio";

export const useNewSend = (handleClose) => {
  const dispatch = useDispatch();
  const Socket = useContext(ContextSocket);

  const { user } = useSelector((state) => state.auth);
  const { settingRequestTypes, settingsResponsables, settingRequestTeams } =
    useSelector((state) => state.settings);
  const { loadingCreateImbox } = useSelector((state) => state.imbox);

  const {
    control,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm({
    defaultValues: {
      asunto: "",
      usuario_solicitante: user?.usuario_id,
    },
  });



  useEffect(() => {
    if (
      settingRequestTypes?.length <= 0 ||
      settingsResponsables?.length <= 0 ||
      settingRequestTeams?.length <= 0
    ) {
      console.log('entro');
      dispatch(settingApi());
    }
  }, [
    dispatch,
    settingRequestTypes,
    settingsResponsables,
    settingRequestTeams,
  ]);

  const handleSendRequest = (data) => {
    const {
      prioridad: { value: valuePrioridad },
      tipo_servicio: { value: valueTipoServicio },
      responsable: { value: valueResponsable },
      equipo_id: { value: valueEquipoId },
      asunto,
      usuario_solicitante,
    } = data;

    const params = {
      asunto,
      prioridad: valuePrioridad,
      tipo_servicio: valueTipoServicio,
      usuario_solicitante,
      usuario_responsable: valueResponsable,
      equipo_id: valueEquipoId,
    };

    // dispatch(createImboxApi(params));
    Socket?.emit("on_messages", params);
    reset();
    handleClose();
  };

  return {
    handleSendRequest,
    settingRequestTypes,
    settingsResponsables,
    settingRequestTeams,
    errors,
    handleSubmit,
    control,
    loading: loadingCreateImbox,
  };
};
