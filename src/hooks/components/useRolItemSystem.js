import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  getRolesApi,
  removeRoleApi,
  searchSettingRolApi,
} from "../../actions/register-settings";
import { toastAlert, toastAlertConfirm } from "../../helpers/alertToast";

export const useRolItemSystem = () => {
  const dispatch = useDispatch();
  const {
    settingUserRol,
    loadingSettingUserRol,
    paginationRolPrev,
    searchSettingUserRol,
    textSearchSetting,
    activeSearchSetting: searchActive,
  } = useSelector((state) => state.registerSettings);

  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const [updateRol, setUpdateRol] = useState(null);

  useEffect(() => {
    if (settingUserRol?.docs.length <= 0) {
      dispatch(getRolesApi({ page: 1, limit: 10 }));
    }
  }, []);

  useEffect(() => {
    if (paginationRolPrev) {
      handlePaginationLeft();
    }
  }, [paginationRolPrev]);

  const handlePaginationLeft = () => {
    if (searchActive) {
      if (searchSettingUserRol?.page > 1) {
        dispatch(
          searchSettingRolApi({
            query: textSearchSetting,
            page: searchSettingUserRol?.page - 1,
            limit: process.env.REACT_APP_LIMIT_PAGINATION,
          })
        );
      } else {
        toastAlert("success", "Eso es todo!");
      }
    } else {
      if (settingUserRol?.page > 1) {
        dispatch(getRolesApi({ page: settingUserRol?.page - 1, limit: 10 }));
      } else {
        toastAlert("success", "Eso es todo!");
      }
    }
  };

  const handlePaginationRight = () => {
    if (searchActive) {
      if (searchSettingUserRol?.page < searchSettingUserRol?.totalPages) {
        dispatch(
          searchSettingRolApi({
            query: textSearchSetting,
            page: searchSettingUserRol?.page - 1,
            limit: process.env.REACT_APP_LIMIT_PAGINATION,
          })
        );
      } else {
        toastAlert("success", "Eso es todo!");
      }
    } else {
      if (settingUserRol?.page < settingUserRol?.totalPages) {
        dispatch(getRolesApi({ page: settingUserRol?.page + 1, limit: 10 }));
      } else {
        toastAlert("success", "Eso es todo!");
      }
    }
  };

  const handleRemoveRol = (rol) => {
    toastAlertConfirm(
      "warning",
      "Eliminar",
      "Estas seguro en eliminar el Rol...",
      "Eliminar",
      () => {
        dispatch(removeRoleApi({ rol }));
      }
    );
  };

  return {
    loadingSettingUserRol,
    docs: searchActive
      ? searchSettingUserRol?.docs
      : settingUserRol?.docs ?? [],
    page: searchActive ? searchSettingUserRol?.page : settingUserRol?.page ?? 0,
    show,
    handleClose,
    handleShow,
    handleRemoveRol,
    updateRol,
    setUpdateRol,
    handlePaginationLeft,
    handlePaginationRight,
  };
};
