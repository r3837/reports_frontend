import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { createTypeServiceApi } from "../../actions/register-settings";

export const useTypeServiceItemSystemNew = (handleClose) => {
  const dispatch = useDispatch();
  const { loadingSettingUserTypeService } = useSelector(
    (state) => state.registerSettings
  );

  const {
    control,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm({
    defaultValues: {
      tipo_servicio: "",
      observacion: "",
    },
  });

  const handleCreateTypeService = (data) => {
    dispatch(createTypeServiceApi(data, handleClose));
  };

  return {
    handleCreateTypeService,
    control,
    handleSubmit,
    errors,
    loading: loadingSettingUserTypeService,
  };
};
