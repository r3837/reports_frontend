import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  getCargosApi,
  removeCargoApi,
  searchSettingCargosApi,
} from "../../actions/register-settings";
import { toastAlert, toastAlertConfirm } from "../../helpers/alertToast";

export const useCargoItemSystem = () => {
  const dispatch = useDispatch();
  const {
    settingUserCargo,
    loadingSettingUserCargo,
    paginationCargoPrev,
    textSearchSetting,
    searchSettingUserCargo,
    activeSearchSetting: searchActive,
  } = useSelector((state) => state.registerSettings);

  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const [updateCargo, setUpdateCargo] = useState(null);

  useEffect(() => {
    if (settingUserCargo?.docs.length <= 0) {
      dispatch(getCargosApi({ page: 1, limit: 10 }));
    }
  }, []);

  useEffect(() => {
    if (paginationCargoPrev) {
      handlePaginationLeft();
    }
  }, [paginationCargoPrev]);

  const handlePaginationLeft = () => {
    if (searchActive) {
      if (searchSettingUserCargo?.page > 1) {
        dispatch(
          searchSettingCargosApi({
            query: textSearchSetting,
            page: searchSettingUserCargo?.page - 1,
            limit: process.env.REACT_APP_LIMIT_PAGINATION,
          })
        );
      } else {
        toastAlert("success", "Eso es todo!");
      }
    } else {
      if (settingUserCargo?.page > 1) {
        dispatch(getCargosApi({ page: settingUserCargo?.page - 1, limit: 10 }));
      } else {
        toastAlert("success", "Eso es todo!");
      }
    }
  };

  const handlePaginationRight = () => {
    if (searchActive) {
      if (searchSettingUserCargo?.page < searchSettingUserCargo?.totalPages) {
        dispatch(
          searchSettingCargosApi({
            query: textSearchSetting,
            page: searchSettingUserCargo?.page - 1,
            limit: process.env.REACT_APP_LIMIT_PAGINATION,
          })
        );
      } else {
        toastAlert("success", "Eso es todo!");
      }
    } else {
      if (settingUserCargo?.page < settingUserCargo?.totalPages) {
        dispatch(getCargosApi({ page: settingUserCargo?.page + 1, limit: 10 }));
      } else {
        toastAlert("success", "Eso es todo!");
      }
    }
  };

  const handleRemoveCargo = (cargo) => {
    toastAlertConfirm(
      "warning",
      "Eliminar",
      "Estas seguro en eliminar el cargo...",
      "Eliminar",
      () => {
        dispatch(removeCargoApi({ cargo }));
      }
    );
  };

  return {
    loadingSettingUserCargo,
    docs: searchActive
      ? searchSettingUserCargo?.docs
      : settingUserCargo?.docs ?? [],
    page: searchActive
      ? searchSettingUserCargo?.page
      : settingUserCargo?.page ?? 0,
    show,
    handleClose,
    handleShow,
    handleRemoveCargo,
    updateCargo,
    setUpdateCargo,
    handlePaginationLeft,
    handlePaginationRight,
  };
};
