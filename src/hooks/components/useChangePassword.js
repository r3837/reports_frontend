import { useEffect } from "react";
import { useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import { changePasswordApi, resetNavegateLogin } from "../../actions/auth";
import { toastAlert } from "../../helpers/alertToast";

export const useChangePassword = () => {
  const { token } = useParams();
  const form = useRef();
  const checkBtn = useRef();
  const dispatch = useDispatch();
  const navegate = useNavigate();

  const [confirmPsssword, setConfirmPsssword] = useState("");
  const [password, setPassword] = useState("");
  const [loading, setLoading] = useState(false);

  const { navegateLogin } = useSelector((state) => state.auth);

  useEffect(() => {
    if (navegateLogin) {
      navegate("/iniciar-sesion");
      dispatch(resetNavegateLogin(false));
    }
  }, [navegateLogin]);

  const onChangePassword = (e) => {
    const password = e.target.value;
    setPassword(password);
  };

  const onChangeConfirmPassword = (e) => {
    const password = e.target.value;
    setConfirmPsssword(password);
  };

  const handleChangePassword = (e) => {
    e.preventDefault();
    setLoading(true);
    form.current.validateAll();
    if (
      checkBtn.current.context._errors.length === 0 &&
      password === confirmPsssword
    ) {
      dispatch(changePasswordApi({ contrasena: password }, token));
    } else {
      setLoading(false);
      toastAlert("error", "Las contraseñas no son iguales");
    }
  };

  return {
    form,
    onChangeConfirmPassword,
    handleChangePassword,
    onChangePassword,
    loading,
    checkBtn,
    password,
    confirmPsssword,
  };
};
