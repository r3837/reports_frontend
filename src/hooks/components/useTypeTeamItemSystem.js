import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  getTypeTeamsApi,
  removeTypeTeamApi,
  searchSettingTypeTeamApi,
} from "../../actions/register-settings";
import { toastAlert, toastAlertConfirm } from "../../helpers/alertToast";

export const useTypeTeamItemSystem = () => {
  const dispatch = useDispatch();

  const {
    settingUserTypeTeam,
    loadingSettingUserTypeTeam,
    paginationTypeTeamPrev,
    searchSettingUserTypeTeam,
    textSearchSetting,
    activeSearchSetting: searchActive,
  } = useSelector((state) => state.registerSettings);

  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const [updateTypeTeam, setUpdateTypeTeam] = useState(null);

  useEffect(() => {
    if (settingUserTypeTeam?.docs.length <= 0) {
      dispatch(getTypeTeamsApi({ page: 1, limit: 10 }));
    }
  }, []);

  useEffect(() => {
    if (paginationTypeTeamPrev) {
      handlePaginationLeft();
    }
  }, [paginationTypeTeamPrev]);

  const handlePaginationLeft = () => {
    if (searchActive) {
      if (searchSettingUserTypeTeam?.page > 1) {
        dispatch(
          searchSettingTypeTeamApi({
            query: textSearchSetting,
            page: searchSettingUserTypeTeam?.page - 1,
            limit: process.env.REACT_APP_LIMIT_PAGINATION,
          })
        );
      } else {
        toastAlert("success", "Eso es todo!");
      }
    } else {
      if (settingUserTypeTeam?.page > 1) {
        dispatch(
          getTypeTeamsApi({ page: settingUserTypeTeam?.page - 1, limit: 10 })
        );
      } else {
        toastAlert("success", "Eso es todo!");
      }
    }
  };

  const handlePaginationRight = () => {
    if (searchActive) {
      if (
        searchSettingUserTypeTeam?.page < searchSettingUserTypeTeam?.totalPages
      ) {
        dispatch(
          searchSettingTypeTeamApi({
            query: textSearchSetting,
            page: searchSettingUserTypeTeam?.page - 1,
            limit: process.env.REACT_APP_LIMIT_PAGINATION,
          })
        );
      } else {
        toastAlert("success", "Eso es todo!");
      }
    } else {
      if (settingUserTypeTeam?.page < settingUserTypeTeam?.totalPages) {
        dispatch(
          getTypeTeamsApi({ page: settingUserTypeTeam?.page + 1, limit: 10 })
        );
      } else {
        toastAlert("success", "Eso es todo!");
      }
    }
  };

  const handleRemoveTypeTeam = ({ id }) => {
    toastAlertConfirm(
      "warning",
      "Eliminar",
      "Estas seguro en eliminar el tipo equipo...",
      "Eliminar",
      () => {
        dispatch(removeTypeTeamApi({ tipo_equipo_id: id }));
      }
    );
  };

  return {
    loadingSettingUserTypeTeam,
    docs: searchActive
      ? searchSettingUserTypeTeam?.docs
      : settingUserTypeTeam?.docs ?? [],
    page: searchActive
      ? searchSettingUserTypeTeam?.page
      : settingUserTypeTeam?.page ?? 0,
    show,
    handleClose,
    handleShow,
    handleRemoveTypeTeam,
    updateTypeTeam,
    setUpdateTypeTeam,
    handlePaginationLeft,
    handlePaginationRight,
  };
};
