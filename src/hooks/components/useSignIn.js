import { useState, useRef, useEffect } from "react";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { loginApi } from "../../actions/auth";

export const useSignIn = () => {
  const form = useRef();
  const checkBtn = useRef();
  const dispatch = useDispatch();

  const { logged } = useSelector((state) => state.auth);

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    if (logged) {
      setLoading(false);
    }
  }, [logged]);

  const onChangeUsername = (e) => {
    const email = e.target.value;
    setEmail(email);
  };

  const onChangePassword = (e) => {
    const password = e.target.value;
    setPassword(password);
  };

  const handleLogin = (e) => {
    e.preventDefault();
    setLoading(true);
    form.current.validateAll();
    if (checkBtn.current.context._errors.length === 0) {
      dispatch(loginApi({ correo_electronico: email, contrasena: password }));
    } else {
      setLoading(false);
    }
  };

  return {
    form,
    onChangeUsername,
    handleLogin,
    onChangePassword,
    loading,
    checkBtn,
    email,
    password,
    logged,
  };
};
