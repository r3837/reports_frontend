import { useState } from "react";

export const useAuthOptions = () => {
  const [selectedMenu, setSelectedMenu] = useState(0);

  const handleMenu = (item) => {
    setSelectedMenu(item);
  };

  return {
    handleMenu,
    selectedMenu,
    setSelectedMenu
  };
};
