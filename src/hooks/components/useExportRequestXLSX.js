import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { requestImboxAllApi } from "../../actions/imbox";

const docs = [
  {
    autor: "Pepito Perez",
    solicitud: "Mantenimiento",
    estado: "Pendiente",
    fecha: "26 Apr 2022 03:14:53",
  },
  {
    autor: "Andres Hurtado",
    solicitud: "Internet",
    estado: "Pendiente",
    fecha: "26 Apr 2022 03:14:53",
  },
  {
    autor: "Rocio Chito",
    solicitud: "Soporte",
    estado: "Pendiente",
    fecha: "26 Apr 2022 03:14:53",
  },
];

export const useExportRequestXLSX = () => {
  const dispatch = useDispatch();
  const { requestImboxAll, loadingRequestImboxAll } = useSelector((state) => state.imbox);

  console.log(requestImboxAll);

  useEffect(() => {
    if (requestImboxAll?.length <= 0) {
      dispatch(requestImboxAllApi());
    }
  }, [requestImboxAll]);

  return {
    docs: requestImboxAll ?? [],
    loading: loadingRequestImboxAll
  };
};
