import { useEffect, useLayoutEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import {
  activeSearchUsersApi,
  getRegisteredUsersApi,
  removeUserByIdApi,
  searchUsersApi,
} from "../../actions/registered-users";
import { settingForUserApi } from "../../actions/settings";
import { toastAlert, toastAlertConfirm } from "../../helpers/alertToast";

export const useRegisterPage = () => {
  const dispatch = useDispatch();
  const navegate = useNavigate();

  const { user } = useSelector((state) => state.auth);
  const {
    loadingUser,
    registeredUsers,
    paginationPrev,
    searchUsers,
    activeSearchUsers: searchActive,
  } = useSelector((state) => state.users);
  const { settingUserRol, settingUserDependency, settingUserCargo } =
    useSelector((state) => state.settings);

  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const [showUpdate, setShowUpdate] = useState(false);
  const handleCloseUpdate = () => setShowUpdate(false);
  const handleShowUpdate = () => setShowUpdate(true);
  const [selectItem, setSelectItem] = useState(null);

  const [activeSearch, setActiveSearch] = useState(false);
  const [textSearch, setTextSearch] = useState("");

  useLayoutEffect(() => {
    if (
      registeredUsers?.docs.length <= 0 &&
      registeredUsers.page <= registeredUsers.totalPages
    ) {
      dispatch(
        getRegisteredUsersApi({
          page: 1,
          limit: process.env.REACT_APP_LIMIT_PAGINATION,
        })
      );
    }
  }, [dispatch]);

  useEffect(() => {
    if (!activeSearch) {
      dispatch(activeSearchUsersApi(false));
    }
  }, [activeSearch]);

  useEffect(() => {
    if (paginationPrev) {
      handlePaginationLeft();
    }
  }, [paginationPrev]);

  const handlePaginationLeft = () => {
    if (searchActive) {
      if (searchUsers?.page > 1) {
        dispatch(
          searchUsersApi({
            query: textSearch,
            page: searchUsers?.page - 1,
            limit: process.env.REACT_APP_LIMIT_PAGINATION,
          })
        );
      } else {
        toastAlert("success", "Eso es todo!");
      }
    } else {
      if (registeredUsers?.page > 1) {
        dispatch(
          getRegisteredUsersApi({ page: registeredUsers?.page - 1, limit: 2 })
        );
      } else {
        toastAlert("success", "Eso es todo!");
      }
    }
  };

  const handlePaginationRight = () => {
    if (activeSearch) {
      if (searchUsers?.page < searchUsers?.totalPages) {
        dispatch(
          searchUsersApi({
            query: textSearch,
            page: searchUsers?.page + 1,
            limit: process.env.REACT_APP_LIMIT_PAGINATION,
          })
        );
      } else {
        toastAlert("success", "Eso es todo!");
      }
    } else {
      if (registeredUsers?.page < registeredUsers?.totalPages) {
        dispatch(
          getRegisteredUsersApi({
            page: registeredUsers?.page + 1,
            limit: process.env.REACT_APP_LIMIT_PAGINATION,
          })
        );
      } else {
        toastAlert("success", "Eso es todo!");
      }
    }
  };

  useEffect(() => {
    if (
      (show || showUpdate) &&
      settingUserRol.length <= 0 &&
      settingUserDependency.length <= 0 &&
      settingUserCargo.length <= 0
    ) {
      dispatch(settingForUserApi());
    }
  }, [show, showUpdate]);

  const handleRemoveMesageRequest = (id) => {
    toastAlertConfirm(
      "warning",
      "Eliminar",
      "Estas seguro en eliminar el mensaje...",
      "Eliminar",
      () => {
        dispatch(removeUserByIdApi({ nro_cedula: id }));
      }
    );
  };

  const handleActiveSearch = () => {
    setActiveSearch((active) => (active ? false : true));
  };

  const handleSearchState = (e) => {
    e.preventDefault();
    const {
      target: { value },
    } = e;
    setTextSearch(value);

    if (value.length <= 0) {
      dispatch(activeSearchUsersApi(false));
    }
  };

  const handleSearch = (event) => {
    if (event.key === "Enter") {
      dispatch(
        searchUsersApi({
          query: textSearch,
          page: 1,
          limit: process.env.REACT_APP_LIMIT_PAGINATION,
        })
      );
    }
  };

  console.log(searchActive);

  return {
    loading: loadingUser,
    docs: searchActive ? searchUsers?.docs : registeredUsers?.docs ?? [],
    page: searchActive ? searchUsers?.page : registeredUsers?.page ?? 0,
    handlePaginationLeft,
    handlePaginationRight,
    handleRemoveMesageRequest,
    isActiveUser: user.usuario_id,
    show,
    handleClose,
    handleShow,
    showUpdate,
    handleCloseUpdate,
    handleShowUpdate,
    selectItem,
    setSelectItem,

    handleActiveSearch,
    activeSearch,
    handleSearch,
    handleSearchState,
  };
};
