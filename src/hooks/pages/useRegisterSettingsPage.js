import { useEffect } from "react";
import { useState } from "react";
import { useDispatch } from "react-redux";
import { useLocation } from "react-router-dom";
import {
  activeSearchSettingCargoApi,
  activeSearchSettingDependencyApi,
  activeSearchSettingRolApi,
  activeSearchSettingTeamApi,
  activeSearchSettingTypeTeamApi,
  searchSettingCargosApi,
  searchSettingDependencyApi,
  searchSettingRolApi,
  searchSettingTeamApi,
  searchSettingTypeTeamApi,
  textSearchSettingApi,
} from "../../actions/register-settings";

export const useRegisterSettingsPage = () => {
  const dispatch = useDispatch();
  const location = useLocation();

  const [activeSearch, setActiveSearch] = useState(false);
  const [textSearch, setTextSearch] = useState("");
  const [curPathActive, setCurPathActive] = useState("");

  useEffect(() => {
    const curPath = window.location.pathname.split("/")[2];
    setCurPathActive(curPath);
  }, [location]);

  useEffect(() => {
    if (!activeSearch) {
      if (curPathActive == undefined) {
        dispatch(activeSearchSettingRolApi(false));
      } else if (curPathActive == "dependencia-del-sistema") {
        dispatch(activeSearchSettingDependencyApi(false));
      } else if (curPathActive == "cargo-del-sistema") {
        dispatch(activeSearchSettingCargoApi(false));
      } else if (curPathActive == "equipo-del-sistema") {
        dispatch(activeSearchSettingTeamApi(false));
      } else if (curPathActive == "tipo-equipo-del-sistema") {
        dispatch(activeSearchSettingTypeTeamApi(false));
      }
    }
  }, [activeSearch]);

  const handleActiveSearch = () => {
    setActiveSearch((active) => (active ? false : true));
  };

  const handleSearchState = (e) => {
    e.preventDefault();
    const {
      target: { value },
    } = e;
    setTextSearch(value);

    if (value.length <= 0) {
      dispatch(activeSearchSettingRolApi(false));
    }
  };

  const handleSearch = (event) => {
    if (event.key === "Enter") {
      dispatch(textSearchSettingApi(textSearch))
      if (curPathActive == undefined) {
        dispatch(
          searchSettingRolApi({
            query: textSearch,
            page: 1,
            limit: process.env.REACT_APP_LIMIT_PAGINATION,
          })
        );
      } else if (curPathActive == "dependencia-del-sistema") {
        dispatch(
          searchSettingDependencyApi({
            query: textSearch,
            page: 1,
            limit: process.env.REACT_APP_LIMIT_PAGINATION,
          })
        );
      } else if (curPathActive == "cargo-del-sistema") {
        dispatch(
          searchSettingCargosApi({
            query: textSearch,
            page: 1,
            limit: process.env.REACT_APP_LIMIT_PAGINATION,
          })
        );
      } else if (curPathActive == "equipo-del-sistema") {
        dispatch(
          searchSettingTeamApi({
            query: textSearch,
            page: 1,
            limit: process.env.REACT_APP_LIMIT_PAGINATION,
          })
        );
      } else if (curPathActive == "tipo-equipo-del-sistema") {
        dispatch(
          searchSettingTypeTeamApi({
            query: textSearch,
            page: 1,
            limit: process.env.REACT_APP_LIMIT_PAGINATION,
          })
        );
      }
    }
  };

  return {
    curPathActive,
    handleSearchState,
    handleSearch,
    handleActiveSearch,
    activeSearch,
  };
};
