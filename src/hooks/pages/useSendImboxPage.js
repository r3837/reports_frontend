import { useEffect } from "react";
import { useLayoutEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import {
  activeSearchSendApi,
  renderMessageId,
  requestImboxRemoveAllApi,
  searchSendImboxApi,
  sendImboxApi,
} from "../../actions/imbox";
import { toastAlert, toastAlertConfirm } from "../../helpers/alertToast";

export const useSendImboxPage = () => {
  const {
    sendImbox,
    loadingSendImbox,
    searchSendImbox,
    activeSearch: searchActive,
  } = useSelector((state) => state.imbox);
  const dispatch = useDispatch();
  const navegate = useNavigate();

  const [activeSearch, setActiveSearch] = useState(false);
  const [textSearch, setTextSearch] = useState("");

  useLayoutEffect(() => {
    if (sendImbox?.docs.length <= 0 && sendImbox.page <= sendImbox.totalPages) {
      dispatch(
        sendImboxApi({ page: 1, limit: process.env.REACT_APP_LIMIT_PAGINATION })
      );
    }
  }, [dispatch, sendImbox]);

  useEffect(() => {
    if (!activeSearch) {
      dispatch(activeSearchSendApi(false));
    }
  }, [activeSearch]);

  const handlePaginationLeft = () => {
    if (searchActive) {
      if (searchSendImbox?.page > 1) {
        dispatch(
          searchSendImboxApi({
            query: textSearch,
            page: searchSendImbox?.page - 1,
            limit: process.env.REACT_APP_LIMIT_PAGINATION,
          })
        );
      } else {
        toastAlert("success", "Eso es todo!");
      }
    } else {
      if (sendImbox?.page > 1) {
        dispatch(
          sendImboxApi({
            page: sendImbox?.page - 1,
            limit: process.env.REACT_APP_LIMIT_PAGINATION,
          })
        );
      } else {
        toastAlert("success", "Eso es todo!");
      }
    }
  };

  const handlePaginationRight = () => {
    if (searchActive) {
      if (searchSendImbox?.page < searchSendImbox?.totalPages) {
        dispatch(
          searchSendImboxApi({
            query: textSearch,
            page: searchSendImbox?.page + 1,
            limit: process.env.REACT_APP_LIMIT_PAGINATION,
          })
        );
      } else {
        toastAlert("success", "Eso es todo!");
      }
    } else {
      if (sendImbox?.page < sendImbox?.totalPages) {
        dispatch(
          sendImboxApi({
            page: sendImbox?.page + 1,
            limit: process.env.REACT_APP_LIMIT_PAGINATION,
          })
        );
      } else {
        toastAlert("success", "Eso es todo!");
      }
    }
  };

  const handleNavegate = (item) => {
    navegate(`/mensajes/${item.solicitud_id}`);
    dispatch(renderMessageId(item.solicitud_id));
  };

  const handleRemoveMesageRequest = (id) => {
    toastAlertConfirm(
      "warning",
      "Eliminar",
      "Estas seguro en eliminar el mensaje...",
      "Eliminar",
      () => {
        if (id) {
          dispatch(requestImboxRemoveAllApi({ solicitud_id: id }));
        }
      }
    );
  };

  const handleActiveSearch = () => {
    setActiveSearch((active) => (active ? false : true));
  };

  const handleSearchState = (e) => {
    e.preventDefault();
    const {
      target: { value },
    } = e;
    setTextSearch(value);

    if (value.length <= 0) {
      dispatch(activeSearchSendApi(false));
    }
  };

  const handleSearch = (event) => {
    if (event.key === "Enter") {
      dispatch(
        searchSendImboxApi({
          query: textSearch,
          page: 1,
          limit: process.env.REACT_APP_LIMIT_PAGINATION,
        })
      );
    }
  };

  return {
    loading: loadingSendImbox,
    docs: searchActive ? searchSendImbox?.docs : sendImbox?.docs ?? [],
    page: searchActive ? searchSendImbox?.page : sendImbox?.page ?? 0,
    handlePaginationLeft,
    handlePaginationRight,
    handleNavegate,
    handleRemoveMesageRequest,
    handleSearchState,
    handleSearch,
    handleActiveSearch,
    activeSearch,
  };
};
