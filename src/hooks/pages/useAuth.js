import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { isLoggedInApi } from "../../actions/auth";

export const useAuth = () => {
  const dispatch = useDispatch();
  const { logged, loading } = useSelector((state) => state.auth);
  const [token] = useState(window.localStorage.getItem("token"));

  useEffect(() => {
    if (token) {
      dispatch(isLoggedInApi(token));
    }
  }, [dispatch, token]);

  return {
    isLogged: logged,
    loading,
  };
};
