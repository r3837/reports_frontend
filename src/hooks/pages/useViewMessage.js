import { useContext, useState } from "react";
import { useEffect, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import {
  changeStatusRequestApi,
  viewMessageApi,
  viewMessage as resertViewMessage,
} from "../../actions/imbox";
import { settingRequestStatusApi } from "../../actions/settings";
import { typeUserRol } from "../../types/typeUserRol";
import ContextSocket from "../../context/context-socketio";

export const useViewMessage = () => {
  const { id } = useParams();
  const dispatch = useDispatch();
  const Socket = useContext(ContextSocket);

  const { loadingViewMessage, viewMessage } = useSelector(
    (state) => state.imbox
  );
  const { user } = useSelector((state) => state.auth);
  const { settingRequestStatus } = useSelector((state) => state.settings);
  const { renderMessageId } = useSelector((state) => state.imbox);

  useEffect(() => {
    dispatch(settingRequestStatusApi());
  }, []);

  useEffect(() => {
    if (id !== renderMessageId) {
      dispatch(viewMessageApi({ id }));
    }
  }, [id]);

  const onChangeStatus = ({ value }) => {
    const { usuario_responsable, nombres, apellidos } = user;
    const { usuario_solicitante, tipo_servicio, asunto } = viewMessage;

    Socket.emit("on_message_status", {
      solicitud_id: Number(id),
      estado_solicitud: value,
      usuario_id_receiver: usuario_solicitante,
      usuario_id: 2,
      asunto: `${tipo_servicio} ${
        asunto.length > 10 ? asunto?.substr(0, 10) : asunto
      }...`,
      responsable: `${nombres} ${apellidos}`,
    });
  };

  return {
    loadingViewMessage,
    doc: viewMessage,
    settingRequestStatus,
    onChangeStatus,
    visibilityStatus: user?.rol === typeUserRol.responsable.description,
  };
};
