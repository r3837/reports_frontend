import { useSelector } from "react-redux";

export const useProfilePage = () => {
    
  const { user } = useSelector((state) => state.auth);

  return {
    user,
  };
};
