import { useEffect, useState } from "react";
import { useLayoutEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import {
  activeSearchApi,
  renderMessageId,
  requesImboxApi,
  requestImboxRemoveApi,
  requestObservationImboxApi,
  searchImboxApi,
  searchSolicitanteImboxApi,
} from "../../actions/imbox";
import { toastAlert, toastAlertConfirm } from "../../helpers/alertToast";
import { typeUserRol } from "../../types/typeUserRol";

export const useRequestImboxPage = () => {
  const {
    requestImbox,
    loadingRequestImbox,
    searchImbox,
    activeSearch: searchActive,
  } = useSelector((state) => state.imbox);
  const { user } = useSelector((state) => state.auth);
  const dispatch = useDispatch();
  const navegate = useNavigate();

  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const [activeSearch, setActiveSearch] = useState(false);
  const [textSearch, setTextSearch] = useState("");

  useLayoutEffect(() => {
    if (
      requestImbox?.docs.length <= 0 &&
      requestImbox.page <= requestImbox.totalPages
    ) {
      user?.rol === typeUserRol.responsable.description
        ? dispatch(
            requesImboxApi({
              page: 1,
              limit: process.env.REACT_APP_LIMIT_PAGINATION,
            })
          )
        : dispatch(
            requestObservationImboxApi({
              page: 1,
              limit: process.env.REACT_APP_LIMIT_PAGINATION,
            })
          );
    }
  }, [dispatch]);

  useEffect(() => {
    if (!activeSearch) {
      dispatch(activeSearchApi(false));
    }
  }, [activeSearch]);

  const handleNavegate = (item) => {
    navegate(`/mensajes/${item.solicitud_id}`);
    dispatch(renderMessageId(item.solicitud_id));
  };

  const handlePaginationLeft = () => {
    if (searchActive) {
      if (searchImbox?.page > 1) {
        dispatch(
          searchImboxApi({
            query: textSearch,
            page: searchImbox?.page - 1,
            limit: process.env.REACT_APP_LIMIT_PAGINATION,
          })
        );
      } else {
        toastAlert("success", "Eso es todo!");
      }
    } else if (user?.rol === typeUserRol.responsable.description) {
      if (requestImbox?.page > 1) {
        dispatch(
          requesImboxApi({
            page: requestImbox?.page - 1,
            limit: process.env.REACT_APP_LIMIT_PAGINATION,
          })
        );
      } else {
        toastAlert("success", "Eso es todo!");
      }
    } else {
      if (requestImbox?.page > 1) {
        dispatch(
          requestObservationImboxApi({
            page: requestImbox?.page - 1,
            limit: 6,
          })
        );
      } else {
        toastAlert("success", "Eso es todo!");
      }
    }
  };

  const handlePaginationRight = () => {
    if (searchActive) {
      if (searchImbox?.page < searchImbox?.totalPages) {
        dispatch(
          searchImboxApi({
            query: textSearch,
            page: searchImbox?.page + 1,
            limit: process.env.REACT_APP_LIMIT_PAGINATION,
          })
        );
      } else {
        toastAlert("success", "Eso es todo!");
      }
    } else if (user?.rol === typeUserRol.responsable.description) {
      if (requestImbox?.page < requestImbox?.totalPages) {
        dispatch(
          requesImboxApi({
            page: requestImbox?.page + 1,
            limit: process.env.REACT_APP_LIMIT_PAGINATION,
          })
        );
      } else {
        toastAlert("success", "Eso es todo!");
      }
    } else {
      if (requestImbox?.page < requestImbox?.totalPages) {
        dispatch(
          requestObservationImboxApi({
            page: requestImbox?.page + 1,
            limit: process.env.REACT_APP_LIMIT_PAGINATION,
          })
        );
      } else {
        toastAlert("success", "Eso es todo!");
      }
    }
  };

  const handleRemoveMesageRequest = (id) => {
    toastAlertConfirm(
      "warning",
      "Eliminar",
      "Estas seguro en eliminar el mensaje...",
      "Eliminar",
      () => {
        if (id) {
          dispatch(requestImboxRemoveApi({ solicitud_id: id }));
        }
      }
    );
  };

  const handleActiveSearch = () => {
    setActiveSearch((active) => (active ? false : true));
  };

  const handleSearchState = (e) => {
    e.preventDefault();
    const {
      target: { value },
    } = e;
    setTextSearch(value);

    if (value.length <= 0) {
      dispatch(activeSearchApi(false));
    }
  };

  const handleSearch = (event) => {
    if (event.key === "Enter") {
      if (user.rol == typeUserRol.solicitante.description) {
        dispatch(
          searchSolicitanteImboxApi({
            query: textSearch,
            page: 1,
            limit: process.env.REACT_APP_LIMIT_PAGINATION,
          })
        );
      } else {
        dispatch(
          searchImboxApi({
            query: textSearch,
            page: 1,
            limit: process.env.REACT_APP_LIMIT_PAGINATION,
          })
        );
      }
    }
  };

  return {
    loading: loadingRequestImbox,
    handleNavegate,
    docs: searchActive ? searchImbox?.docs : requestImbox?.docs ?? [],
    page: searchActive ? searchImbox?.page : requestImbox?.page ?? 0,
    handlePaginationLeft,
    handlePaginationRight,
    handleRemoveMesageRequest,
    show,
    handleClose,
    handleShow,
    handleActiveSearch,
    activeSearch,
    handleSearch,
    handleSearchState,
    user,
  };
};
