import { createContext, useEffect, useState } from "react";
import { io } from "socket.io-client";
const Context = createContext(null);

export function ContextSocketProvider({ children }) {
  const [Socket, setSocket] = useState(null);
  useEffect(() => {
    const TOKEN = localStorage.getItem("token");
    const SOCKET_URI = process.env.REACT_APP_SOCKET;
    const socket = io(SOCKET_URI, {
      transports: ["websocket"],
      autoConnect: true,
      forceNew: true,
      withCredentials: true,
      auth: (cb) => {
        cb({ token: TOKEN })
      }
    });
    setSocket(socket);
  }, []);

  return <Context.Provider value={Socket}>{children}</Context.Provider>;
}

export default Context;
