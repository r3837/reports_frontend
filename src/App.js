import React, { useEffect, useContext, useState } from "react";
import { Route, Routes, useLocation } from "react-router-dom";

import { RequireAuth } from "./components/requireAuthRoutes/RequireAuth";
import { SpinnerLazyCustom } from "./components/spinner/SpinnerLazyCustom";

import { HomePage } from "./pages/home/index";
import { AdminPage } from "./pages/admin/index";

import { AuthPage } from "./pages/auth/AuthPage";
import { useAuth } from "./hooks/pages/useAuth";
import { ProfilePage } from "./pages/profile/ProifilePage";
import { SendImboxPage } from "./pages/send-imbox";
import { RequestImboxPage } from "./pages/request-imbox";
import { OptionsPage } from "./pages/options/OptionsPage";
import { ViewMessage } from "./pages/message-view";
import { RegisterPage } from "./pages/register";

import { RegisterSettingsPage } from "./pages/register-settings/RegisterSettingsPage";
import { RolItemSystem } from "./components/register-data-system/RolItemSystem";
import { DependencyItemSystem } from "./components/register-data-system/DependencyItemSystem";
import { CargoItemSystem } from "./components/register-data-system/CargoItemSystem";
import { AuthChangePasswordPage } from "./pages/auth/AuthChangePassword";
import ContextSocket from "./context/context-socketio";
import { toastAlert } from "./helpers/alertToast";
import { useDispatch, useSelector } from "react-redux";
import {
  createImboxSocket,
  createObservationSocket,
  imboxUpdateSocket,
  requestImboxSocket,
} from "./actions/socket";
import "./App.css";
import { createObservation } from "./actions/imbox";
import { typeUserRol } from "./types/typeUserRol";
import { TeamItemSystem } from "./components/register-data-system/TeamItemSystem";
import { TypeTeamItemSystem } from "./components/register-data-system/TypeTeamItemSystem";
import { TypeServiceItemSystem } from "./components/register-data-system/TypeServiceItemSystem";

const App = () => {
  let location = useLocation();
  const dispatch = useDispatch();
  const Socket = useContext(ContextSocket);
  let { isLogged, loading } = useAuth();

  useEffect(() => {
    Socket?.on("on_message_status", (payload) => {
      console.log(payload);
      toastAlert(
        "success",
        `
       ${payload?.responsable} ha cambiado el
       estado de tu solicitud ${payload?.asunto}
       a ${payload?.estado_solicitud?.toLowerCase()}.
      `
      );
    });

    Socket?.on("on_message_status_response", (payload) => {
      if (payload.ok) {
        dispatch(imboxUpdateSocket(payload?.doc));
        toastAlert("success", payload?.msg);
      } else {
        toastAlert("error", payload.error);
      }
    });

    Socket?.on("on_messages_sender", (payload) => {
      if (payload.ok) {
        dispatch(createImboxSocket(payload.doc));
        toastAlert("success", payload?.msg);
      } else {
        toastAlert("error", payload?.error.msg);
      }
    });

    Socket?.on("on_messages_receiver", (payload) => {
      if (payload.ok) {
        dispatch(requestImboxSocket(payload.doc));
        toastAlert("success", payload?.msg);
      } else {
        toastAlert("error", payload?.error.msg);
      }
    });

    Socket?.on("on_message_observation_sender", (payload) => {
      if (payload.ok) {
        dispatch(createObservationSocket(payload.doc));
        toastAlert("success", payload?.msg);
      } else {
        toastAlert("error", payload?.error.msg);
      }
    });

    Socket?.on("on_message_observation_receiver", (payload) => {
      if (payload.ok) {
        dispatch(createObservationSocket(payload.doc));
        toastAlert("success", payload?.msg);
      } else {
        toastAlert("error", payload?.error.msg);
      }
    });

    Socket?.on("on_message_observation_imbox", (payload) => {
      if (payload.ok) {
        // dispatch(requestImboxSocket(payload.doc));
      } else {
        toastAlert("error", payload?.error.msg);
      }
    });

    return () => Socket?.disconnect();
  }, [Socket]);

  if (loading) {
    return <SpinnerLazyCustom />;
  }

  return (
    <Routes location={location}>
      <Route path="/iniciar-sesion" element={<AuthPage />} />
      <Route
        path="/cambio-de-contrasena/:token"
        element={<AuthChangePasswordPage />}
      />
      <Route
        path="/"
        element={<RequireAuth isLogged={isLogged} children={<HomePage />} />}
      >
        <Route index element={<ProfilePage />} />
        <Route path="/solicitudes-enviadas" element={<SendImboxPage />} />
        <Route path="/registrar" element={<RegisterPage />} />
        <Route
          path="/registrar-datos-del-sistema"
          element={<RegisterSettingsPage />}
        >
          <Route index element={<RolItemSystem />} />
          <Route
            path="/registrar-datos-del-sistema/dependencia-del-sistema"
            element={<DependencyItemSystem />}
          />
          <Route
            path="/registrar-datos-del-sistema/cargo-del-sistema"
            element={<CargoItemSystem />}
          />
          <Route
            path="/registrar-datos-del-sistema/equipo-del-sistema"
            element={<TeamItemSystem />}
          />

          <Route
            path="/registrar-datos-del-sistema/tipo-equipo-del-sistema"
            element={<TypeTeamItemSystem />}
          />

          <Route
            path="/registrar-datos-del-sistema/tipo-servicio-del-sistema"
            element={<TypeServiceItemSystem />}
          />
        </Route>

        <Route path="/solicitudes-entrantes" element={<RequestImboxPage />} />
        <Route path="/mensajes/:id" element={<ViewMessage />} />
      </Route>
      <Route
        path="/administrador"
        element={<RequireAuth isLogged={isLogged} children={<AdminPage />} />}
      />
      {/* <Route path="/Opciones" element={<OptionsPage />} /> */}
    </Routes>
  );
};

export default App;
