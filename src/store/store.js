import { createStore, combineReducers, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";

import { authReducer } from "../reducers/authReducer";
import { ImboxReducer } from "../reducers/imboxReducer";
import { RegisterSettingsReducer } from "../reducers/registerSettingReducer";
import { SettingsReducer } from "../reducers/settingsReducer";
import { usersReducer } from "../reducers/usersReducer";

const composeEnhancers =
  typeof window === "object" && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
        // Specify extension’s options like name, actionsBlacklist, actionsCreators, serialize...
      })
    : compose;

const reducers = combineReducers({
  auth: authReducer,
  imbox: ImboxReducer,
  settings: SettingsReducer,
  registerSettings: RegisterSettingsReducer,
  users: usersReducer,
});

export const store = createStore(
  reducers,
  composeEnhancers(applyMiddleware(thunk))
);
