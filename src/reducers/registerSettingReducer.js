import { types } from "../types/types";

const initial = {
  activeSearchSetting: false,
  textSearchSetting: "",
  loadingSettingUserRol: false,
  loadingCreateSettingRol: false,
  loadingUpdateSettingRol: false,
  loadingRemoveSettingRol: false,
  paginationRolPrev: false,
  settingUserRol: {
    docs: [],
    totalPages: 0,
    page: 0,
  },
  searchSettingUserRol: {
    docs: [],
    totalPages: 0,
    page: 0,
  },
  loadingSettingUserDependency: false,
  loadingCreateSettingDependency: false,
  loadingUpdateSettingDependency: false,
  loadingRemoveSettingDependency: false,
  paginationDependencyPrev: false,
  settingUserDependency: {
    docs: [],
    totalPages: 0,
    page: 0,
  },
  searchSettingUserDependency: {
    docs: [],
    totalPages: 0,
    page: 0,
  },
  loadingSettingUserCargo: false,
  loadingCreateSettingCargo: false,
  loadingUpdateSettingCargo: false,
  loadingRemoveSettingCargo: false,
  paginationCargoPrev: false,
  settingUserCargo: {
    docs: [],
    totalPages: 0,
    page: 0,
  },
  searchSettingUserCargo: {
    docs: [],
    totalPages: 0,
    page: 0,
  },
  loadingSettingUserTeam: false,
  loadingCreateSettingTeam: false,
  loadingUpdateSettingTeam: false,
  loadingRemoveSettingTeam: false,
  paginationTeamPrev: false,
  settingUserTeam: {
    docs: [],
    totalPages: 0,
    page: 0,
  },
  searchSettingUserTeam: {
    docs: [],
    totalPages: 0,
    page: 0,
  },
  loadingSettingUserTypeTeam: false,
  loadingCreateSettingTypeTeam: false,
  loadingUpdateSettingTypeTeam: false,
  loadingRemoveSettingTypeTeam: false,
  paginationTypeTeamPrev: false,
  settingUserTypeTeam: {
    docs: [],
    totalPages: 0,
    page: 0,
  },
  searchSettingUserTypeTeam: {
    docs: [],
    totalPages: 0,
    page: 0,
  },
  loadingSettingUserTypeService: false,
  loadingCreateSettingTypeService: false,
  loadingUpdateSettingTypeService: false,
  loadingRemoveSettingTypeService: false,
  paginationTypeServicePrev: false,
  settingUserTypeService: {
    docs: [],
    totalPages: 0,
    page: 0,
  },
  searchSettingUserTypeService: {
    docs: [],
    totalPages: 0,
    page: 0,
  },
};

export const RegisterSettingsReducer = (state = initial, action) => {
  switch (action.type) {
    case types.loadingSettingsUserCargos:
      return {
        ...state,
        loadingSettingUserCargo: action.payload ?? true,
      };

    case types.settingsUserCargos:
      return {
        ...state,
        settingUserCargo: {
          docs: action.payload.docs,
          page: action.payload.page,
          totalPages: action.payload.totalPages,
        },
        loadingSettingUserCargo: false,
      };

    case types.settingsUserCargosPGN:
      let ioHash = {};
      return {
        ...state,
        settingUserCargo: {
          page: action.payload.page,
          totalPages: action.payload.totalPages,
          docs: [...state.settingUserCargo.docs, ...action.payload.docs].filter(
            (o) => (ioHash[o.cargo] ? false : (ioHash[o.cargo] = true))
          ),
        },
        loadingSettingUserCargo: false,
      };

    case types.loadingCreateSettingCargo:
      return {
        ...state,
        loadingCreateSettingCargo: action.payload ?? true,
      };

    case types.createSettingCargo:
      return {
        ...state,
        loadingCreateSettingCargo: false,
        settingUserCargo:
          state.settingUserCargo.docs.length > 0
            ? {
                ...state.settingUserCargo,
                docs: [...state.settingUserCargo.docs, action.payload],
              }
            : {
                ...state.settingUserCargo,
                docs: [action.payload],
              },
      };

    case types.loadingUpdateSettingCargo:
      return {
        ...state,
        loadingUpdateSettingCargo: action.payload ?? true,
      };

    case types.updateSettingCargo:
      return {
        ...state,
        settingUserCargo: {
          ...state.settingUserCargo,
          docs: state.settingUserCargo.docs.map((item) =>
            item.cargo_id === action.payload?.cargo_id
              ? (item = { cargo: action.payload?.cargo })
              : item
          ),
        },
        loadingUpdateSettingCargo: false,
      };

    case types.loadingRemoveSettingCargo:
      return {
        ...state,
        loadingRemoveSettingCargo: action.payload ?? true,
      };

    case types.removeSettingCargo:
      return {
        ...state,
        settingUserCargo: {
          ...state.settingUserCargo,
          docs: state.settingUserCargo.docs.filter(
            (item) => item.cargo !== action.payload?.cargo
          ),
        },
        loadingRemoveSettingCargo: false,
        paginationCargoPrev:
          state.settingUserCargo.page > 1 &&
          state.settingUserCargo.docs.length <= 0
            ? true
            : false,
      };

    // rol

    case types.loadingSettingsUserRol:
      return {
        ...state,
        loadingSettingUserRol: action.payload ?? true,
      };

    case types.settingsUserRol:
      return {
        ...state,
        settingUserRol: {
          docs: action.payload.docs,
          page: action.payload.page,
          totalPages: action.payload.totalPages,
        },
        loadingSettingUserRol: false,
      };

    case types.settingsUserRolPGN:
      let ioHashRol = {};
      return {
        ...state,
        settingUserRol: {
          page: action.payload.page,
          totalPages: action.payload.totalPages,
          docs: [...state.settingUserRol.docs, ...action.payload.docs].filter(
            (o) => (ioHashRol[o.rol] ? false : (ioHashRol[o.rol] = true))
          ),
        },
        loadingSettingUserRol: false,
      };

    case types.loadingCreateSettingRol:
      return {
        ...state,
        loadingCreateSettingRol: action.payload ?? true,
      };

    case types.createSettingRol:
      return {
        ...state,
        loadingCreateSettingRol: false,
        settingUserRol:
          state.settingUserRol.docs.length > 0
            ? {
                ...state.settingUserRol,
                docs: [...state.settingUserRol.docs, action.payload],
              }
            : {
                ...state.settingUserRol,
                docs: [action.payload],
              },
      };

    case types.loadingUpdateSettingRol:
      return {
        ...state,
        loadingUpdateSettingRol: action.payload ?? true,
      };

    case types.updateSettingRol:
      return {
        ...state,
        settingUserRol: {
          ...state.settingUserRol,
          docs: state.settingUserRol.docs.map((item) =>
            item.rol === action.payload?.oldRol
              ? (item = { rol: action.payload?.rol })
              : item
          ),
        },
        loadingUpdateSettingRol: false,
      };

    case types.textSearchSetting:
      return {
        ...state,
        textSearchSetting: action.payload,
      };

    case types.loadingRemoveSettingRol:
      return {
        ...state,
        loadingRemoveSettingRol: action.payload ?? true,
      };

    case types.removeSettingRol:
      return {
        ...state,
        settingUserRol: {
          ...state.settingUserRol,
          docs: state.settingUserRol.docs.filter(
            (item) => item.rol !== action.payload?.rol
          ),
        },
        loadingRemoveSettingRol: false,
        paginationRolPrev:
          state.settingUserRol.page > 1 && state.settingUserRol.docs.length <= 0
            ? true
            : false,
      };

    // dependency

    case types.loadingSettingsUserDependency:
      return {
        ...state,
        loadingSettingUserDependency: action.payload ?? true,
      };

    case types.settingsUserDependency:
      return {
        ...state,
        settingUserDependency: {
          docs: action.payload.docs,
          page: action.payload.page,
          totalPages: action.payload.totalPages,
        },
        loadingSettingUserDependency: false,
      };

    case types.settingsUserDependencyPGN:
      let ioHashDependency = {};
      return {
        ...state,
        settingUserDependency: {
          page: action.payload.page,
          totalPages: action.payload.totalPages,
          docs: [
            ...state.settingUserDependency.docs,
            ...action.payload.docs,
          ].filter((o) =>
            ioHashDependency[o.dependencia_id]
              ? false
              : (ioHashDependency[o.dependencia_id] = true)
          ),
        },
        loadingSettingUserDependency: false,
      };

    case types.loadingCreateSettingDependency:
      return {
        ...state,
        loadingCreateSettingDependency: action.payload ?? true,
      };

    case types.createSettingDependency:
      return {
        ...state,
        loadingCreateSettingDependency: false,
        settingUserDependency:
          state.settingUserDependency.docs.length > 0
            ? {
                ...state.settingUserDependency,
                docs: [...state.settingUserDependency.docs, action.payload],
              }
            : {
                ...state.settingUserDependency,
                docs: [action.payload],
              },
      };

    case types.loadingUpdateSettingDependency:
      return {
        ...state,
        loadingUpdateSettingDependency: action.payload ?? true,
      };

    case types.updateSettingDependency:
      return {
        ...state,
        settingUserDependency: {
          ...state.settingUserDependency,
          docs: state.settingUserDependency.docs.map((item) =>
            item.dependencia_id === action.payload?.dependencia_id
              ? (item = action.payload)
              : item
          ),
        },
        loadingUpdateSettingDependency: false,
      };

    case types.loadingRemoveSettingDependency:
      return {
        ...state,
        loadingRemoveSettingDependency: action.payload ?? true,
      };

    case types.removeSettingDependency:
      return {
        ...state,
        settingUserDependency: {
          ...state.settingUserDependency,
          docs: state.settingUserDependency.docs.filter(
            (item) => item.dependencia_id !== action.payload?.dependencia_id
          ),
        },
        loadingRemoveSettingDependency: false,
        paginationDependencyPrev:
          state.settingUserDependency.page > 1 &&
          state.settingUserDependency.docs.length <= 0
            ? true
            : false,
      };

    // team

    case types.loadingSettingsUserTeam:
      return {
        ...state,
        loadingSettingUserTeam: action.payload ?? true,
      };

    case types.settingsUserTeam:
      return {
        ...state,
        settingUserTeam: {
          docs: action.payload.docs,
          page: action.payload.page,
          totalPages: action.payload.totalPages,
        },
        loadingSettingUserTeam: false,
      };

    case types.settingsUserTeamPGN:
      let ioHashTeam = {};
      return {
        ...state,
        settingUserTeam: {
          page: action.payload.page,
          totalPages: action.payload.totalPages,
          docs: [...state.settingUserTeam.docs, ...action.payload.docs].filter(
            (o) =>
              ioHashTeam[o.equipo_id] ? false : (ioHashTeam[o.equipo_id] = true)
          ),
        },
        loadingSettingUserTeam: false,
      };

    case types.loadingCreateSettingTeam:
      return {
        ...state,
        loadingCreateSettingTeam: action.payload ?? true,
      };

    case types.createSettingTeam:
      return {
        ...state,
        loadingCreateSettingTeam: false,
        settingUserTeam:
          state.settingUserTeam.docs.length > 0
            ? {
                ...state.settingUserTeam,
                docs: [...state.settingUserTeam.docs, action.payload],
              }
            : {
                ...state.settingUserTeam,
                docs: [action.payload],
              },
      };

    case types.loadingUpdateSettingTeam:
      return {
        ...state,
        loadingUpdateSettingTeam: action.payload ?? true,
      };

    case types.updateSettingTeam:
      return {
        ...state,
        settingUserTeam: {
          ...state.settingUserTeam,
          docs: state.settingUserTeam.docs.map((item) =>
            item.equipo_id === action.payload?.equipo_id
              ? (item = action.payload)
              : item
          ),
        },
        loadingUpdateSettingTeam: false,
      };

    case types.loadingRemoveSettingTeam:
      return {
        ...state,
        loadingRemoveSettingTeam: action.payload ?? true,
      };

    case types.removeSettingTeam:
      return {
        ...state,
        settingUserTeam: {
          ...state.settingUserTeam,
          docs: state.settingUserTeam.docs.filter(
            (item) => item.equipo_id !== action.payload?.equipo_id
          ),
        },
        loadingRemoveSettingTeam: false,
        paginationTeamPrev:
          state.settingUserTeam.page > 1 &&
          state.settingUserTeam.docs.length <= 0
            ? true
            : false,
      };

    // type team

    case types.loadingSettingsUserTypeTeam:
      return {
        ...state,
        loadingSettingUserTypeTeam: action.payload ?? true,
      };

    case types.settingsUserTypeTeam:
      return {
        ...state,
        settingUserTypeTeam: {
          docs: action.payload.docs,
          page: action.payload.page,
          totalPages: action.payload.totalPages,
        },
        loadingSettingUserTypeTeam: false,
      };

    case types.settingsUserTypeTeamPGN:
      let ioHashTypeTeam = {};
      return {
        ...state,
        settingUserTypeTeam: {
          page: action.payload.page,
          totalPages: action.payload.totalPages,
          docs: [
            ...state.settingUserTypeTeam.docs,
            ...action.payload.docs,
          ].filter((o) =>
            ioHashTypeTeam[o.tipo_equipo_id]
              ? false
              : (ioHashTypeTeam[o.tipo_equipo_id] = true)
          ),
        },
        loadingSettingUserTypeTeam: false,
      };

    case types.loadingCreateSettingTypeTeam:
      return {
        ...state,
        loadingCreateSettingTypeTeam: action.payload ?? true,
      };

    case types.createSettingTypeTeam:
      return {
        ...state,
        loadingCreateSettingTypeTeam: false,
        settingUserTypeTeam:
          state.settingUserTypeTeam.docs.length > 0
            ? {
                ...state.settingUserTypeTeam,
                docs: [...state.settingUserTypeTeam.docs, action.payload],
              }
            : {
                ...state.settingUserTypeTeam,
                docs: [action.payload],
              },
      };

    case types.loadingUpdateSettingTypeTeam:
      return {
        ...state,
        loadingUpdateSettingTypeTeam: action.payload ?? true,
      };

    case types.updateSettingTypeTeam:
      return {
        ...state,
        settingUserTypeTeam: {
          ...state.settingUserTypeTeam,
          docs: state.settingUserTypeTeam.docs.map((item) =>
            item.tipo_equipo_id === action.payload?.tipo_equipo_id
              ? (item = action.payload)
              : item
          ),
        },
        loadingUpdateSettingTypeTeam: false,
      };

    case types.loadingRemoveSettingTypeTeam:
      return {
        ...state,
        loadingRemoveSettingTypeTeam: action.payload ?? true,
      };

    case types.removeSettingTypeTeam:
      return {
        ...state,
        settingUserTypeTeam: {
          ...state.settingUserTypeTeam,
          docs: state.settingUserTypeTeam.docs.filter(
            (item) => item.tipo_equipo_id !== action.payload?.tipo_equipo_id
          ),
        },
        loadingRemoveSettingTypeTeam: false,
        paginationTypeTeamPrev:
          state.settingUserTypeTeam.page > 1 &&
          state.settingUserTypeTeam.docs.length <= 0
            ? true
            : false,
      };

    // Type service
    case types.loadingSettingsUserTypeService:
      return {
        ...state,
        loadingSettingUserTypeService: action.payload ?? true,
      };

    case types.settingsUserTypeService:
      return {
        ...state,
        settingUserTypeService: {
          docs: action.payload.docs,
          page: action.payload.page,
          totalPages: action.payload.totalPages,
        },
        loadingSettingUserTypeService: false,
      };

    case types.settingsUserTypeServicePGN:
      let ioHashTypeService = {};
      return {
        ...state,
        settingUserTypeService: {
          page: action.payload.page,
          totalPages: action.payload.totalPages,
          docs: [
            ...state.settingUserTypeService.docs,
            ...action.payload.docs,
          ].filter((o) =>
            ioHashTypeService[o.tipo_equipo_id]
              ? false
              : (ioHashTypeService[o.tipo_equipo_id] = true)
          ),
        },
        loadingSettingUserTypeService: false,
      };

    case types.loadingCreateSettingTypeService:
      return {
        ...state,
        loadingCreateSettingTypeService: action.payload ?? true,
      };

    case types.createSettingTypeService:
      return {
        ...state,
        loadingCreateSettingTypeService: false,
        settingUserTypeService:
          state.settingUserTypeService.docs.length > 0
            ? {
                ...state.settingUserTypeService,
                docs: [...state.settingUserTypeService.docs, action.payload],
              }
            : {
                ...state.settingUserTypeService,
                docs: [action.payload],
              },
      };

    case types.loadingUpdateSettingTypeService:
      return {
        ...state,
        loadingUpdateSettingTypeService: action.payload ?? true,
      };

    case types.updateSettingTypeService:
      return {
        ...state,
        settingUserTypeService: {
          ...state.settingUserTypeService,
          docs: state.settingUserTypeService.docs.map((item) =>
            item.tipo_servicio_id === action.payload?.tipo_servicio_id
              ? (item = action.payload)
              : item
          ),
        },
        loadingUpdateSettingTypeService: false,
      };

    case types.loadingRemoveSettingTypeService:
      return {
        ...state,
        loadingRemoveSettingTypeService: action.payload ?? true,
      };

    case types.removeSettingTypeService:
      return {
        ...state,
        settingUserTypeService: {
          ...state.settingUserTypeService,
          docs: state.settingUserTypeService.docs.filter(
            (item) => item.tipo_servicio_id !== action.payload?.tipo_servicio_id
          ),
        },
        loadingRemoveSettingTypeService: false,
        paginationTypeServicePrev:
          state.settingUserTypeService.page > 1 &&
          state.settingUserTypeService.docs.length <= 0
            ? true
            : false,
      };

    case types.activeSearchRolSetting:
      return {
        ...state,
        activeSearchSetting: action.payload ?? true,
        searchSettingUserRol: !action.payload
          ? { docs: [], totalPages: 0, page: 0 }
          : state.searchSettingUserRol,
      };

    case types.searchSettingUserRol:
      return {
        ...state,
        searchSettingUserRol: action.payload,
      };

    case types.activeSearchCargoSetting:
      return {
        ...state,
        activeSearchSetting: action.payload ?? true,
        searchSettingUserCargo: !action.payload
          ? { docs: [], totalPages: 0, page: 0 }
          : state.searchSettingUserCargo,
      };

    case types.searchSettingUserCargo:
      return {
        ...state,
        searchSettingUserCargo: action.payload,
      };

    case types.activeSearchTeamSetting:
      return {
        ...state,
        activeSearchSetting: action.payload ?? true,
        searchSettingUserTeam: !action.payload
          ? { docs: [], totalPages: 0, page: 0 }
          : state.searchSettingUserTeam,
      };

    case types.searchSettingUserTeam:
      return {
        ...state,
        searchSettingUserTeam: action.payload,
      };

    case types.activeSearchTypeTeamSetting:
      return {
        ...state,
        activeSearchSetting: action.payload ?? true,
        searchSettingUserTypeTeam: !action.payload
          ? { docs: [], totalPages: 0, page: 0 }
          : state.searchSettingUserTypeTeam,
      };

    case types.searchSettingUserTypeTeam:
      return {
        ...state,
        searchSettingUserTypeTeam: action.payload,
      };

    case types.activeSearchDependencySetting:
      return {
        ...state,
        activeSearchSetting: action.payload ?? true,
        searchSettingUserDependency: !action.payload
          ? { docs: [], totalPages: 0, page: 0 }
          : state.searchSettingUserDependency,
      };

    case types.searchSettingUserDependency:
      return {
        ...state,
        searchSettingUserDependency: action.payload,
      };

    case types.activeSearchTypeServiceSetting:
      return {
        ...state,
        activeSearchSetting: action.payload ?? true,
        searchSettingUserTypeService: !action.payload
          ? { docs: [], totalPages: 0, page: 0 }
          : state.searchSettingUserTypeService,
      };

    case types.searchSettingUserTypeService:
      return {
        ...state,
        searchSettingUserTypeService: action.payload,
      };

    case types.logout:
      return {
        activeSearchSetting: false,
        textSearchSetting: "",
        loadingSettingUserRol: false,
        loadingCreateSettingRol: false,
        loadingUpdateSettingRol: false,
        loadingRemoveSettingRol: false,
        paginationRolPrev: false,
        settingUserRol: {
          docs: [],
          totalPages: 0,
          page: 0,
        },
        searchSettingUserRol: {
          docs: [],
          totalPages: 0,
          page: 0,
        },
        loadingSettingUserDependency: false,
        loadingCreateSettingDependency: false,
        loadingUpdateSettingDependency: false,
        loadingRemoveSettingDependency: false,
        paginationDependencyPrev: false,
        settingUserDependency: {
          docs: [],
          totalPages: 0,
          page: 0,
        },
        searchSettingUserDependency: {
          docs: [],
          totalPages: 0,
          page: 0,
        },
        loadingSettingUserCargo: false,
        loadingCreateSettingCargo: false,
        loadingUpdateSettingCargo: false,
        loadingRemoveSettingCargo: false,
        paginationCargoPrev: false,
        settingUserCargo: {
          docs: [],
          totalPages: 0,
          page: 0,
        },
        searchSettingUserCargo: {
          docs: [],
          totalPages: 0,
          page: 0,
        },
        loadingSettingUserTeam: false,
        loadingCreateSettingTeam: false,
        loadingUpdateSettingTeam: false,
        loadingRemoveSettingTeam: false,
        paginationTeamPrev: false,
        settingUserTeam: {
          docs: [],
          totalPages: 0,
          page: 0,
        },
        searchSettingUserTeam: {
          docs: [],
          totalPages: 0,
          page: 0,
        },
        loadingSettingUserTypeTeam: false,
        loadingCreateSettingTypeTeam: false,
        loadingUpdateSettingTypeTeam: false,
        loadingRemoveSettingTypeTeam: false,
        paginationTypeTeamPrev: false,
        settingUserTypeTeam: {
          docs: [],
          totalPages: 0,
          page: 0,
        },
        searchSettingUserTypeTeam: {
          docs: [],
          totalPages: 0,
          page: 0,
        },
        loadingSettingUserTypeService: false,
        loadingCreateSettingTypeService: false,
        loadingUpdateSettingTypeService: false,
        loadingRemoveSettingTypeService: false,
        paginationTypeServicePrev: false,
        settingUserTypeService: {
          docs: [],
          totalPages: 0,
          page: 0,
        },
        searchSettingUserTypeService: {
          docs: [],
          totalPages: 0,
          page: 0,
        },
      };

    default:
      return state;
  }
};
