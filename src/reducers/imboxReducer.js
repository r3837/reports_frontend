import { types } from "../types/types";

const initial = {
  renderMessageId: null,
  loadingCreateImbox: false,
  loadingRequestImbox: true,
  requestImbox: {
    docs: [],
    totalPages: 0,
    page: 0,
  },
  loadingRequestImboxAll: false,
  requestImboxAll: [],
  loadingSendImbox: true,
  sendImbox: {
    docs: [],
    totalPages: 0,
    page: 0,
  },
  loadingViewMessage: true,
  loadingObservation: false,
  loadingCreateObservation: false,
  viewMessage: {
    observations: {
      docs: [],
      totalPages: 0,
      page: 0,
    },
  },
  activeSearch: false,
  searchImbox: {
    docs: [],
    totalPages: 0,
    page: 0,
  },
  searchSendImbox: {
    docs: [],
    totalPages: 0,
    page: 0,
  },
};

export const ImboxReducer = (state = initial, action) => {
  switch (action.type) {
    case types.loadingCreateImbox:
      return {
        ...state,
        loadingCreateImbox: action.payload ?? true,
      };

    case types.createImbox:
      return {
        ...state,
        loadingCreateImbox: false,
        sendImbox:
          state.sendImbox.docs.length > 0
            ? {
                ...state.sendImbox,
                docs: [action.payload, ...state.sendImbox.docs],
              }
            : {
                ...state.sendImbox,
                docs: [action.payload],
              },
      };

    case types.loadingRequestImbox:
      return {
        ...state,
        loadingRequestImbox: action.payload ?? true,
      };

    case types.requestImbox:
      return {
        ...state,
        requestImbox: action.payload,
        loadingRequestImbox: false,
      };

    case types.loadingSendImbox:
      return {
        ...state,
        loadingSendImbox: action.payload ?? true,
      };

    case types.sendImbox:
      return {
        ...state,
        sendImbox: action.payload,
        loadingSendImbox: false,
      };

    case types.loadingViewMessage:
      return {
        ...state,
        loadingViewMessage: action.payload ?? true,
      };

    case types.viewMessage:
      return {
        ...state,
        loadingViewMessage: false,
        viewMessage: {
          ...state.viewMessage,
          ...action.payload,
        },
      };

    case types.loadingObservation:
      return {
        ...state,
        loadingObservation: action.payload ?? true,
      };

    case types.imboxObservationsReset:
      return {
        ...state,
        viewMessage: {
          ...state.viewMessage,
          observations: {
            docs: [],
            totalPages: 0,
            page: 0,
          },
        },
      };

    case types.imboxObservations:
      let ioHash = {};
      return {
        ...state,
        loadingObservation: false,
        viewMessage: {
          ...state.viewMessage,
          observations: {
            page: action.payload.page,
            totalPages: action.payload.totalPages,
            docs: [
              ...state.viewMessage.observations.docs,
              ...action.payload.docs,
            ].filter((o) =>
              ioHash[o.observacion_id]
                ? false
                : (ioHash[o.observacion_id] = true)
            ),
          },
        },
      };

    case types.loadingCreateObservation:
      return {
        ...state,
        loadingCreateObservation: action.payload ?? true,
      };

    case types.createObservation:
      let coHash = {};
      return {
        ...state,
        loadingCreateObservation: false,
        viewMessage: {
          ...state.viewMessage,
          observations: {
            ...state.viewMessage.observations,
            docs: [
              ...state.viewMessage.observations.docs,
              action.payload,
            ].filter((o) =>
              coHash[o.observacion_id]
                ? false
                : (coHash[o.observacion_id] = true)
            ),
          },
        },
      };

    case types.requestImboxRemove:
      return {
        ...state,
        requestImbox: {
          ...state.requestImbox,
          docs: state.requestImbox.docs.filter(
            (item) => item.solicitud_id !== action.payload?.solicitud_id
          ),
        },
      };

    case types.requestImboxRemoveAll:
      return {
        ...state,
        requestImbox: {
          ...state.requestImbox,
          docs: state.requestImbox.docs.filter(
            (item) => item.solicitud_id !== action.payload?.solicitud_id
          ),
        },
        sendImbox: {
          ...state.sendImbox,
          docs: state.sendImbox.docs.filter(
            (item) => item.solicitud_id !== action.payload?.solicitud_id
          ),
        },
      };

    case types.imboxUpdateSocket:
      return {
        ...state,
        requestImbox: {
          ...state.requestImbox,
          docs: state.requestImbox.docs.map((item) =>
            item?.solicitud_id === action.payload?.solicitud_id
              ? { ...item, estado: action.payload?.estado_solicitud }
              : item
          ),
        },
        sendImbox: {
          ...state.sendImbox,
          docs: state.sendImbox.docs.map((item) =>
            item?.solicitud_id === action.payload?.solicitud_id
              ? { ...item, estado: action.payload?.estado_solicitud }
              : item
          ),
        },
      };

    case types.requestImboxSocket:
      let socketHash = {};
      return {
        ...state,
        requestImbox: {
          ...state.requestImbox,
          docs: [action.payload, ...state.requestImbox.docs].filter((o) =>
            socketHash[o.solicitud_id]
              ? false
              : (socketHash[o.solicitud_id] = true)
          ),
        },
      };

    case types.renderMessageId:
      return {
        ...state,
        renderMessageId: action.payload,
      };

    case types.activeSearch:
      return {
        ...state,
        activeSearch: action.payload ?? true,
        searchImbox: !action.payload
          ? { docs: [], totalPages: 0, page: 0 }
          : state.searchImbox,
      };

    case types.activeSearchSend:
      return {
        ...state,
        activeSearch: action.payload ?? true,
        searchSendImbox: !action.payload
          ? { docs: [], totalPages: 0, page: 0 }
          : state.searchSendImbox,
      };

    case types.searchImbox:
      return {
        ...state,
        searchImbox: action.payload,
      };

    case types.searchSendImbox:
      return {
        ...state,
        searchSendImbox: action.payload,
      };

    case types.loadingRequestImboxAll:
      return {
        ...state,
        loadingRequestImboxAll: action.payload ?? true,
      };

    case types.requestImboxAll:
      return {
        ...state,
        requestImboxAll: action.payload,
        loadingRequestImboxAll: false,
      };

    case types.logout:
      return {
        renderMessageId: null,
        loadingCreateImbox: false,
        loadingRequestImbox: true,
        requestImbox: {
          docs: [],
          totalPages: 0,
          page: 0,
        },
        loadingRequestImboxAll: false,
        requestImboxAll: [],
        loadingSendImbox: true,
        sendImbox: {
          docs: [],
          totalPages: 0,
          page: 0,
        },
        loadingViewMessage: true,
        loadingObservation: false,
        loadingCreateObservation: false,
        viewMessage: {
          observations: {
            docs: [],
            totalPages: 0,
            page: 0,
          },
        },
        activeSearch: false,
        searchImbox: {
          docs: [],
          totalPages: 0,
          page: 0,
        },
        searchSendImbox: {
          docs: [],
          totalPages: 0,
          page: 0,
        },
      };

    default:
      return state;
  }
};
