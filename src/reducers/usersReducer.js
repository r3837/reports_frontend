import { types } from "../types/types";

/**
 *  @param {*} state =>
 * uid: 928293893
 * email: ejemplo@gmail.com
 * names: Pepito Rafael
 * lastNames: Perez
 */

const initial = {
  loadingUser: false,
  loadingRegister: false,
  loadingRemoveUser: false,
  loadingUpdateUser: false,
  paginationPrev: false,
  activeSearchUsers: false,
  registeredUsers: {
    docs: [],
    totalPages: 0,
    page: 0,
  },
};

export const usersReducer = (state = initial, action) => {
  switch (action.type) {
    case types.loadingUser:
      return {
        ...state,
        loadingUser: action.payload ?? true,
      };
    case types.registeredUser:
      return {
        ...state,
        registeredUsers: action.payload,
        loadingUser: false,
        paginationPrev: false,
      };

    case types.loadingRegister:
      return {
        ...state,
        loadingRegister: false,
      };

    case types.addUser:
      return {
        ...state,
        registeredUsers: {
          ...state.registeredUsers,
          docs: [...state.registeredUsers.docs, action.payload],
        },
        loadingRegister: false,
      };

    case types.loadingUpdateUser:
      return {
        ...state,
        loadingUpdateUser: action.payload ?? true,
      };

    case types.updateUser:
      return {
        ...state,
        registeredUsers: {
          ...state.registeredUsers,
          docs: state.registeredUsers?.docs.map((item) =>
            item?.usuario_id === action.payload?.usuario_id
              ? action.payload
              : item
          ),
        },
        loadingRegister: false,
      };

    case types.loadingRemoveUser:
      return {
        ...state,
        loadingRemoveUser: action.payload ?? true,
      };

    case types.removeUser:
      const p = state.registeredUsers?.docs.findIndex(
        (item) => item.usuario_id === action.payload?.usuario_id
      );
      state.registeredUsers?.docs.splice(p, 1);

      return {
        ...state,
        registeredUsers: {
          ...state.registeredUsers,
        },
        loadingRegister: false,
        paginationPrev:
          state.registeredUsers.page > 1 &&
          state.registeredUsers.docs.length <= 0
            ? true
            : false,
      };

      case types.activeSearchUsers:
        return {
          ...state,
          activeSearchUsers: action.payload ?? true,
          searchUsers: !action.payload
            ? { docs: [], totalPages: 0, page: 0 }
            : state.searchUsers,
        };
  
      case types.searchUsers:
        return {
          ...state,
          searchUsers: action.payload,
        };

    case types.logout:
      return {
        loadingUser: false,
        loadingRegister: false,
        loadingRemoveUser: false,
        loadingUpdateUser: false,
        paginationPrev: false,
        activeSearchUsers: false,
        registeredUsers: {
          docs: [],
          totalPages: 0,
          page: 0,
        },
      };

    default:
      return state;
  }
};
