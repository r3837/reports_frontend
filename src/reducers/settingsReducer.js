import { types } from "../types/types";

const initial = {
  loading: false,
  settingRequestTypes: [],
  settingsResponsables: [],
  settingRequestStatus: [],
  settingRequestTeams: [],
  loadingeSettingUser: false,
  settingUserRol: [],
  settingUserDependency: [],
  settingUserCargo: [],
  settingUserStatus: [
    {
      label: "Activo",
      value: true,
    },
    {
      label: "Inactivo",
      value: false,
    },
  ],
  loadingSettingTypeTeam: false,
  settingUserTypeTeam: [],
};

export const SettingsReducer = (state = initial, action) => {
  switch (action.type) {
    case types.loadingSettings:
      return {
        ...state,
        loading: true,
      };

    case types.settings:
      return {
        ...state,
        settingRequestTypes:
          state?.settingRequestTypes?.length > 0 &&
          action.payload?.docsRquestType?.length <= 0
            ? state?.settingRequestTypes
            : action.payload?.docsRquestType,
        settingsResponsables:
          state?.settingsResponsables?.length > 0 &&
          action.payload?.docsResponsable?.length <= 0
            ? state?.settingsResponsables
            : action.payload?.docsResponsable,
        settingRequestStatus:
          state?.settingRequestStatus?.length > 0 &&
          action.payload?.docsRequestStatus?.length <= 0
            ? state?.settingRequestStatus
            : action.payload?.docsRequestStatus,
        settingRequestTeams:
          state?.settingRequestTeams?.length > 0 &&
          action.payload?.settingUserTeams.length <= 0
            ? state?.settingRequestTeams
            : action.payload?.settingUserTeams,
        loading: false,
      };

    case types.loadingSettingsUser:
      return {
        ...state,
        loadingeSettingUser: action.payload ?? true,
      };

    case types.settingsUser:
      return {
        ...state,
        settingUserRol: action.payload.settingUserRol,
        settingUserDependency: action.payload.settingUserDependency,
        settingUserCargo: action.payload.settingUserCargo,
        loadingeSettingUser: false,
      };

    case types.loadingSettingTypeTeam:
      return {
        ...state,
        loadingSettingTypeTeam: action.payload ?? true,
      };
    case types.settingsTypeTeam:
      return {
        ...state,
        settingUserTypeTeam: action.payload.settingTeamTypeTeam,
        settingUserDependency: action.payload.settingTeamDependency,
        loadingSettingTypeTeam: false,
      };

    case types.logout:
      return {
        loading: false,
        settingRequestTypes: [],
        settingsResponsables: [],
        settingRequestStatus: [],
        settingRequestTeams: [],
        loadingeSettingUser: false,
        settingUserRol: [],
        settingUserDependency: [],
        settingUserCargo: [],
        settingUserStatus: [
          {
            label: "Activo",
            value: true,
          },
          {
            label: "Inactivo",
            value: false,
          },
        ],
        loadingSettingTypeTeam: false,
        settingTeamDependency: [],
      };

    default:
      return state;
  }
};
