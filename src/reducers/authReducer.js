import { types } from "../types/types";

/**
 *  @param {*} state =>
 * uid: 928293893
 * email: ejemplo@gmail.com
 * names: Pepito Rafael
 * lastNames: Perez
 */

const initial = {
  loading: false,
  user: null,
  logged: false,
  navegateLogin: false,
};

export const authReducer = (state = initial, action) => {
  switch (action.type) {
    case types.isLogged:
      return {
        ...state,
        loading: action.payload ?? true,
      };
    case types.login:
      return {
        user: action.payload,
        logged: true,
        loading: false,
      };

    case types.resetNavegateLogin:
      return {
        ...state,
        navegateLogin: action.payload,
      };

    case types.logout:
      return {
        loading: false,
        user: null,
        navegateLogin: false,
        logged: false,
      };

    default:
      return state;
  }
};
